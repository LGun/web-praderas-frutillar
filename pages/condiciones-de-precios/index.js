import React from "react";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
	title: { alignText: "left", width: "100%", paddingBottom: "20px" },
	padding: { paddingBottom: "20px !important" },
	specialPadding: { paddingTop: "20px", paddingBottom: "20px" }
}));

const Index = (props) => {
	const classes = useStyles();
	return (
		<Grid container spacing={3} className="special-cont-profile">
			<Grid item xs={12} className="ai-center d-flex column">
				<Typography variant="h5" gutterBottom className={classes.specialPadding}>
					Términos y condiciones de precio preferente
				</Typography>

				<Typography variant="body1" gutterBottom className={classes.padding}>
					Queremos informarles que como Inmobiliaria hemos considerado dar un valor preferente para un stock
					limitado de parcelas para aquellos clientes que decidan pagar al contado y con financiamiento
					propio. Para acceder a esta opción los clientes deben firmar la carta de seriedad de la oferta y
					haber pagado la garantía respectiva. La estructura sobre condiciones de precio preferente es la
					siguiente:
				</Typography>
				<Typography variant="h6" className={classes.title} gutterBottom>
					Condiciones de precio preferente
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Esta opción comenzará a regir el viernes 06 de agosto de 2021 y podrá ser finalizada unilateralmente
					por parte de la inmobiliaria sin tener lugar el pago de indemnización alguna a ningún tercero
					interesado.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					En una primera instancia, sólo tres (3) parcelas podrán hacer uso de la opción de precio preferente
					por un monto único y total de $45.000.000 millones de pesos. Lo anterior se aplicará bajo las
					siguientes condiciones: (i) Entrega de Vale Vista con el valor de la parcela, (ii) entrega de carta
					de instrucciones relacionada al documento señalado en el punto anterior con un plazo de sesenta (60)
					días corridos máximo a partir de la fecha en que se transfiere la garantía por seriedad de la
					oferta. En el evento que no se cumpla con el plazo, el precio de la parcela se mantendrá desde un
					precio de $55.000.000 millones de pesos.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Una vez vendidas las tres (3) parcelas señaladas en el punto II, se procederá automáticamente a
					vender cinco (5) parcelas adicionales a un precio especial de $48.000.000 millones de pesos. Dicha
					venta deberá cumplir las mismas condiciones señaladas en el párrafo anterior, sumando el monto de
					$3.000.000 millones de pesos adicionales a través de un Vale Vista complementario y su respectiva
					carta.
				</Typography>
			</Grid>
		</Grid>
	);
};

export default Index;
