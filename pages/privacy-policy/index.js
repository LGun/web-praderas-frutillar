import React from "react";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
	title: { alignText: "left", width: "100%", paddingBottom: "20px" },
	padding: { paddingBottom: "20px !important" },
	specialPadding: { paddingTop: "20px", paddingBottom: "20px" }
}));

const Index = (props) => {
	const classes = useStyles();

	return (
		<Grid container spacing={3} className="special-cont-profile">
			<Grid item xs={12} className="ai-center d-flex column">
				<Typography variant="h5" gutterBottom className={classes.specialPadding}>
					Términos y condiciones de canales digitales de Praderas de Frutillar
				</Typography>
				<Typography variant="h6" className={classes.title} gutterBottom>
					1. ASPECTOS GENERALES.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Los Términos y Condiciones que a continuación se detallan, regulan el acceso en Chile al sitio web
					www.praderasdefrutillar.cl y a la app “Praderas de Frutillar”, en adelante el “sitio web” y “app”
					respectivamente y en conjunto “canales digitales”, además de su correcto uso por parte de todo
					usuario o consumidor que ingrese a los canales digitales. Se deja constancia que la prestadora 72d
					Desarrollos Tecnológicos SpA es responsable de la parte técnica, y ha cedido el uso a título de
					arrendamiento a “Inmobiliaria los Cruceros Limitada”, de la “app” y “web” “Praderas de Frutillar”,
					sus códigos e infraestructura, siendo todo el contenido y gestión de aquella de responsabilidad de
					ésta última. En este sentido, es importante regular los mecanismos y procedimientos de uso de los
					mismos, ya que los canales digitales de la empresa contienen información relativa a la parcelación
					Fundo Praderas de Frutillar de Inmobiliaria Los Cruceros Limitada y a su proceso de compra.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Estos Términos y Condiciones se aplicarán y se entenderán incorporados en todas y cada una de las
					transacciones que se realicen con Inmobiliaria Los Cruceros Limitada, a través de cualquiera de sus
					sociedades relacionadas desarrolladoras de proyectos inmobiliarios, en adelante “Sociedades del
					grupo”, mediante los sistemas de comercialización comprendidos en el sitio web. Tanto el uso del
					sitio web, como la aplicación de estos Términos y Condiciones y cualquier transacción comercial que
					tenga lugar en los canales digitales, se someterán a la legislación vigente de la República de
					Chile, especialmente aquellas que dicen relación con la protección de los derechos y garantías de
					los consumidores, sin renunciar a los derechos y acciones que otorgan estas leyes.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Consecuente con lo anterior y con el objeto de poder brindar una mejor atención, Inmobiliaria Los
					Cruceros Limitada recomienda a cada uno de los usuarios descargar el presente instrumento,
					promoviendo su lectura previa realización de cualquier transacción con las Sociedades del grupo.
				</Typography>
				<Typography variant="h6" className={classes.title} gutterBottom>
					2. COMUNICACIONES ELECTRÓNICAS.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Toda comunicación promocional o aviso publicitario enviado electrónicamente desde los canales
					digitales de la empresa indicarán en el campo de asunto a que se refiere, nuestra identidad como
					Inmobiliaria Los Cruceros Limitada o Fundo Praderas de Frutillar y, además, se incorporará un link
					de Opt-out para que el destinatario, si lo desea, pueda solicitar la suspensión de esos envíos, sin
					perjuicio que la reserva, oferta de compra, promesa de compraventa o compraventa definitiva, se
					suscribirá, en definitiva, por aquella inmobiliaria de las Sociedades del grupo que desarrolle el
					proyecto inmobiliario en particular.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Inmobiliaria Los Cruceros Limitada no solicita datos personales ni financieros a través de e- mail
					ni de ninguna otra forma escrita. Si tienes cualquier duda comunícate con nosotros al +56975790254 o
					a contacto@praderasdefrutillar.cl.
				</Typography>
				<Typography variant="h6" className={classes.title} gutterBottom>
					3. ALCANCE DE LOS PRECIOS INFORMADOS EN ESTE SITIO.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Una vez recibido el comprobante de pago, Inmobiliaria Los Cruceros Limitada no modificará las
					condiciones o precios previamente pactados y bajo los cuales se haya ofrecido la parcela respectiva.
					Cualquier cambio en las informaciones publicadas en este sitio respecto a las condiciones,
					promociones y ofertas, sólo se referirá a operaciones futuras, sin afectar, en caso alguno, derechos
					adquiridos por los consumidores al momento de realizar exitosamente un pago.
				</Typography>
				<Typography variant="h6" className={classes.title} gutterBottom>
					4. POLÍTICAS DE PRIVACIDAD Y SEGURIDAD.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Inmobiliaria Los Cruceros Limitada se obliga a adoptar todas las medidas de seguridad necesarias
					para resguardar la privacidad de los datos personales de usuarios o clientes, que voluntariamente se
					entreguen al hacer uso de este sitio, ingresándolos al completar un formulario de contacto o un
					proceso de pago, los cuales sólo podrán ser utilizados por Inmobiliaria Los Cruceros Limitada para
					el llenado automático de documentos, recibos o comprobantes asociados a sus transacciones y para la
					operación de los medios de pago disponibles en los canales digitales. Del mismo modo, se adoptarán
					dichas medidas de seguridad sobre aquellos datos que le sean solicitados a clientes o usuarios
					relativos a su nombre, dirección electrónica y número telefónico. Los mencionados datos bajo ningún
					caso serán entregados a terceros, salvo autorización expresa del cliente o en aquellos casos en que
					la ley lo permita o la autoridad legal o administrativa respectiva los exija, y sólo podrán ser
					compartidos con las empresas que forman parte de las sociedades del grupo.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					El cliente inscrito en nuestros canales digitales tendrá derecho a requerir información de sus datos
					personales registrados y disponer de modificación, rectificación o eliminación de estos cuando lo
					estime conveniente en conformidad a la Ley N 19.628. Para solicitar de ello, el cliente debe enviar
					un mail a contacto@praderasdefrutillar.cl.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Inmobiliaria Los Cruceros Limitada utiliza en el sitio web el certificado SSL (Secure Sockets Layer)
					que asegura tanto la autenticidad del sitio como el cifrado de toda la información que entregan los
					usuarios a la Inmobiliaria. Cada vez que se realice una compra en los canales digitales de
					Inmobiliaria Los Cruceros Limitada y se complete información de carácter personal, el navegador del
					cliente se conectará a los canales digitales de Inmobiliaria Los Cruceros Limitada a través del
					protocolo SSL que acredita que el cliente está realmente en los respectivos servidores de
					Inmobiliaria Los Cruceros (lo anterior se materializa con la aparición del código HTTPS en la barra
					de direcciones del navegador del cliente). De esta forma, se establece un método de cifrado de
					información del cliente y una clave de sesión única. Esta tecnología permite que información
					personal, como nombre, dirección y datos de tarjeta de crédito sean codificadas antes de realizar la
					transferencia para que no pueda ser leída cuando viaja a través de Internet. Todos los certificados
					SSL se crean para un servidor particular, en un dominio específico y para una entidad comercial
					comprobada.
				</Typography>
				<Typography variant="h6" className={classes.title} gutterBottom>
					5. POLÍTICA DE COOKIES:
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Para el correcto funcionamiento de los canales digitales de Inmobiliaria Los Cruceros Limitada y con
					el objeto de proveer a nuestros visitantes y clientes, una mejor experiencia de navegación y una
					adecuada oferta de nuestros productos y servicios, teniendo en cuenta sus distintos intereses,
					resulta necesaria la utilización de cookies, que se descarguen y almacenen en sus dispositivos, con
					la finalidad de recordar sus preferencias de navegación, optimizar nuestro sitio y simplificar su
					uso.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Atendido que Inmobiliaria Los Cruceros Limitada está alineada con la ejecución de buenas prácticas
					empresariales, hemos diseñado una “Política de Cookies”, con el objeto de ofrecer mayor información
					relativa al funcionamiento de éstas, específicamente en cuanto su uso, los distintos tipos de
					cookies utilizadas por Inmobiliaria Los Cruceros Limitada y la manera cómo los usuarios pueden
					desactivarlas de sus dispositivos.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Dicho lo anterior, se informa a nuestros usuarios que, cuando estén visitando el sitio web nuestros
					canales digitales, estos podrían, automáticamente y sin mayor advertencia, grabar información del
					dispositivo del cliente, en forma de archivo de cookie.
				</Typography>
				<Typography variant="h6" className={classes.title} gutterBottom>
					¿Qué son las cookies?
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Las cookies son pequeños archivos de texto (ficheros), a menudo encriptados, que se instalan en el
					dispositivo del usuario, cada vez que se descarga un contenido de una página web o la web en
					general, para su visualización y que permiten, no solo conocer su actividad, sino que, identificar
					inequívocamente el dispositivo del usuario.
				</Typography>
				<Typography variant="h6" className={classes.title} gutterBottom>
					¿Para qué utilizamos las cookies?
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					En el sitio web de Inmobiliaria Los Cruceros Limitada, utilizamos cookies propias o de terceros para
					(i) mejorar el sitio web de Inmobiliaria Los Cruceros Limitada y, en consecuencia, su funcionamiento
					y nuestros servicios; (ii) elaborar información estadística respecto al uso de la web para
					optimizarla; y, (iii) personalizar el contenido que ofrecemos a nuestros usuarios en base a un
					análisis de sus hábitos de navegación.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					En efecto, las cookies permiten determinar qué elementos son de agrado del cliente y qué contenido
					pudo haber visualizado en sus visitas previas al sitio web, permitiendo personalizar el contenido
					ofrecido de acuerdo a sus hábitos y preferencias.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Hacemos presente que, la información almacenada en el dispositivo del cliente, no proporciona datos
					personales de éste, así como tampoco puede asociarse a una persona identificada o identificable. Se
					deja expresa constancia que la información obtenida por las cookies, está sujeta a la protección que
					brinda la Ley Número 19.628 sobre Datos Personales.
				</Typography>
				<Typography variant="h6" className={classes.title} gutterBottom>
					¿Qué tipo de cookies utilizamos?
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Cookies técnicas. Son aquéllas que permiten al usuario la navegación a través de un dispositivo y la
					utilización de las diferentes opciones o servicios que en ella existan como, por ejemplo, controlar
					el tráfico y la comunicación de datos, identificar la sesión, acceder a sitios de acceso
					restringido, recordar los elementos que integran un pedido, realizar el proceso de compra de un
					pedido, realizar la solicitud de inscripción o participación en un evento, utilizar elementos de
					seguridad durante la navegación, almacenar contenidos para la difusión de videos o sonido o
					compartir contenidos a través de redes sociales.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Cookies de análisis. Son aquéllas que permiten el seguimiento y análisis del comportamiento de los
					usuarios de los sitios web a los que están vinculadas. La información recogida mediante este tipo de
					cookies, se utiliza en la medición de la actividad de los sitios web, aplicación o plataforma y para
					la elaboración de perfiles de navegación de los usuarios de dichos sitios, aplicaciones y
					plataformas, con el fin de introducir mejoras en función del análisis de los datos de uso que hacen
					los usuarios del servicio.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Estas cookies generan un ID de usuario anónimo que se utiliza para medir cuántas veces visita el
					sitio un usuario. Asimismo, registra cuándo fue la primera y última vez que visitó el sitio y cuándo
					se ha terminado una sesión y su navegación.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Cookies publicitarias. Son aquéllas que permiten la gestión, de la forma más eficaz posible, de los
					espacios publicitarios que se encuentren en nuestra página web, aplicación o plataforma, almacenando
					de esta manera, información relativa al comportamiento de los usuarios obtenida a través de la
					observación continua de sus hábitos de navegación, lo que permite desarrollar un perfil específico
					para mostrar publicidad en función del mismo.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Ahora bien, hacemos presente que las cookies pueden ser de “Sesión”, que son aquellas que duran el
					tiempo que el usuario está navegando por la página web y se borran al término. Sirven para almacenar
					información que solo interesa conservar para la prestación del servicio solicitado por el usuario en
					una sola ocasión; o bien “Persistentes”, las cuales quedan almacenadas en el terminal del usuario
					por un tiempo más largo, facilitando así el control de las preferencias elegidas sin tener que
					repetir ciertos parámetros cada vez que se visite el sitio web.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Asimismo, informamos que el sitio web, cuenta con enlaces de redes sociales (como Facebook o
					Instagram), por lo que, al tratarse de empresas externas, no podrá controlar las cookies utilizadas
					por estas webs externas.
				</Typography>
				<Typography variant="h6" className={classes.title} gutterBottom>
					¿Cómo deshabilitar las cookies?
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Los usuarios tienen la posibilidad no solo de configurar su navegador para ser informados en todo
					evento, de la recepción de las distintas cookies que Inmobiliaria Los Cruceros Limitada pudiera usar
					en el sitio web, sino que también, permitir, bloquear, restringir o incluso deshabilitar el uso de
					las cookies instaladas en su equipo, mediante la configuración de las opciones del navegador de
					Internet utilizado por los usuarios.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Para estos efectos, los usuarios podrán deshabilitar el uso de cookies cuando lo consideren
					oportuno, a través de las opciones de configuración/ajuste de su navegador, el cual proporcionará
					las instrucciones operativas necesarias para la referida deshabilitación.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Por su parte, se deja expresa constancia que, cuando un usuario elige deshabilitar alguna de
					nuestras cookies, Inmobiliaria Los Cruceros Limitada deja de recopilar nueva información, sin
					embargo, la información recolectada con anterioridad a los cambios de preferencia de su navegador,
					podría eventualmente seguir siendo utilizada.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Inmobiliaria Los Cruceros Limitada no será responsable, en ninguna medida, de la información
					almacenada ni proporcionada por este medio, por lo que es de exclusiva responsabilidad del usuario,
					permitir o rechazar el uso de cookies.
				</Typography>
				<Typography variant="h6" className={classes.title} gutterBottom>
					6. PROMOCIONES.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Las promociones que se ofrezcan en los canales digitales no serán necesariamente las mismas que se
					publiciten en otros canales de venta de Inmobiliaria Los Cruceros Limitada.
				</Typography>
				<Typography variant="h6" className={classes.title} gutterBottom>
					7. INFORMACIÓN DE OTROS SITIOS WEB.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Inmobiliaria Los Cruceros Limitada no se responsabiliza por información proporcionada por otros
					sitios web o portales inmobiliarios y las consecuencias derivadas de ello cuando no dispongan de
					contratos o alianzas con Inmobiliaria Los Cruceros Limitada. Toda la información debe ser verificada
					con uno de nuestros ejecutivos o a través de nuestros canales digitales.
				</Typography>
				<Typography variant="h6" className={classes.title} gutterBottom>
					8. AUTORIZACIÓN DE ENVÍO DE INFORMACIÓN, OFERTAS Y PROMOCIONES.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					El cliente autoriza expresamente el envío de todo tipo de información relacionada con ofertas y
					promociones propias de Inmobiliaria Los Cruceros Limitada. Conforme al artículo 28 B de la Ley Nº
					19.496, el cliente podrá solicitar en cualquier momento la suspensión del envío de esta información.
				</Typography>
				<Typography variant="h6" className={classes.title} gutterBottom>
					9. PRECIOS E INFORMACIÓN.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Los precios exhibidos en nuestros canales digitales están sujetos a la disponibilidad de parcelas
					que la Inmobiliaria tenga en stock y sólo tienen validez para compras o reservas efectuadas en
					nuestros canales digitales o cualquier otra plataforma que no constituya venta presencial; lo que no
					podrá ser aplicable en nuestras salas de venta u otros canales de venta directos con el cliente.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Las superficies, dimensiones y medidas contenidas en los planos de las parcelas son sólo
					referenciales y pueden no constituir una representación exacta de la realidad.
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Todas las imágenes, caracterizaciones y textos contenidos en nuestros canales digitales fueron
					elaborados con fines ilustrativos y sus características, superficies, dimensiones y medidas son
					aproximadas, no constituyendo necesariamente una representación exacta de la realidad. Su único
					objetivo es mostrar las características generales del proyecto y no de cada uno de sus detalles.
				</Typography>
				<Typography variant="h6" className={classes.title} gutterBottom>
					10. ENTREGA DE LA PARCELA
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Las condiciones y plazos de entrega serán expresados en el contrato de promesa que deberás suscribir
					una vez realizada la reserva, sin embargo, esta deberá respetar y mencionar lo ofrecido en la
					cotización realizada por el vendedor de Inmobiliaria Los Cruceros Limitada.
				</Typography>
				<Typography variant="h6" className={classes.title} gutterBottom>
					11. CANALES DIGITALES
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Inmobiliaria Los Cruceros Limitada se reserva el derecho a modificar cualquier dato o información
					contenida en sus canales digitales, así como a suspender sin previo aviso su funcionamiento de
					manera total o parcial.
				</Typography>
				<Typography variant="h6" className={classes.title} gutterBottom>
					12. PROPIEDAD INTELECTUAL
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Todos los contenidos incluidos en el sitio web, como textos, material gráfico, logotipos, íconos de
					botones, imágenes, audio clips, descargas digitales y compilaciones de datos, son propiedad de
					Inmobiliaria Los Cruceros Limitada, o de sus proveedores de contenidos, y están protegidos por las
					leyes chilenas e internacionales sobre propiedad intelectual. Los materiales gráficos, logotipos,
					encabezados de páginas, frases publicitarias, iconos de botones, textos escritos y nombres de
					servicios incluidos en este sitio son marcas comerciales, creaciones o imágenes comerciales de
					propiedad de Inmobiliaria Los Cruceros Limitada. Dichas marcas, creaciones e imágenes comerciales no
					se pueden usar en relación a ningún producto o servicio que pueda causar confusión entre los
					clientes y en ninguna forma que desprestigie o desacredite a Inmobiliaria Los Cruceros Limitada. Las
					demás marcas comerciales que no sean de propiedad de Inmobiliaria Los Cruceros Limitada y que
					aparezcan en este sitio pertenecen a sus respectivos dueños, tales como la titularidad de dominio de
					72d Desarrollos Tecnológicos SpA sobre la “app” y “web” de “Praderas de Frutillar”, sus códigos e
					infraestructura.
				</Typography>
				<Typography variant="h6" className={classes.title} gutterBottom>
					13. CONTACTO Inmobiliaria Los Cruceros Limitada
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Cualquier consulta sobre la información contenida en estos Términos y Condiciones o de nuestro sitio
					web puede ser realizada vía mail a contacto@praderasdefrutillar.cl o al teléfono +56957268816
				</Typography>
				<Typography variant="h6" className={classes.title} gutterBottom>
					14. INFORMACIÓN DE LA EMPRESA
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Razón Social: Inmobiliaria Los Cruceros Limitada RUT: 76.114.448-0 Representantes Legales: Alejandra
					del Rosario Ortega Follmer y Eduardo Federico José Edwards Zaccarelli Dirección: Fundo El Rincón A -
					Frutillar - Región de Los Lagos
				</Typography>
				<Typography variant="h6" className={classes.title} gutterBottom>
					15. DOMICILIOS
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					Para estos efectos, el domicilio de Inmobilia Los Cruceros será Fundo el Rincón A, Frutillar, Región
					de los Lagos, República de Chile. Cualquier dificultad o controversia que se produzca a raíz del
					presente instrumento será sometido a la jurisdicción y competencia de los tribunales civiles de
					Santiago. Sin perjuicio de lo anterior, siempre podrán aplicarse las reglas de competencia
					establecidas en el artículo 50 A de la Ley N° 19.496.
				</Typography>
				<Typography variant="h6" className={classes.title} gutterBottom>
					16. VIGENCIA DE ESTOS TÉRMINOS Y CONDICIONES
				</Typography>
				<Typography variant="body1" gutterBottom className={classes.padding}>
					El presente instrumento comenzará a regir desde el 04 de Enero de 2021.
				</Typography>
			</Grid>
		</Grid>
	);
};

export default Index;
