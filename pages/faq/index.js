import React from "react";
import dynamic from "next/dynamic";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
const VideoComponent = dynamic(() => import("../../src/components/video"));

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
		backgroundColor: "#FFF",
		padding: "40px 0px",
		minHeight: "calc(100vh - 390px)"
	},
	title: { alignText: "left", width: "100%", paddingBottom: "20px" },
	padding: { paddingBottom: "20px !important" },
	specialPadding: { paddingTop: "20px", paddingBottom: "20px" },
	heading: {
		fontSize: theme.typography.pxToRem(15),
		fontWeight: 600,
		color: "#6b6b6b"
	},
	videoTranscript: {
		fontSize: "12px"
	},
	headingCont: {
		height: "100%",
		position: "relative"
	},
	borderBottom: {
		position: "absolute",
		height: "3px",
		backgroundColor: "#A3AD31",
		width: "calc(100% + 18px)",
		bottom: "-12px",
		left: "-15px"
	},
	"@media (max-width: 1070px)": {
		root: {
			flexGrow: 1,
			backgroundColor: "#FFF",
			padding: "40px 0px",
			minHeight: "calc(100vh - 165px)"
		}
	}
}));

const Index = (props) => {
	const classes = useStyles();

	return (
		<div className={classes.root}>
			<Grid container spacing={3} className="special-cont-profile">
				<Grid item xs={12} className="ai-center d-flex column">
					<h1 className="simple-title-faq green padding-b20">
						<strong>- PREGUNTAS FRECUENTES -</strong>
					</h1>
				</Grid>
				<Grid item xs={12} className="ai-center d-flex column">
					<Accordion className="full-width" defaultExpanded>
						<AccordionSummary
							expandIcon={<ExpandMoreIcon />}
							aria-controls="panel1a-content"
							className="accordion-header-bg"
							id="panel1a-header"
						>
							<div className={classes.headingCont}>
								<Typography className={classes.heading}>CÓMO COMPRAR</Typography>
								{/* <div className={classes.borderBottom} /> */}
							</div>
						</AccordionSummary>
						<AccordionDetails className="accordion-details-bg">
							<Grid container spacing={6}>
								<Grid item xs={12} md={6}>
									<h3 className="faq-title">
										¿QUÉ PASA SI ME ATRASO EN EL PAGO DE LAS CUOTAS DEL PIE?
									</h3>
									<div className="br-20">
										<VideoComponent url="https://www.youtube.com/watch?v=KJ5UazhKXC8" />
										<Accordion className="full-width">
											<AccordionSummary
												expandIcon={<ExpandMoreIcon />}
												aria-controls="panel1a-content"
												id="panel1a-header"
											>
												<Typography className={classes.heading}>Video Transcript</Typography>
											</AccordionSummary>
											<AccordionDetails>
												<Typography className={classes.videoTranscript}>
													El número de cuotas permitidas para pagar el pie de la vivienda es
													diferente para cada proyecto, y depende de la fecha de entrega del
													mismo. Por esto, mientras más temprano sea su decisión de compra,
													podrá optar a una mayor cantidad de cuotas. El número de cuotas
													permitidas para pagar el pie de la vivienda es diferente para cada
													proyecto, y depende de la fecha de entrega del mismo. Por esto,
													mientras más temprano sea su decisión de compra, podrá optar a una
													mayor cantidad de cuotas.
												</Typography>
											</AccordionDetails>
										</Accordion>
									</div>
								</Grid>
								<Grid item xs={12} md={6}>
									<h3 className="faq-title">
										¿QUÉ PASA SI ME ATRASO EN EL PAGO DE LAS CUOTAS DEL PIE?
									</h3>
									<div className="br-20">
										<VideoComponent url="https://www.youtube.com/watch?v=KJ5UazhKXC8" />
										<Accordion className="full-width">
											<AccordionSummary
												expandIcon={<ExpandMoreIcon />}
												aria-controls="panel1a-content"
												id="panel1a-header"
											>
												<Typography className={classes.heading}>Video Transcript</Typography>
											</AccordionSummary>
											<AccordionDetails>
												<Typography className={classes.videoTranscript}>
													El número de cuotas permitidas para pagar el pie de la vivienda es
													diferente para cada proyecto, y depende de la fecha de entrega del
													mismo. Por esto, mientras más temprano sea su decisión de compra,
													podrá optar a una mayor cantidad de cuotas. El número de cuotas
													permitidas para pagar el pie de la vivienda es diferente para cada
													proyecto, y depende de la fecha de entrega del mismo. Por esto,
													mientras más temprano sea su decisión de compra, podrá optar a una
													mayor cantidad de cuotas.
												</Typography>
											</AccordionDetails>
										</Accordion>
									</div>
								</Grid>
								<Grid item xs={12} md={6}>
									<h3 className="faq-title">
										¿QUÉ PASA SI ME ATRASO EN EL PAGO DE LAS CUOTAS DEL PIE?
									</h3>
									<div className="br-20">
										<VideoComponent url="https://www.youtube.com/watch?v=KJ5UazhKXC8" />
										<Accordion className="full-width">
											<AccordionSummary
												expandIcon={<ExpandMoreIcon />}
												aria-controls="panel1a-content"
												id="panel1a-header"
											>
												<Typography className={classes.heading}>Video Transcript</Typography>
											</AccordionSummary>
											<AccordionDetails>
												<Typography className={classes.videoTranscript}>
													El número de cuotas permitidas para pagar el pie de la vivienda es
													diferente para cada proyecto, y depende de la fecha de entrega del
													mismo. Por esto, mientras más temprano sea su decisión de compra,
													podrá optar a una mayor cantidad de cuotas. El número de cuotas
													permitidas para pagar el pie de la vivienda es diferente para cada
													proyecto, y depende de la fecha de entrega del mismo. Por esto,
													mientras más temprano sea su decisión de compra, podrá optar a una
													mayor cantidad de cuotas.
												</Typography>
											</AccordionDetails>
										</Accordion>
									</div>
								</Grid>
								<Grid item xs={12} md={6}>
									<h3 className="faq-title">
										¿QUÉ PASA SI ME ATRASO EN EL PAGO DE LAS CUOTAS DEL PIE?
									</h3>
									<div className="br-20">
										<VideoComponent url="https://www.youtube.com/watch?v=KJ5UazhKXC8" />
										<Accordion className="full-width">
											<AccordionSummary
												expandIcon={<ExpandMoreIcon />}
												aria-controls="panel1a-content"
												id="panel1a-header"
											>
												<Typography className={classes.heading}>Video Transcript</Typography>
											</AccordionSummary>
											<AccordionDetails>
												<Typography className={classes.videoTranscript}>
													El número de cuotas permitidas para pagar el pie de la vivienda es
													diferente para cada proyecto, y depende de la fecha de entrega del
													mismo. Por esto, mientras más temprano sea su decisión de compra,
													podrá optar a una mayor cantidad de cuotas. El número de cuotas
													permitidas para pagar el pie de la vivienda es diferente para cada
													proyecto, y depende de la fecha de entrega del mismo. Por esto,
													mientras más temprano sea su decisión de compra, podrá optar a una
													mayor cantidad de cuotas.
												</Typography>
											</AccordionDetails>
										</Accordion>
									</div>
								</Grid>
							</Grid>
						</AccordionDetails>
					</Accordion>
					<Accordion className="full-width">
						<AccordionSummary
							expandIcon={<ExpandMoreIcon />}
							aria-controls="panel1a-content"
							className="accordion-header-bg"
							id="panel1a-header"
						>
							<div className={classes.headingCont}>
								<Typography className={classes.heading}>FINANCIAMIENTO</Typography>
								{/* <div className={classes.borderBottom} /> */}
							</div>
						</AccordionSummary>
						<AccordionDetails className="accordion-details-bg">
							<Grid container spacing={6}>
								<Grid item xs={12} md={6}>
									<h3 className="faq-title">
										¿QUÉ PASA SI ME ATRASO EN EL PAGO DE LAS CUOTAS DEL PIE?
									</h3>
									<div className="br-20">
										<VideoComponent url="https://www.youtube.com/watch?v=KJ5UazhKXC8" />
										<Accordion className="full-width">
											<AccordionSummary
												expandIcon={<ExpandMoreIcon />}
												aria-controls="panel1a-content"
												id="panel1a-header"
											>
												<Typography className={classes.heading}>Video Transcript</Typography>
											</AccordionSummary>
											<AccordionDetails>
												<Typography className={classes.videoTranscript}>
													El número de cuotas permitidas para pagar el pie de la vivienda es
													diferente para cada proyecto, y depende de la fecha de entrega del
													mismo. Por esto, mientras más temprano sea su decisión de compra,
													podrá optar a una mayor cantidad de cuotas. El número de cuotas
													permitidas para pagar el pie de la vivienda es diferente para cada
													proyecto, y depende de la fecha de entrega del mismo. Por esto,
													mientras más temprano sea su decisión de compra, podrá optar a una
													mayor cantidad de cuotas.
												</Typography>
											</AccordionDetails>
										</Accordion>
									</div>
								</Grid>
								<Grid item xs={12} md={6}>
									<h3 className="faq-title">
										¿QUÉ PASA SI ME ATRASO EN EL PAGO DE LAS CUOTAS DEL PIE?
									</h3>
									<div className="br-20">
										<VideoComponent url="https://www.youtube.com/watch?v=KJ5UazhKXC8" />
										<Accordion className="full-width">
											<AccordionSummary
												expandIcon={<ExpandMoreIcon />}
												aria-controls="panel1a-content"
												id="panel1a-header"
											>
												<Typography className={classes.heading}>Video Transcript</Typography>
											</AccordionSummary>
											<AccordionDetails>
												<Typography className={classes.videoTranscript}>
													El número de cuotas permitidas para pagar el pie de la vivienda es
													diferente para cada proyecto, y depende de la fecha de entrega del
													mismo. Por esto, mientras más temprano sea su decisión de compra,
													podrá optar a una mayor cantidad de cuotas. El número de cuotas
													permitidas para pagar el pie de la vivienda es diferente para cada
													proyecto, y depende de la fecha de entrega del mismo. Por esto,
													mientras más temprano sea su decisión de compra, podrá optar a una
													mayor cantidad de cuotas.
												</Typography>
											</AccordionDetails>
										</Accordion>
									</div>
								</Grid>
								<Grid item xs={12} md={6}>
									<h3 className="faq-title">
										¿QUÉ PASA SI ME ATRASO EN EL PAGO DE LAS CUOTAS DEL PIE?
									</h3>
									<div className="br-20">
										<VideoComponent url="https://www.youtube.com/watch?v=KJ5UazhKXC8" />
										<Accordion className="full-width">
											<AccordionSummary
												expandIcon={<ExpandMoreIcon />}
												aria-controls="panel1a-content"
												id="panel1a-header"
											>
												<Typography className={classes.heading}>Video Transcript</Typography>
											</AccordionSummary>
											<AccordionDetails>
												<Typography className={classes.videoTranscript}>
													El número de cuotas permitidas para pagar el pie de la vivienda es
													diferente para cada proyecto, y depende de la fecha de entrega del
													mismo. Por esto, mientras más temprano sea su decisión de compra,
													podrá optar a una mayor cantidad de cuotas. El número de cuotas
													permitidas para pagar el pie de la vivienda es diferente para cada
													proyecto, y depende de la fecha de entrega del mismo. Por esto,
													mientras más temprano sea su decisión de compra, podrá optar a una
													mayor cantidad de cuotas.
												</Typography>
											</AccordionDetails>
										</Accordion>
									</div>
								</Grid>
								<Grid item xs={12} md={6}>
									<h3 className="faq-title">
										¿QUÉ PASA SI ME ATRASO EN EL PAGO DE LAS CUOTAS DEL PIE?
									</h3>
									<div className="br-20">
										<VideoComponent url="https://www.youtube.com/watch?v=KJ5UazhKXC8" />
										<Accordion className="full-width">
											<AccordionSummary
												expandIcon={<ExpandMoreIcon />}
												aria-controls="panel1a-content"
												id="panel1a-header"
											>
												<Typography className={classes.heading}>Video Transcript</Typography>
											</AccordionSummary>
											<AccordionDetails>
												<Typography className={classes.videoTranscript}>
													El número de cuotas permitidas para pagar el pie de la vivienda es
													diferente para cada proyecto, y depende de la fecha de entrega del
													mismo. Por esto, mientras más temprano sea su decisión de compra,
													podrá optar a una mayor cantidad de cuotas. El número de cuotas
													permitidas para pagar el pie de la vivienda es diferente para cada
													proyecto, y depende de la fecha de entrega del mismo. Por esto,
													mientras más temprano sea su decisión de compra, podrá optar a una
													mayor cantidad de cuotas.
												</Typography>
											</AccordionDetails>
										</Accordion>
									</div>
								</Grid>
							</Grid>
						</AccordionDetails>
					</Accordion>
					<Accordion className="full-width">
						<AccordionSummary
							expandIcon={<ExpandMoreIcon />}
							aria-controls="panel1a-content"
							className="accordion-header-bg"
							id="panel1a-header"
						>
							<div className={classes.headingCont}>
								<Typography className={classes.heading}>POST-VENTA</Typography>
								{/* <div className={classes.borderBottom} /> */}
							</div>
						</AccordionSummary>
						<AccordionDetails className="accordion-details-bg">
							<Grid container spacing={6}>
								<Grid item xs={12} md={6}>
									<h3 className="faq-title">
										¿QUÉ PASA SI ME ATRASO EN EL PAGO DE LAS CUOTAS DEL PIE?
									</h3>
									<div className="br-20">
										<VideoComponent url="https://www.youtube.com/watch?v=KJ5UazhKXC8" />
										<Accordion className="full-width">
											<AccordionSummary
												expandIcon={<ExpandMoreIcon />}
												aria-controls="panel1a-content"
												id="panel1a-header"
											>
												<Typography className={classes.heading}>Video Transcript</Typography>
											</AccordionSummary>
											<AccordionDetails>
												<Typography className={classes.videoTranscript}>
													El número de cuotas permitidas para pagar el pie de la vivienda es
													diferente para cada proyecto, y depende de la fecha de entrega del
													mismo. Por esto, mientras más temprano sea su decisión de compra,
													podrá optar a una mayor cantidad de cuotas. El número de cuotas
													permitidas para pagar el pie de la vivienda es diferente para cada
													proyecto, y depende de la fecha de entrega del mismo. Por esto,
													mientras más temprano sea su decisión de compra, podrá optar a una
													mayor cantidad de cuotas.
												</Typography>
											</AccordionDetails>
										</Accordion>
									</div>
								</Grid>
								<Grid item xs={12} md={6}>
									<h3 className="faq-title">
										¿QUÉ PASA SI ME ATRASO EN EL PAGO DE LAS CUOTAS DEL PIE?
									</h3>
									<div className="br-20">
										<VideoComponent url="https://www.youtube.com/watch?v=KJ5UazhKXC8" />
										<Accordion className="full-width">
											<AccordionSummary
												expandIcon={<ExpandMoreIcon />}
												aria-controls="panel1a-content"
												id="panel1a-header"
											>
												<Typography className={classes.heading}>Video Transcript</Typography>
											</AccordionSummary>
											<AccordionDetails>
												<Typography className={classes.videoTranscript}>
													El número de cuotas permitidas para pagar el pie de la vivienda es
													diferente para cada proyecto, y depende de la fecha de entrega del
													mismo. Por esto, mientras más temprano sea su decisión de compra,
													podrá optar a una mayor cantidad de cuotas. El número de cuotas
													permitidas para pagar el pie de la vivienda es diferente para cada
													proyecto, y depende de la fecha de entrega del mismo. Por esto,
													mientras más temprano sea su decisión de compra, podrá optar a una
													mayor cantidad de cuotas.
												</Typography>
											</AccordionDetails>
										</Accordion>
									</div>
								</Grid>
								<Grid item xs={12} md={6}>
									<h3 className="faq-title">
										¿QUÉ PASA SI ME ATRASO EN EL PAGO DE LAS CUOTAS DEL PIE?
									</h3>
									<div className="br-20">
										<VideoComponent url="https://www.youtube.com/watch?v=KJ5UazhKXC8" />
										<Accordion className="full-width">
											<AccordionSummary
												expandIcon={<ExpandMoreIcon />}
												aria-controls="panel1a-content"
												id="panel1a-header"
											>
												<Typography className={classes.heading}>Video Transcript</Typography>
											</AccordionSummary>
											<AccordionDetails>
												<Typography className={classes.videoTranscript}>
													El número de cuotas permitidas para pagar el pie de la vivienda es
													diferente para cada proyecto, y depende de la fecha de entrega del
													mismo. Por esto, mientras más temprano sea su decisión de compra,
													podrá optar a una mayor cantidad de cuotas. El número de cuotas
													permitidas para pagar el pie de la vivienda es diferente para cada
													proyecto, y depende de la fecha de entrega del mismo. Por esto,
													mientras más temprano sea su decisión de compra, podrá optar a una
													mayor cantidad de cuotas.
												</Typography>
											</AccordionDetails>
										</Accordion>
									</div>
								</Grid>
								<Grid item xs={12} md={6}>
									<h3 className="faq-title">
										¿QUÉ PASA SI ME ATRASO EN EL PAGO DE LAS CUOTAS DEL PIE?
									</h3>
									<div className="br-20">
										<VideoComponent url="https://www.youtube.com/watch?v=KJ5UazhKXC8" />
										<Accordion className="full-width">
											<AccordionSummary
												expandIcon={<ExpandMoreIcon />}
												aria-controls="panel1a-content"
												id="panel1a-header"
											>
												<Typography className={classes.heading}>Video Transcript</Typography>
											</AccordionSummary>
											<AccordionDetails>
												<Typography className={classes.videoTranscript}>
													El número de cuotas permitidas para pagar el pie de la vivienda es
													diferente para cada proyecto, y depende de la fecha de entrega del
													mismo. Por esto, mientras más temprano sea su decisión de compra,
													podrá optar a una mayor cantidad de cuotas. El número de cuotas
													permitidas para pagar el pie de la vivienda es diferente para cada
													proyecto, y depende de la fecha de entrega del mismo. Por esto,
													mientras más temprano sea su decisión de compra, podrá optar a una
													mayor cantidad de cuotas.
												</Typography>
											</AccordionDetails>
										</Accordion>
									</div>
								</Grid>
							</Grid>
						</AccordionDetails>
					</Accordion>
				</Grid>
			</Grid>
		</div>
	);
};

export default Index;
