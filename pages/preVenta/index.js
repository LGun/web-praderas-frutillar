import React, { useState, useEffect, useContext } from "react";
import { useRouter } from "next/router";

import dynamic from "next/dynamic";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";
import { makeStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import InsertEmoticon from "@material-ui/icons/InsertEmoticon";

import { onAuthStateChange } from "../../src/firebase";
import ProfileModal from "../../src/components/modal/modalProfile";
import { getUserByUid, getLikedLandByUid } from "../../src/services";

import { ProfileModalDispatch } from "../../src/context/profileModal.context";
import { UserDispatchContext, UserContext } from "../../src/context/user.context";

const VideoComponent = dynamic(() => import("../../src/components/video"));
const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
		backgroundColor: "#FFF",
		minHeight: "calc(100vh - 390px)"
	},
	paper: {
		padding: theme.spacing(2),
		textAlign: "center",
		color: theme.palette.text.secondary,
		backgroundColor: "#E3E3E3",
		width: "100%",
		position: "relative",
		textAlign: "start",
		maxWidth: "500px",
		fontSize: "14px",
		color: "#484848",
		cursor: "pointer"
	},
	button: {
		background: "#ff8900",
		borderRadius: "0px 3px 3px 0px",
		position: "absolute",
		right: 0,
		top: 0,
		height: "100%"
	},
	arrow: {
		color: "#FFF"
	},
	favorite: {
		color: "#363636",
		marginRight: "5px"
	},
	create: {
		color: "#363636",
		marginRight: "5px"
	}
}));

const PreVenta = (props) => {
	const router = useRouter();
	const classes = useStyles();
	const dispatch = useContext(ProfileModalDispatch);
	const [ user, setUser ] = useState({ loggedIn: false, uid: "" });
	const [ profileArea, setProfileArea ] = useState("profileSection");
	const userDispatch = useContext(UserDispatchContext);
	const _user = useContext(UserContext);
	const { profile = {} } = _user;
	const { firstName = "", phone = "", email = "" } = profile;
	const fechUser = async () => {
		if (user.loggedIn) {
			const { uid = "" } = user;
			if (uid) {
				const _user = await getUserByUid(uid);
				const _likedLands = await getLikedLandByUid(uid);
				if (_user) {
					userDispatch({
						type: "UPDATE",
						payload: { profile: _user, likedLands: _likedLands }
					});
				}
			} else {
				router.push("/");
			}
		}
	};

	const handleClick = (type) => {
		dispatch({
			type: "UPDATE",
			payload: {
				open: true,
				modalCont: type,
				redirectTo: "checklist"
			}
		});
	};

	useEffect(
		() => {
			fechUser();
		},
		[ user ]
	);

	useEffect(() => {
		const unsubscribe = onAuthStateChange(setUser);
		return () => {
			unsubscribe();
		};
	}, []);

	const getProfileContent = () => {
		return (
			<React.Fragment>
				<Avatar
					className="avatar-profile"
					alt="Remy Sharp"
					src="https://firebasestorage.googleapis.com/v0/b/parcelapp-cl.appspot.com/o/multimedia%2Fusuario-8%20(1).png?alt=media&token=ca3b5929-7983-4be6-9d2a-337014d7120f"
				/>
				<h2 className="profile-name">{firstName}</h2>
				<h2 className="profile-email">{email}</h2>
				<h2 className="profile-phone">{phone}</h2>
			</React.Fragment>
		);
	};

	return (
		<div className={classes.root}>
			<Grid container spacing={3} className="special-cont-profile">
				<Grid item xs={12} className="ai-center d-flex column no-pd">
					<VideoComponent
						contClassName="full-width"
						width="100%"
						url="https://www.youtube.com/watch?v=DL_G2e1KQsE"
					/>
				</Grid>
				<Grid item xs={12} className="ai-center d-flex column pd-top-20">
					<div className="call-us-cont">
						<div className="call-us-text">Llámanos al</div>
						<div className="call-us-text">
							<strong>+56975790254</strong>
						</div>
						<div className="icon-floating-right">
							<img src="/Rectangulo 1833@2x.png" />
						</div>
					</div>
				</Grid>
				<Grid item xs={12} className="ai-center d-flex column">
					<div className="call-us-sub-text">
						o escríbenos a <strong>contacto@praderasdefrutillar.cl</strong>
					</div>
				</Grid>
				<Grid item xs={12} className="ai-center d-flex column">
					<Divider className="call-us-divider" />
				</Grid>
				<Grid item xs={12} className="ai-center d-flex column">
					<div className="call-us-text orange">Ahora, si ya he tomado la decisión de comprar,</div>
					<div className="call-us-text orange">
						<strong>¿Qué trámites tengo que hacer?</strong>
					</div>
				</Grid>
				<Grid item xs={12} className="ai-center d-flex column">
					<div className="call-us-info-box">
						<div className="smily-icon">
							<InsertEmoticon className="smily-cont-size" />
						</div>
						<div className="call-us-right-text">
							¡Fácil! tenemos todo detallado en esta misma APP, regístrate en menos de 20 segundos sin
							compromiso para acceder a esta y a mucha más información
						</div>
					</div>
				</Grid>
				<Grid item xs={12} className="ai-center d-flex column pd-bot-20">
					<Button
						type="submit"
						value="Send"
						className="call-us-register-btn"
						variant="contained"
						color="primary"
						onClick={() => handleClick("register")}
					>
						REGISTRARME
					</Button>
					<div className="call-us-login" onClick={() => handleClick("login")}>
						O <strong className="call-us-link"> INICIA SESIÓN</strong>
					</div>
				</Grid>
			</Grid>
			<ProfileModal />
		</div>
	);
};

export default PreVenta;
