import React, { useState, useEffect, useContext } from "react";
import { useRouter } from "next/router";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Divider from "@material-ui/core/Divider";
import { makeStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import Favorite from "@material-ui/icons/Favorite";
import ArrowRight from "@material-ui/icons/KeyboardArrowRight";
import ArrowDown from "@material-ui/icons/KeyboardArrowDown";
import EditIcon from "@material-ui/icons/Edit";
import Create from "@material-ui/icons/Create";

import { onAuthStateChange } from "../../src/firebase";
import ProfileModal from "../../src/components/modal/modalProfile";
import LikedLands from "../../src/components/content/likedLandsCont";
import { getUserByUid, getLikedLandByUid, updateUserByUid } from "../../src/services";
import { UserDispatchContext, UserContext } from "../../src/context/user.context";
import LandInfo from "../../src/components/modal/landInfo";

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
		backgroundColor: "#FFF",
		padding: "40px 0px",
		minHeight: "calc(100vh - 390px)"
	},
	paper: {
		padding: theme.spacing(2),
		textAlign: "center",
		color: theme.palette.text.secondary,
		backgroundColor: "#E3E3E3",
		width: "100%",
		position: "relative",
		textAlign: "start",
		maxWidth: "500px",
		fontSize: "14px",
		color: "#484848",
		cursor: "pointer"
	},
	button: {
		background: "#9ac200",
		borderRadius: "0px 3px 3px 0px",
		position: "absolute",
		right: 0,
		top: 0,
		height: "100%"
	},
	arrow: {
		color: "#FFF"
	},
	favorite: {
		color: "#363636",
		marginRight: "5px"
	},
	create: {
		color: "#363636",
		marginRight: "5px"
	},
	textField: {
		marginBottom: "10px",
		backgroundColor: "#fff",
		backgroundColor: "#F5F5F5",
		border: "3px solid #D9D9D9",
		borderRadius: "8px",
		maxWidth: "400px",
		"& label.Mui-focused": {
			color: "#ee7f0e"
		},
		"& .MuiInput-underline:after": {
			borderBottomColor: "green"
		},
		"& .MuiOutlinedInput-root": {
			"&.Mui-focused fieldset": {
				borderColor: "#ee7f0e"
			}
		}
	},
	"@media (max-width: 1070px)": {
		paper: {
			padding: "10px"
		}
	}
}));

const Index = (props) => {
	const router = useRouter();
	const classes = useStyles();
	const [ user, setUser ] = useState({ loggedIn: false, uid: "" });
	const [ editMode, setEditMode ] = useState(false);
	const [ profileArea, setProfileArea ] = useState("profileSection");
	const userDispatch = useContext(UserDispatchContext);
	const _user = useContext(UserContext);
	const { profile = {} } = _user;
	const { firstName = "", phone = "", email = "", customerId } = profile;
	const fechUser = async () => {
		if (user.loggedIn) {
			const { uid = "" } = user;
			if (uid) {
				const _user = await getUserByUid(uid);
				const _likedLands = await getLikedLandByUid(uid);
				if (_user) {
					userDispatch({
						type: "UPDATE",
						payload: { profile: _user, likedLands: _likedLands }
					});
				}
			} else {
				router.push("/");
			}
		}
	};

	useEffect(
		() => {
			fechUser();
		},
		[ user ]
	);

	useEffect(() => {
		const unsubscribe = onAuthStateChange(setUser);
		return () => {
			unsubscribe();
		};
	}, []);

	const getProfileContent = () => {
		return (
			<React.Fragment>
				<Avatar
					className="avatar-profile"
					alt="Remy Sharp"
					src="https://firebasestorage.googleapis.com/v0/b/parcelapp-cl.appspot.com/o/multimedia%2Fusuario-8%20(1).png?alt=media&token=ca3b5929-7983-4be6-9d2a-337014d7120f"
				/>

				<h2 className="profile-name">{firstName ? firstName : "Nombre"}</h2>
				<h2 className="profile-email">Email: {email ? email : "Email"}</h2>
				<h2 className="profile-phone">Teléfono: {phone ? phone : "+56 9"}</h2>
			</React.Fragment>
		);
	};

	const renderProfileArea = () => {
		switch (profileArea) {
			case "profileSection":
				return getProfileContent();
			case "likedLandsSection":
				return <LikedLands handleBack={() => setProfileArea("profileSection")} />;
			default:
				break;
		}
	};

	const updateProfile = (payload) => {
		console.log("payload", payload);
		userDispatch({
			type: "UPDATE",
			payload: {
				profile: {
					...profile,
					...payload
				}
			}
		});
	};

	const handleEditClick = () => {
		if (editMode) {
			updateUserByUid({
				customerId: customerId,
				firstName: firstName,
				phone: phone,
				email: email
			});
			setEditMode(false);
		} else {
			setEditMode(true);
		}
	};

	const renderProfileForm = () => {
		if (editMode) {
			return (
				<React.Fragment>
					<TextField
						id="name-outlined-basic"
						name="email"
						className={classes.textField}
						label="Email"
						variant="outlined"
						fullWidth
						value={email}
						onChange={(e) => updateProfile({ emal: e.target.value })}
					/>
					<TextField
						id="name-outlined-basic"
						name="name"
						className={classes.textField}
						label="Nombre"
						variant="outlined"
						fullWidth
						value={firstName}
						onChange={(e) => updateProfile({ firstName: e.target.value })}
					/>
					<TextField
						id="name-outlined-basic"
						name="phone"
						className={classes.textField}
						label="Telefono"
						variant="outlined"
						fullWidth
						value={phone}
						onChange={(e) => updateProfile({ phone: e.target.value })}
					/>
				</React.Fragment>
			);
		}
		return (
			<React.Fragment>
				<h2 className="profile-name">{firstName ? firstName : "Nombre"}</h2>
				<h2 className="profile-email">
					<strong>Email:</strong> {email ? email : "Email"}
				</h2>
				<h2 className="profile-phone">
					<strong>Teléfono:</strong> {phone ? phone : "+56 9"}
				</h2>
			</React.Fragment>
		);
	};

	return (
		<div className={classes.root}>
			<Grid container spacing={3} className="special-cont-profile">
				<Grid item xs={12} className="ai-center d-flex column">
					<div className="profile-header-title">MI PERFIL DE USUARIO</div>
					<Avatar
						className="avatar-profile"
						alt="Remy Sharp"
						src="https://firebasestorage.googleapis.com/v0/b/parcelapp-cl.appspot.com/o/multimedia%2Fusuario-8%20(1).png?alt=media&token=ca3b5929-7983-4be6-9d2a-337014d7120f"
					/>
					{renderProfileForm()}
				</Grid>
				<Grid item xs={12} className="ai-center d-flex column">
					<Divider className="profile-divider" />
				</Grid>
				<Grid item xs={12} className="ai-center d-flex column">
					<Button
						variant="outlined"
						endIcon={<EditIcon />}
						className="edit-btn-profle"
						onClick={() => handleEditClick()}
					>
						{!editMode ? "Editar" : "Guardar"}
					</Button>
				</Grid>
				<Grid item xs={12} sm={6} className="ai-center d-flex column mobile-only">
					<Paper className={classes.paper}>
						<div className="d-flex jc-start ai-center">
							<Favorite className={classes.favorite} />LISTA DE PARCELAS
						</div>
						<Button className={classes.button}>
							<ArrowDown className={classes.arrow} />
						</Button>
					</Paper>
				</Grid>
				<Grid item xs={12} className="ai-center d-flex column">
					<LikedLands handleBack={() => setProfileArea("profileSection")} />
				</Grid>
				<Grid item xs={12} sm={6} className="ai-center d-flex column mobile-only">
					<Paper className={classes.paper} onClick={() => router.push("/checklist")}>
						<div className="d-flex jc-start ai-center">
							<Create className={classes.favorite} />MI CHECKLIST DE COMPRA
						</div>
						<Button className={classes.button}>
							<ArrowRight className={classes.arrow} />
						</Button>
					</Paper>
				</Grid>
				{/* {profileArea !== "likedLandsSection" && (
					<Grid item xs={12} sm={6} className="ai-center d-flex column">
						<Paper className={classes.paper} onClick={() => setProfileArea("likedLandsSection")}>
							<div className="d-flex jc-start ai-center">
								<Favorite className={classes.favorite} />LISTA DE PARCELAS
							</div>
							<Button className={classes.button}>
								<ArrowRight className={classes.arrow} />
							</Button>
						</Paper>
					</Grid>
				)} */}
			</Grid>
			{/* <ProfileModal /> */}
		</div>
	);
};

export default Index;
