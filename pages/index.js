import React, { useState, useEffect, useContext } from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { useRouter } from "next/router";
import dynamic from "next/dynamic";
import Slider from "../src/components/slider";
import HomeMenu from "../src/components/menu/homeMenu";
import Divider from "../src/components/divider/simpleDivider";
import GalleryCont from "../src/components/content/galleryCont";
import Testimony from "../src/components/content/testimony";
import Contact from "../src/components/content/contact";
import Caracteristics from "../src/components/content/caracteristicsCont";
import SalesCont from "../src/components/content/salesCont";
import NearPlaces from "../src/components/content/nearPlacesCont";
import ProfileModal from "../src/components/modal/modalProfile";
import KnowUsText from "../src/components/content/knowUsText";
import firebase, { onAuthStateChange } from "../src/firebase";
import { DispatchContext } from "../src/context/multimedia.context";
import { ProfileModalDispatch } from "../src/context/profileModal.context";
import { TestimonyDispatchContext } from "../src/context/testimony.context";
import { UserDispatchContext } from "../src/context/user.context";
import { sendEvent } from "../src/utils/googleTagManager";
import { auth } from "../src/firebase";
import {
	getUserByUid,
	getLikedLandByUid,
	getProjectFeaturesCollection,
	getSliderCollection,
	getNearPlacesCollection,
	getOurLocationCollection,
	getOthersCollection,
	getOffersCollection
} from "../src/services";

const SimpleTextCont = dynamic(() => import("../src/components/content/simpleTextCont"));
const SimpleIconCont = dynamic(() => import("../src/components/content/iconCont"));
const SelectArea = dynamic(() => import("../src/components/content/selectAreaCont"));

const Index = (props) => {
	const router = useRouter();
	const [ user, setUser ] = useState({ loggedIn: false, uid: "" });
	const dispatch = useContext(DispatchContext);
	const dispatchProfileModal = useContext(ProfileModalDispatch);
	const testimonyDispatch = useContext(TestimonyDispatchContext);
	const userDispatch = useContext(UserDispatchContext);

	const fechDataMultemedia = async () => {
		const db = firebase.firestore();
		const data = await db.collection("multimedia").get();
		const salesLands = await db.collection("Oferta_Parcelas").get();
		const caracteristics = await db.collection("Características_del_proyecto").get();
		const nearPlaces = await db.collection("Lugares_Cercanos").get();
		const ourUbication = await db.collection("Nuestra_Ubicación").get();
		console.log("ourUbication.docs", ourUbication.docs);
		console.log("onSales.docs", salesLands.docs);

		// dispatch({
		// 	type: "UPDATE",
		// 	payload: {
		// 		sales: [ ...salesLands.docs.map((item) => item.data()) ],
		// 		oldData: [ ...data.docs.map((doc) => doc.data()) ],
		// 		caracteristics: [ ...caracteristics.docs.map((carac) => carac.data()) ],
		// 		nearPlaces: [ ...nearPlaces.docs.map((item) => item.data()) ],
		// 		ourUbication: [ ...ourUbication.docs.map((item) => item.data()) ],
		// 		sales2: [ ...salesLands.docs.map((item) => item.data()) ]
		// 	}
		// });
	};

	const fechDataTestimony = async () => {
		const db = firebase.firestore();
		const data = await db.collection("testimonials").get();
		// console.log("data.docs.map((doc) => doc.data())", data.docs.map((doc) => doc.data()));
		testimonyDispatch({
			type: "UPDATE",
			payload: data.docs.map((doc) => doc.data())
		});
	};

	const fechUser = async () => {
		if (user.loggedIn) {
			const { uid = "" } = user;
			if (uid) {
				const _user = await getUserByUid(uid);
				const _likedLands = await getLikedLandByUid(uid);
				console.log("uid!!!!!", uid);
				console.log("_likedLands", _likedLands);
				if (_user) {
					userDispatch({
						type: "UPDATE",
						payload: { profile: _user, likedLands: _likedLands }
					});
				}
			}
		}
	};

	const fetchMultimediaContent = async (sectionName, serviceGetData) => {
		const _content = (await serviceGetData()) || [];
		console.log("_content", _content);
		if (_content) {
			const payload = {};
			payload[sectionName] = _content;
			console.log("payload first try", payload);
			dispatch({
				type: "UPDATE",
				payload: payload
			});
		}
	};

	useEffect(() => {
		fechDataMultemedia();
		fechDataTestimony();
		fetchMultimediaContent("mainSliders", getSliderCollection);
		fetchMultimediaContent("nearPlaces", getNearPlacesCollection);
		fetchMultimediaContent("caracteristics", getProjectFeaturesCollection);
		fetchMultimediaContent("ourUbication", getOurLocationCollection);
		fetchMultimediaContent("others", getOthersCollection);
		fetchMultimediaContent("sales", getOffersCollection);
	}, []);

	useEffect(
		() => {
			fechUser();
		},
		[ user ]
	);

	useEffect(() => {
		if (router.pathname === "/privacy-policy") {
			router.push("/privacy-policy");
		}
		const unsubscribe = onAuthStateChange(setUser);
		return () => {
			unsubscribe();
		};
	}, []);

	useEffect(() => {
		const { query = {} } = router;
		const { login = "" } = query;
		if (login === "open") {
			dispatchProfileModal({
				type: "UPDATE",
				payload: {
					open: true
				}
			});
		}
	}, []);

	return (
		<React.Fragment>
			<div id="home">
				<Slider />
			</div>
			<div className="desktop-only">
				<HomeMenu />
				<section>
					<SimpleTextCont />
				</section>
				<Divider />
				<section>
					<SimpleIconCont />
				</section>
			</div>
			<div className="mobile-only">
				<KnowUsText />
				{/* <BtnPanelMobile /> */}
			</div>
			<div className="bg-grey">
				<Caracteristics />
			</div>
			<SalesCont />
			<div className="bg-grey">
				<GalleryCont />
			</div>
			<NearPlaces />
			<div className="bg-grey">
				<SelectArea />
			</div>
			<div className="bg-grey">{/* <BankFees /> */}</div>

			{/* <Testimony /> */}

			{/* <div className="bg-grey"> */}
			<Contact />
			{/* </div> */}
			<div
				id="whatsapp-button"
				className="stickyWhatsappCont"
				onClick={() => {
					sendEvent("whatsapp-button-click");
					window.open(
						"https://wa.me/56975790254?text=¡Hola!%20estoy%20en%20https://praderasdefrutillar.cl%20mi%20nombre%20es:"
					);
				}}
			>
				<LazyLoadImage src="/sticky_whatsapp.png" alt="whatsapp_logo" />
			</div>
			<ProfileModal />
		</React.Fragment>
	);
};

export default Index;
