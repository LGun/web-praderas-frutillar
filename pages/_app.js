import React from "react";
import { withRouter } from "next/router";
import App, { Container } from "next/app";
import Head from "next/head";
import { MuiThemeProvider } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
// import 'bootstrap/dist/css/bootstrap.min.css'
import "./main.css";
import "./image-gallery.css";
import theme from "../src/utils/theme";
import NavBar from "../src/components/navbar/desktopNav";
import NavBarMobile from "../src/components/navbar/index";
import FooterComponent from "../src/components/footer";
import { LandProvider } from "../src/context/land.context";
import { MultimediaProvider } from "../src/context/multimedia.context";
import { StepProvider } from "../src/context/step.context";
import { UserProvider } from "../src/context/user.context";
import { TestimonyProvider } from "../src/context/testimony.context";
import { ProfileModalProvider } from "../src/context/profileModal.context";
import { ModalProvider } from "../src/context/modal.context";

class _App extends App {
	static async getInitialProps({ Component, ctx }) {
		return {
			pageProps: Component.getInitialProps ? await Component.getInitialProps(ctx) : {}
		};
	}
	// fetchData = async () => {};
	componentDidMount() {
		const jssStyles = document.querySelector("#jss-server-side");
		if (jssStyles && jssStyles.parentNode) {
			jssStyles.parentNode.removeChild(jssStyles);
		}
	}

	render() {
		const { Component, pageProps, store } = this.props;

		return (
			<React.Fragment>
				<Head>
					<meta charSet="utf-8" />
					<meta
						name="viewport"
						content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no"
					/>
					<meta name="robots" content="”index, follow" />
					<title>Praderas de Frutillar</title>
					//SEO GOOGLE
					<meta
						name="description"
						content="Nuestro proyecto FUNDO PRADERAS DE FRUTILLAR, es una parcelación a 5 minutos del centro de Frutillar, X Región de los Lagos"
					/>
					// FACEBOOK
					<meta property="og:title" content="Praderas de Frutillar" key="title" />
					<meta
						property="og:description"
						content="Nuestro proyecto FUNDO PRADERAS DE FRUTILLAR, es una parcelación a 5 minutos del centro de Frutillar, X Región de los Lagos"
						key="description"
					/>
					<meta property="og:url" content="https://praderasdefrutillar.cl/" key="url" />
					<meta
						property="og:image"
						itemprop="image"
						content="https://firebasestorage.googleapis.com/v0/b/parcelapp-cl.appspot.com/o/multimedia%2Fimg-lugares-06-PU-100.jpg?alt=media&token=f18bf6d0-d782-4ee6-babe-21f1a4fd4d4f"
						key="image"
					/>
					<meta property="og:site_name" content="Praderas de frutillar" key="site_name" />
				</Head>
				<MuiThemeProvider theme={theme}>
					<UserProvider>
						<TestimonyProvider>
							<StepProvider>
								<LandProvider>
									<MultimediaProvider>
										<CssBaseline />
										<ModalProvider>
											<ProfileModalProvider>
												<NavBar />
												<NavBarMobile />
												<Component {...pageProps} />
											</ProfileModalProvider>
										</ModalProvider>
									</MultimediaProvider>
								</LandProvider>
							</StepProvider>
						</TestimonyProvider>
					</UserProvider>
				</MuiThemeProvider>
				<FooterComponent />
			</React.Fragment>
		);
	}
}

export default withRouter(_App);
