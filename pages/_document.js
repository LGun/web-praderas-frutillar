import React, { Fragment } from "react";
import Document, { Head, Main, NextScript, Html } from "next/document";
import { ServerStyleSheets } from "@material-ui/styles";
import theme from "../src/utils/theme";

class _Document extends Document {
	render() {
		return (
			<Html lang="es" dir="ltr">
				<Head>
					<script
						dangerouslySetInnerHTML={{
							__html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','GTM-PWFWPHS');`
						}}
						async
					/>
					<link
						rel="stylesheet"
						type="text/css"
						charSet="UTF-8"
						href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css"
						defer
					/>
					<link
						rel="stylesheet"
						type="text/css"
						href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css"
					/>
					<link
						rel="stylesheet"
						href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
						integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
						crossOrigin="anonymous"
						defer
					/>

					<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDxlGyPFi4S8S5XU-6GSodqwDfBHB2jH44&callback=initMap" />
					<script src="https://polyfill.io/v3/polyfill.min.js?features=default" defer />
					<script src="/mapPolygonCenter.js" defer />
				</Head>
				<body>
					<noscript
						dangerouslySetInnerHTML={{
							__html: `<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PWFWPHS"
        height="0" width="0" style="display:none;visibility:hidden"></iframe>`
						}}
					/>
					<Main />
					<NextScript />
				</body>
			</Html>
		);
	}
}

_Document.getInitialProps = async (ctx) => {
	const sheets = new ServerStyleSheets();
	const originalRenderPage = ctx.renderPage;

	ctx.renderPage = () =>
		originalRenderPage({
			enhanceApp: (WrappedComponent) => (props) => sheets.collect(<WrappedComponent {...props} />)
		});

	const initialProps = await Document.getInitialProps(ctx);

	return {
		...initialProps,
		styles: (
			<Fragment>
				{initialProps.styles}
				{sheets.getStyleElement()}
			</Fragment>
		)
	};
};

export default _Document;
