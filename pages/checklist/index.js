import React, { useState, useEffect, useContext } from "react";
import { useRouter } from "next/router";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";
import { makeStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";

import { onAuthStateChange } from "../../src/firebase";
import StepModal from "../../src/components/modal/stepModal";
import LikedLands from "../../src/components/content/likedLandsCont";
import { getUserByUid, getLikedLandByUid } from "../../src/services";
import { UserDispatchContext, UserContext } from "../../src/context/user.context";
import ChecklistTable from "../../src/components/table/checklistTable";
import { StepModalProvider } from "../../src/context/stepModal.context";
import { STEP_TITLE } from "../../src/utils/constants";
import LoaderComponent from "../../src/components/loader";
import ChecklistMobile from "../../src/components/content/checkListMobile";
const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
		backgroundColor: "#FFF",
		padding: "40px 0px",
		minHeight: "calc(100vh - 390px)"
	},
	paper: {
		padding: theme.spacing(2),
		textAlign: "center",
		color: theme.palette.text.secondary,
		backgroundColor: "#E3E3E3",
		width: "100%",
		position: "relative",
		textAlign: "start",
		maxWidth: "500px",
		fontSize: "14px",
		color: "#484848",
		cursor: "pointer"
	},
	button: {
		background: "#ff8900",
		borderRadius: "0px 3px 3px 0px",
		position: "absolute",
		right: 0,
		top: 0,
		height: "100%"
	},
	arrow: {
		color: "#FFF"
	},
	favorite: {
		color: "#363636",
		marginRight: "5px"
	},
	create: {
		color: "#363636",
		marginRight: "5px"
	}
}));

const CheckListBouth = (props) => {
	const router = useRouter();
	const classes = useStyles();
	const [ loading, setLoading ] = useState(true);
	const [ user, setUser ] = useState({ loggedIn: false, uid: "" });
	const [ profileArea, setProfileArea ] = useState("profileSection");
	const userDispatch = useContext(UserDispatchContext);
	const _user = useContext(UserContext);
	const { profile = {} } = _user;
	const { firstName = "", phone = "", email = "", stepNumber = 0 } = profile;
	const fechUser = async () => {
		console.log("user.loggedIn", user);
		if (user.loggedIn) {
			console.log("__user", user);
			const { uid = "" } = user;
			console.log("__uid", uid);
			if (uid) {
				console.log("__uid2", uid);
				const _user = await getUserByUid(uid);
				const _likedLands = await getLikedLandByUid(uid);
				if (_user) {
					userDispatch({
						type: "UPDATE",
						payload: { profile: _user, likedLands: _likedLands }
					});
				}
			} else {
				router.push({ pathname: "/", query: { login: "open" } });
			} 
		}
	};

	const _fetchUser = async () => {
		setLoading(true);
		await fechUser();
		setLoading(false);
	};

	useEffect(
		() => {
			_fetchUser();
		},
		[ user ]
	);

	useEffect(() => {
		const unsubscribe = onAuthStateChange(setUser);
		if(!localStorage.getItem("customerId")) {
			router.push({ pathname: "/", query: { login: "open" } });
		}
	}, []);

	const getProfileContent = () => {
		return (
			<React.Fragment>
				<Avatar
					className="avatar-profile"
					alt="Remy Sharp"
					src="https://firebasestorage.googleapis.com/v0/b/parcelapp-cl.appspot.com/o/multimedia%2Fusuario-8%20(1).png?alt=media&token=ca3b5929-7983-4be6-9d2a-337014d7120f"
				/>
				<h2 className="profile-name">{firstName}</h2>
				<h2 className="profile-email">{email}</h2>
				<h2 className="profile-phone">{phone}</h2>
			</React.Fragment>
		);
	};

	const renderProfileArea = () => {
		switch (profileArea) {
			case "profileSection":
				return getProfileContent();
			case "likedLandsSection":
				return <LikedLands handleBack={() => setProfileArea("profileSection")} />;
			default:
				break;
		}
	};

	return loading ? (
		<LoaderComponent />
	) : (
		<div className={classes.root}>
			<StepModalProvider>
				<Grid container spacing={3} className="special-cont-profile">
					<Grid item xs={12} className="ai-center d-flex column">
						<div className="profile-header-title">MI CHECKLIST DE COMPRA</div>
						<p className="checklist-paragraph">
							En este checklist te detallamos todo el proceso de compra, paso a paso, de forma
							transparente. Te acompañamos para hacer las cosas de forma eficiente. Estarás cubierto legal
							y comercialmente. Sabemos que juntos estamos construyendo tu sueño
						</p>
					</Grid>
					<Grid item xs={12} className="ai-center d-flex column no-pd-mo">
						<div className="checklist-state-cont">
							ESTADO |{" "}
							<span className="checklist-title-text">{`${stepNumber + 1} ${STEP_TITLE[
								stepNumber
							]}`}</span>
						</div>
					</Grid>
					<Grid item xs={12} className="ai-center d-flex column">
						<Divider className="profile-divider" />
					</Grid>
					<Grid item xs={12} sm={12} className="ai-center d-flex column desktop-only">
						<ChecklistTable />
					</Grid>
					<Grid item xs={12} sm={12} className="ai-center d-flex column mobile-only-checklist">
						<ChecklistMobile />
					</Grid>
				</Grid>
				<StepModal />
			</StepModalProvider>
		</div>
	);
};

export default CheckListBouth;
