import React, { useContext, useEffect, useState } from "react";
import { INCREMENT, DECREMENT } from "../constants";
import { ModalDispatchContext, ModalContext } from "../context/modal.context";

export const increment = (isServer) => {
	return (dispatch) => {
		dispatch({
			type: INCREMENT,
			from: isServer ? "server" : "client"
		});
	};
};

export const decrement = (isServer) => {
	return (dispatch) => {
		dispatch({
			type: DECREMENT,
			from: isServer ? "server" : "client"
		});
	};
};

export const setModalOpen = (value, modalDispatch) => {
	return () =>
		modalDispatch({
			type: "UPDATE",
			payload: {
				open: value,
				test: "asdasda"
			}
		});
};
