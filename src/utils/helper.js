import _ from "lodash";

export const ValidateEmail = (email) => {
	const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(String(email).toLowerCase());
};

export const formatLatLong = (arrayOfLatLng) => {
	if (typeof arrayOfLatLng !== "undefined") {
		return arrayOfLatLng.map((latlong) => {
			if (latlong && typeof latlong !== "undefined") {
				const splited = latlong.split(",");
				if (splited.length > 0) {
					return { lat: Number(splited[0]), lng: Number(splited[1]) };
				}
			}
		});
	}
};

export const sanitizeMultimediaData = (data, value) => {
	let filteredBySection = _.filter(data, function(multimediaItem) {
		if (typeof multimediaItem.sections !== "undefined") {
			return multimediaItem.sections.includes(value) && multimediaItem.active;
		}
	});
	filteredBySection = filteredBySection.map((item) => {
		item.order = item.title.charAt(0);
		return item;
	});
	filteredBySection = _.orderBy(filteredBySection, [ "order" ], [ "asc" ]);
	return filteredBySection;
};

export const orderMultimediaByTitle = (data) => {
	let filteredBySection = data.map((item) => {
		item.order = item.title.charAt(0);
		return item;
	});
	filteredBySection = _.orderBy(filteredBySection, [ "order" ], [ "asc" ]);
	console.log("filteredBySection", filteredBySection);
	return filteredBySection;
};

export const getLandIcons = (land) => {
	const trees = land.trees ? <img className="small-icon-image" src="/Rectangulo 1779.png" /> : null;
	const volcano =
		land.volcanoView === "Si" || land.volcanoView === "Parcial" ? (
			<img className="small-icon-image" src="/volcan.svg" />
		) : null;
	const corner = land.cornerLand ? <img className="small-icon-image" src="/Rectangulo 1827@2x.png" /> : null;
	const discount = land.discount > 0 ? <img className="small-icon-image" src="/Rectangulo 1828@2x.png" /> : null;
	return [ volcano, trees, corner, discount ];
};

export const getErrorMessageByCodeLogin = (errorCode) => {
	switch (errorCode) {
		case "auth/wrong-password":
			return "Tu contraseña no es correcta. Vuelve a comprobarla.";
		case "auth/user-not-found":
			return "Usuario y contraseña no son correctos.";
		default:
			return "Tu contraseña no es correcta. Vuelve a comprobarla.";
	}
};

export const getErrorMessageByCodeRegister = (errorCode) => {
	switch (errorCode) {
		case "auth/email-already-in-use":
			return "El email esta en uso";
		case "auth/argument-error":
			return "Debes ingresar todos los campos";
		case "auth/invalid-email":
			return "El email es invalido";
		default:
			return errorCode;
	}
};
