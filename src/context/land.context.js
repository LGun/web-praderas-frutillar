import React, { createContext, useReducer } from "react";
import landReducer from "../reducers/land.reducer";
const defaultLand = [];

export const LandContext = createContext();
export const DispatchContext = createContext();

export function LandProvider(props) {
	const [ land, dispatch ] = useReducer(landReducer, defaultLand);
	return (
		<DispatchContext.Provider value={dispatch}>
			<LandContext.Provider value={land}>{props.children}</LandContext.Provider>
		</DispatchContext.Provider>
	);
}
