import React, { createContext, useReducer } from "react";
import userReducer from "../reducers/user.reducer";
const defaultUser = { likedLands: [], profile: {} };

export const UserContext = createContext();
export const UserDispatchContext = createContext();

export function UserProvider(props) {
	const [ user, dispatch ] = useReducer(userReducer, defaultUser);
	return (
		<UserDispatchContext.Provider value={dispatch}>
			<UserContext.Provider value={user}>{props.children}</UserContext.Provider>
		</UserDispatchContext.Provider>
	);
}
