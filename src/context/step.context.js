import React, { createContext, useReducer } from "react";
import stepReducer from "../reducers/step.reducer";
import { STEPPS } from "../utils/constants";
const defaultStep = { name: "" };

export const StepContext = createContext();
export const DispatchContext = createContext();

export function StepProvider(props) {
	const [ step, dispatch ] = useReducer(stepReducer, defaultStep);
	return (
		<DispatchContext.Provider value={dispatch}>
			<StepContext.Provider value={step}>{props.children}</StepContext.Provider>
		</DispatchContext.Provider>
	);
}
