import React, { createContext, useReducer } from "react";
import stepModalReducer from "../reducers/stepModal.reducer";
const defaultStepModal = { open: false, landNumber: "" };

export const StepModalContext = createContext();
export const StepModalDispatchContext = createContext();

export function StepModalProvider(props) {
	const [ modal, dispatch ] = useReducer(stepModalReducer, defaultStepModal);
	return (
		<StepModalDispatchContext.Provider value={dispatch}>
			<StepModalContext.Provider value={modal}>{props.children}</StepModalContext.Provider>
		</StepModalDispatchContext.Provider>
	);
}
