import React, { createContext, useReducer } from "react";
import testimonyReducer from "../reducers/testimony.reducer";
const defaultTestimony = [];

export const TestimonyContext = createContext();
export const TestimonyDispatchContext = createContext();

export function TestimonyProvider(props) {
	const [ testimony, dispatch ] = useReducer(testimonyReducer, defaultTestimony);
	return (
		<TestimonyDispatchContext.Provider value={dispatch}>
			<TestimonyContext.Provider value={testimony}>{props.children}</TestimonyContext.Provider>
		</TestimonyDispatchContext.Provider>
	);
}
