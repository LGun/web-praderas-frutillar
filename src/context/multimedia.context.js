import React, { createContext, useReducer } from "react";
import multimediaReducer from "../reducers/multimedia.reducer";
const defaultMultimedia = { sales: [], caracteristics: [], nearPlaces: [], oldData: [], ourUbication: [] };

export const MultimediaContext = createContext();
export const DispatchContext = createContext();

export function MultimediaProvider(props) {
	const [ multimedia, dispatch ] = useReducer(multimediaReducer, defaultMultimedia);
	return (
		<DispatchContext.Provider value={dispatch}>
			<MultimediaContext.Provider value={multimedia}>{props.children}</MultimediaContext.Provider>
		</DispatchContext.Provider>
	);
}
