import React, { createContext, useReducer } from "react";
import modalReducer from "../reducers/profileModal.reducer";
const defaultModal = { open: false, modalCont: "login" };
export const ProfileModalContext = createContext();
export const ProfileModalDispatch = createContext();

export function ProfileModalProvider(props) {
	const [ modal, dispatch ] = useReducer(modalReducer, defaultModal);
	return (
		<ProfileModalDispatch.Provider value={dispatch}>
			<ProfileModalContext.Provider value={modal}>{props.children}</ProfileModalContext.Provider>
		</ProfileModalDispatch.Provider>
	);
}
