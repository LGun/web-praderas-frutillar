import React, { createContext, useReducer, useState } from "react";
import modalReducer from "../reducers/modal.reducer";
const defaultModal = { open: false, land: {}, landNumber: "" };

export const ModalContext = createContext();
export const ModalDispatchContext = createContext();

export function ModalProvider(props) {
	const [ modal, dispatch ] = useReducer(modalReducer, defaultModal);
	return (
		<ModalDispatchContext.Provider value={dispatch}>
			<ModalContext.Provider value={modal}>{props.children}</ModalContext.Provider>
		</ModalDispatchContext.Provider>
	);
}
