import firebase from "../firebase";
import { PROJECT_DEFAULT } from "../utils/constants";
import _ from "lodash";
// const projectFeaturesCollection = db.collection("content-project-features");
// const landFeaturesCollection = db.collection("content-land-features");
// const sliderCollection = db.collection("content-slider");
// const connectivityCollection = db.collection("content-connectivity");
// const purchaseLoggedCollection = db.collection("content-purchase-logged");
// const purchaseNotLoggedCollection = db.collection("content-purchase-not-logged");
// const knowCollection = db.collection("content-know");
// const nearPlacesCollection = db.collection("content-near-places");
// const ourLocationCollection = db.collection("content-our-Location");
// const landOffersCollection = db.collection("content-land-offers");
// const othersCollection = db.collection("content-others");

export const getNearPlacesCollection = async () => {
	const db = firebase.firestore();
	let multimediaRef = await db
		.collection("content-near-places")
		.where("projectId", "==", PROJECT_DEFAULT.value)
		.get()
		.then((snapshot) => {
			if (snapshot.empty) {
				return;
			}
			if (snapshot.docs.length > 0) {
				return snapshot.docs.map((_doc) => _doc.data());
			}
			return {};
		})
		.catch((err) => {
			console.log("Error getting documents", err);
		});
	const response = _.orderBy(multimediaRef, [ "position" ], [ "asc" ]);
	return response;
};

export const getOffersCollection = async () => {
	const db = firebase.firestore();
	let multimediaRef = await db
		.collection("content-land-offers")
		.where("projectId", "==", PROJECT_DEFAULT.value)
		.get()
		.then((snapshot) => {
			if (snapshot.empty) {
				return;
			}
			if (snapshot.docs.length > 0) {
				return snapshot.docs.map((_doc) => _doc.data());
			}
			return {};
		})
		.catch((err) => {
			console.log("Error getting documents", err);
		});
	const response = _.orderBy(multimediaRef, [ "position" ], [ "asc" ]);
	return response;
};

export const getLandsCollection = async () => {
	const db = firebase.firestore();
	let multimediaRef = await db
		.collection("lands")
		.where("projectId", "==", PROJECT_DEFAULT.value)
		.get()
		.then((snapshot) => {
			if (snapshot.empty) {
				return;
			}
			if (snapshot.docs.length > 0) {
				return snapshot.docs.map((_doc) => _doc.data());
			}
			return {};
		})
		.catch((err) => {
			console.log("Error getting documents", err);
		});
	return multimediaRef;
};

export const getLandsByProjectId = async () => {
	const db = firebase.firestore();
	let lands = await db
		.collection("lands")
		.where("projectId", "==", PROJECT_DEFAULT.value)
		.get()
		.then((snapshot) => {
			if (snapshot.empty) {
				console.log("No matching documents.");
				return [];
			}
			if (snapshot.docs.length > 0) {
				return [ ...snapshot.docs.map((doc) => doc.data()) ];
			}
			return [];
		})
		.catch((err) => {
			console.log("Error getting documents", err);
			return [];
		});
	return lands;
};

export const getOurLocationCollection = async () => {
	const db = firebase.firestore();
	let multimediaRef = await db
		.collection("content-our-Location")
		.where("projectId", "==", PROJECT_DEFAULT.value)
		.get()
		.then((snapshot) => {
			if (snapshot.empty) {
				console.log("No matching documents.");
				return;
			}
			if (snapshot.docs.length > 0) {
				return snapshot.docs.map((_doc) => _doc.data());
			}
			return {};
		})
		.catch((err) => {
			console.log("Error getting documents getOurLocationCollection", err);
		});
	return multimediaRef;
};

export const getOthersCollection = async () => {
	const db = firebase.firestore();
	let multimediaRef = await db
		.collection("content-others")
		.where("projectId", "==", PROJECT_DEFAULT.value)
		.get()
		.then((snapshot) => {
			if (snapshot.empty) {
				console.log("No matching documents.");
				return;
			}
			if (snapshot.docs.length > 0) {
				return snapshot.docs.map((_doc) => _doc.data());
			}
			return {};
		})
		.catch((err) => {
			console.log("Error getting documents", err);
		});
	const response = _.orderBy(multimediaRef, [ "position" ], [ "asc" ]);
	return response;
};

export const getProjectFeaturesCollection = async () => {
	const db = firebase.firestore();
	let multimediaRef = await db
		.collection("content-project-features")
		.where("projectId", "==", PROJECT_DEFAULT.value)
		.get()
		.then((snapshot) => {
			if (snapshot.empty) {
				console.log("No matching documents.");
				return;
			}
			if (snapshot.docs.length > 0) {
				return snapshot.docs.map((_doc) => _doc.data());
			}
			return {};
		})
		.catch((err) => {
			console.log("Error getting documents", err);
		});
	let response = _.filter(multimediaRef, function(item) {
		return item.isMobile;
	});
	response = _.orderBy(response, [ "position" ], [ "asc" ]);
	return response;
};

export const getSliderCollection = async () => {
	const db = firebase.firestore();
	let finalResponse = [];
	let sliderRef = await db
		.collection("content-slider")
		.where("projectId", "==", PROJECT_DEFAULT.value)
		.get()
		.then((snapshot) => {
			if (snapshot.empty) {
				console.log("No matching documents.");
				return;
			}
			if (snapshot.docs.length > 0) {
				return snapshot.docs.map((_doc) => _doc.data());
			}
			return {};
		})
		.catch((err) => {
			console.log("Error getSliderCollection getting documents", err);
		});
	let dekstopGallery = _.filter(sliderRef, function(item) {
		return !item.isMobile;
	});
	dekstopGallery = _.orderBy(dekstopGallery, [ "position" ], [ "asc" ]);
	let mobileGallery = _.filter(sliderRef, function(item) {
		return item.isMobile;
	});
	mobileGallery = _.orderBy(mobileGallery, [ "position" ], [ "asc" ]);
	finalResponse = dekstopGallery.map((itemGallery) => {
		const specificImage =
			_.find(mobileGallery, function(o) {
				return itemGallery.title === o.title;
			}) || {};
		return { ...itemGallery, urlMobile: specificImage.urlMultimedia || "" };
	});
	console.log("getSliderCollection", finalResponse);
	return finalResponse;
};

export const getUserByUid = async (customerId) => {
	const db = firebase.firestore();
	let usersRef = await db
		.collection("customers")
		.where("customerId", "==", customerId)
		.get()
		.then((snapshot) => {
			if (snapshot.empty) {
				console.log("No matching documents.");
				return;
			}
			console.log("snapshot", snapshot);
			if (snapshot.docs.length > 0) {
				return snapshot.docs[0].data();
			}
			return {};
		})
		.catch((err) => {
			console.log("Error getting documents", err);
		});
	return usersRef;
};

export const updateUserByUid = async ({ customerId, email, phone, firstName }) => {
	if (customerId && email && phone && firstName) {
		const db = firebase.firestore();
		let usersRef = await db
			.collection("customers")
			.doc(customerId)
			.update({ email: email, phone: phone, firstName: firstName })
			.then((response) => {
				console.log(response);
				return response;
			})
			.catch((err) => {
				console.log("updateUserByUid: Error updating user", err);
			});
		return usersRef;
	}
	console.log("updateUserByUid: Error no data payload");
};

export const updateUser = async (payload) => {
	const { customerId = "" } = payload;
	if (typeof customerId !== "undefined" && customerId) {
		const db = firebase.firestore();
		let usersRef = await db
			.collection("customers")
			.doc(customerId)
			.update(payload)
			.then((response) => {
				console.log("updateUser", response);
				return response;
			})
			.catch((err) => {
				console.log("updateUserByUid: Error updating user", err);
			});
		return usersRef;
	}
};

export const getLikedLandByUid = async (customerId) => {
	const db = firebase.firestore();
	let wishListRef = await db
		.collection("usersWishList")
		.where("customerId", "==", customerId)
		.get()
		.then((snapshot) => {
			if (snapshot.empty) {
				console.log("No matching documents.");
				return;
			}
			console.log("snapshot wish List", snapshot);
			if (snapshot.docs.length > 0) {
				return [ ...snapshot.docs.map((doc) => doc.data().landNumber) ];
			}
			return [];
		})
		.catch((err) => {
			console.log("Error getting documents", err);
		});
	return wishListRef;
};

export const createUserLikedLand = async (payload) => {
	payload.projectId = PROJECT_DEFAULT.value;
	const db = firebase.firestore();
	const res = await db.collection("usersWishList").doc().set(payload);
	return res;
};

export const deleteUserLikedLand = async (payload) => {
	const db = firebase.firestore();
	let wishListRef = await db
		.collection("usersWishList")
		.where("customerId", "==", payload.customerId)
		.where("landNumber", "==", payload.landNumber)
		.get()
		.then((snapshot) => {
			if (snapshot.empty) {
				console.log("No matching documents.");
				return;
			}
			if (snapshot.docs.length > 0) {
				snapshot.forEach(function(doc) {
					doc.ref.delete();
				});
			}
			return [];
		})
		.catch((err) => {
			console.log("Error getting documents", err);
		});
	return wishListRef;
};

export const createUser = async (payload) => {
	payload.projectId = PROJECT_DEFAULT.value;
	payload.projectName = PROJECT_DEFAULT.label;
	const db = firebase.firestore();
	const res = await db.collection("customers").doc(payload.customerId).set(payload);
	return res;
};

export const uploadFile = async (payload) => {
	const storage = firebase.storage();
	try {
		console.log("payload", payload);
		let urlNewFile = "";
		const { customerId = "", fileName = "" } = payload;
		if (payload.file !== null && customerId) {
			const ref = storage.ref(`customers/${customerId}/${fileName}`);
			await ref.put(payload.file);
			urlNewFile = await ref.getDownloadURL();
		}
		return urlNewFile;
		/*
      const newPayload = { ...payload }
      newPayload.urlFileWarranty = urlFileWarranty
      newPayload.timeLastEdit = timestamp
      newPayload.timeLastStep = timestamp
      await fs.customersCollection.doc(payload.customerId).update(newPayload)
      commit(COMMIT, newPayload)
      */
	} catch (e) {
		console.error(e);
		return false;
	}
};
