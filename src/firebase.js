import firebase from "firebase";

const provider = new firebase.auth.GoogleAuthProvider();
// const config = {
// 	apiKey: "AIzaSyBg4rceeWeqOo0pxRnrUB0SR5QPTs-BtiI",
// 	authDomain: "parcelapp-cl.firebaseapp.com",
// 	databaseURL: "https://parcelapp-cl.firebaseio.com",
// 	projectId: "parcelapp-cl",
// 	storageBucket: "parcelapp-cl.appspot.com",
// 	messagingSenderId: "475551894180",
// 	appId: "1:475551894180:web:8b60ea8c00b3b108437431",
// 	measurementId: "G-56V66411NL"
// };
const config = {
	apiKey: "AIzaSyDxlGyPFi4S8S5XU-6GSodqwDfBHB2jH44",
	authDomain: "original-mesh-305921.firebaseapp.com",
	projectId: "original-mesh-305921",
	storageBucket: "original-mesh-305921.appspot.com",
	messagingSenderId: "639375421040",
	appId: "1:639375421040:web:abfbfd33a652aa494aceb5",
	measurementId: "G-DG798829KE"
};

if (!firebase.apps.length) {
	firebase.initializeApp(config);
}

export const auth = firebase.auth();

export const SignInWithGoogle = () => {
	return auth
		.signInWithPopup(provider)
		.then((result) => {
			console.log("result", result);
			return result;
		})
		.catch((error) => {
			console.log("error", error);
			return { error, status: 500 };
		});
};

export const SignOut = () => {
	return auth
		.signOut()
		.then((result) => {
			console.log("signOut", result);
			return result;
		})
		.catch((error) => {
			console.log("error", error);
			return { error, status: 500 };
		});
};

export const onAuthStateChange = (callback) => {
	return firebase.auth().onAuthStateChanged((user) => {
		if (user) {
			const { uid = "" } = user;
			console.log("The user is logged in", user);
			callback({ loggedIn: true, uid });
		} else {
			console.log("The user is not logged in");
			callback({ loggedIn: false });
		}
		return user;
	});
};

export default firebase;
