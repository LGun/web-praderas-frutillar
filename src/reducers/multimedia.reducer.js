const reducer = (state, action) => {
	switch (action.type) {
		case "UPDATE":
			return (state = { ...state, ...action.payload });
		default:
			return state;
	}
};

export default reducer;
