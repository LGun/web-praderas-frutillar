import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import InputBase from "@material-ui/core/InputBase";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import SearchIcon from "@material-ui/icons/Search";
import DirectionsIcon from "@material-ui/icons/Directions";
import { DispatchContext } from "../../context/step.context";

const useStyles = makeStyles((theme) => ({
	root: {
		padding: "2px 4px",
		display: "flex",
		alignItems: "center",
		width: 600,
		backgroundColor: "#EAEAEA",
		border: "0.5px solid #C8D1D3",
		boxShadow: "none"
	},
	input: {
		marginLeft: theme.spacing(1),
		flex: 1
	},
	iconButton: {
		padding: 10,
		color: "#FF8F1C"
	},
	divider: {
		height: 28,
		margin: 4,
		width: "2px",
		backgroundColor: "#FF8F1C"
	}
}));

export default function SearchAreaBtn() {
	const classes = useStyles();
	const dispatch = useContext(DispatchContext);

	const handleChange = (event) => {
		dispatch({
			type: "UPDATE",
			payload: {
				filtered: event.target.value
			}
		});
	};

	return (
		<Paper component="form" className={classes.root}>
			<InputBase
				className={classes.input}
				placeholder="Ingresa Nº parcela"
				inputProps={{ "aria-label": "Ingresa Nº parcela" }}
				onChange={(e) => handleChange(e)}
				type="number"
			/>
			<Divider className={classes.divider} orientation="vertical" />
			<IconButton color="primary" className={classes.iconButton} aria-label="directions">
				<SearchIcon />
			</IconButton>
		</Paper>
	);
}
