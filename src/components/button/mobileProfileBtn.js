import React, { useContext } from "react";
import { useRouter } from "next/router";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import Popover from "@material-ui/core/Popover";
import Divider from "@material-ui/core/Divider";
import MenuItem from "@material-ui/core/MenuItem";
import { makeStyles } from "@material-ui/core/styles";

import { SignOut } from "../../firebase";
import { UserContext } from "../../context/user.context";
import { ProfileModalDispatch } from "../../context/profileModal.context";
import { UserDispatchContext } from "../../context/user.context";

const useStyles = makeStyles({
	root: {
		width: "100vw"
	}
});

export default function MobileProfileBtn() {
	const router = useRouter();
	const classes = useStyles();
	const dispatch = useContext(ProfileModalDispatch);
	const userDispatch = useContext(UserDispatchContext);
	const user = useContext(UserContext);
	const { profile = {} } = user;
	const { firstName = "" } = profile;
	const [ anchorEl, setAnchorEl ] = React.useState(null);

	const handleClick = () => {
		dispatch({
			type: "UPDATE",
			payload: {
				open: true
			}
		});
	};

	const handleProfileClick = (event) => {
		setAnchorEl(event.currentTarget);
	};

	const handleClose = () => {
		setAnchorEl(0);
	};

	const handleSignOut = () => {
		SignOut();
		userDispatch({
			type: "UPDATE",
			payload: {
				profile: {}
			}
		});
		handleClose();
		router.push("/");
	};

	const goToProfile = () => {
		router.push("/perfil");
		handleClose();
	};
	const goToCheckList = () => {
		router.push("/checklist");
		handleClose();
	};
	const logOutComponent = (
		<AccountCircleIcon
			onClick={() => handleClick()}
			style={{ color: "#525252", fontSize: "27px" }}
			className="profile-icon"
		/>
	);
	const loggedInComponent = (
		<React.Fragment>
			<div className="logged-in-mobile-btn" onClick={handleProfileClick}>
				<AccountCircleIcon style={{ color: "#525252", fontSize: "27px" }} className="profile-icon" />
				<span className="mobile-btn-profile-text">{firstName}</span>
			</div>
			<Popover
				id="simple-menu"
				anchorEl={anchorEl}
				keepMounted
				open={Boolean(anchorEl)}
				onClose={() => handleClose()}
				anchorOrigin={{ vertical: 52 }}
				// transformOrigin={{ vertical: "top", horizontal: "center" }}
				PaperProps={{
					style: {
						borderRadius: "0px 0px 0px 0px",
						width: "100vw",
						maxWidth: "100vw",
						marginLeft: "16px",
						boxShadow: "0 4px 2px -2px rgba(0,0,0,0.2)"
					}
				}}
			>
				<div className={classes.root}>
					<MenuItem className="menu-text" onClick={() => goToProfile()}>
						Ver Perfil
					</MenuItem>
					<MenuItem className="menu-text" onClick={() => goToCheckList()}>
						Checklist de Compra
					</MenuItem>
					<Divider />
					<MenuItem className="menu-text green" onClick={() => handleSignOut()}>
						Cerrar Sesión
					</MenuItem>
				</div>
			</Popover>
		</React.Fragment>
	);

	return firstName ? loggedInComponent : logOutComponent;
}
