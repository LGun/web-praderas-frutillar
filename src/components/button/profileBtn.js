import React, { useContext, useEffect } from "react";
import { useRouter } from "next/router";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import { UserContext } from "../../context/user.context";
import { ProfileModalDispatch } from "../../context/profileModal.context";
import { makeStyles } from "@material-ui/core/styles";
import Popover from "@material-ui/core/Popover";
import Divider from "@material-ui/core/Divider";
import MenuItem from "@material-ui/core/MenuItem";
import { UserDispatchContext } from "../../context/user.context";
import { SignOut } from "../../firebase";
const useStyles = makeStyles({
	root: {
		width: 230
	}
});
export default function ProfileBtn() {
	const userDispatch = useContext(UserDispatchContext);
	const router = useRouter();
	const classes = useStyles();
	const user = useContext(UserContext);
	console.log("user", user);
	const { profile = {} } = user;
	const { firstName = "", customerId = "" } = profile;
	const dispatch = useContext(ProfileModalDispatch);
	const [ anchorEl, setAnchorEl ] = React.useState(null);

	const handleProfileClick = (event) => {
		setAnchorEl(event.currentTarget);
	};

	const handleClose = () => {
		setAnchorEl(0);
	};

	const handleSignOut = () => {
		SignOut();
		userDispatch({
			type: "UPDATE",
			payload: {
				profile: {}
			}
		});
		handleClose();
		router.push("/");
	};

	const goToProfile = () => {
		router.push("/perfil");
		handleClose();
	};

	const goToCheckList = () => {
		router.push("/checklist");
		handleClose();
	};

	const handleClick = () => {
		dispatch({
			type: "UPDATE",
			payload: {
				open: true
			}
		});
	};
	const logOutComponent = (
		<div className="relative clickable" onClick={() => handleClick()}>
			<AccountCircleIcon
				style={{ color: "#FFFFFF", fontSize: "27px", marginRight: "5px" }}
				className="profile-icon"
			/>
			<span>Iniciar sesión</span>
			<div className="reason-border" />
		</div>
	);
	const loggedInComponent = (
		<div className="position-relative">
			<div className="relative clickable" onClick={handleProfileClick}>
				<AccountCircleIcon
					style={{ color: "#FFFFFF", fontSize: "27px", marginRight: "5px" }}
					className="profile-icon"
				/>
				<span>{firstName}</span>
				<div className="reason-border" />
			</div>
			<Popover
				id="simple-menu"
				anchorEl={anchorEl}
				keepMounted
				open={Boolean(anchorEl)}
				onClose={() => handleClose()}
				anchorOrigin={{ vertical: 34, horizontal: -35 }}
				// transformOrigin={{ vertical: "top", horizontal: "center" }}
				PaperProps={{
					style: {
						borderRadius: "0px 0px 0px 20px"
					}
				}}
			>
				<div className={classes.root}>
					<MenuItem className="menu-text" onClick={() => goToProfile()}>
						Ver Perfil
					</MenuItem>
					<MenuItem className="menu-text" onClick={() => goToCheckList()}>
						Checklist de Compra
					</MenuItem>
					<Divider />
					<MenuItem className="menu-text green" onClick={() => handleSignOut()}>
						Cerrar Sesión
					</MenuItem>
				</div>
			</Popover>
		</div>
	);

	return customerId ? loggedInComponent : logOutComponent;
}
