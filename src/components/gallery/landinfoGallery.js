import React, { useEffect, useState } from "react";
import dynamic from "next/dynamic";
import Slider from "react-slick";
import { LazyLoadImage } from "react-lazy-load-image-component";
const VideoComponent = dynamic(() => import("../video"));

function useWindowSize() {
	const [ windowSize, setWindowSize ] = useState({
		width: undefined,
		height: undefined
	});

	useEffect(() => {
		// only execute all the code below in client side
		if (typeof window !== "undefined") {
			// Handler to call on window resize
			function handleResize() {
				// Set window width/height to state
				setWindowSize({
					width: window.innerWidth,
					height: window.innerHeight
				});
			}

			// Add event listener
			window.addEventListener("resize", handleResize);

			// Call handler right away so state gets updated with initial window size
			handleResize();

			// Remove event listener on cleanup
			return () => window.removeEventListener("resize", handleResize);
		}
	}, []); // Empty array ensures that effect is only run on mount
	return windowSize;
}

const SimpleSlider = ({ multimediaList = [] }) => {
	const settings = {
		arrow: true,
		dots: true,
		infinite: true,
		speed: 500,
		slidesToShow: 1,
		slidesToScroll: 1
	};
	const size = useWindowSize();
	console.log("multimediaList", multimediaList);
	return (
		<div id="land-info-slider">
			<Slider {...settings}>
				{multimediaList.map((item, index) => {
					if (index !== 0) {
						const { uploadType = "", urlMultimedia = "" } = item;
						if (urlMultimedia) {
							if (uploadType === "Youtube Video") {
								return (
									<div>
										<VideoComponent
											height={size.width > 1200 ? "360px" : "260px"}
											url={urlMultimedia}
										/>
									</div>
								);
							} else {
								return (
									<div>
										<LazyLoadImage className="single-img land-info-img" src={urlMultimedia} />
									</div>
								);
							}
						}
					}

					return null;
				})}
			</Slider>
		</div>
	);
};
export default SimpleSlider;
