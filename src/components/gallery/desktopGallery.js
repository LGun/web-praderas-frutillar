import React from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";

export default function ImageGridList({ multimedia = [], openModal }) {
	const isVideo = (tile) =>
		typeof tile.uploadType !== "undefined" && tile.uploadType === "Youtube Video" ? true : false;
	const getMultimediaUrl = (tile) =>
		typeof tile.urlMultimedia !== "undefined" && tile.urlMultimedia ? tile.urlMultimedia : "";
	return (
		<GridList className="desktop-only padding-b5" cellHeight={230} cols={4}>
			{multimedia.map((tile, index) => (
				<GridListTile
					key={index}
					cols={1}
					onClick={() =>
						openModal({
							video: isVideo(tile),
							src: getMultimediaUrl(tile)
						})}
					className={`${isVideo(tile) && "videoGrid"} gallery-img-cont`}
				>
					<LazyLoadImage
						className="full-width full-height"
						src={tile.urlThumbnail || tile.urlMultimedia}
						alt={tile.alt}
					/>
					<div className="only-on-hover card-info">
						<h1 className="overlay-title">{tile.hover || ""}</h1>
					</div>
					{typeof tile.uploadType !== "undefined" &&
					tile.uploadType === "Youtube Video" && (
						<div className="ytd-thumbnail-overlay-side-panel-renderer">
							<LazyLoadImage src="/video-play.svg" className="play-video-btn" />
						</div>
					)}
				</GridListTile>
			))}
		</GridList>
	);
}
