import React, { useState, useEffect } from "react";
import dynamic from "next/dynamic";
import { LazyLoadImage } from "react-lazy-load-image-component";
import Modal from "@material-ui/core/Modal";
const DesktopGallery = dynamic(() => import("./desktopGallery"));
const Carousel = dynamic(() => import("../card/carousel"));
const VideoComponent = dynamic(() => import("../video"));

function useWindowSize() {
	const [ windowSize, setWindowSize ] = useState({
		width: undefined,
		height: undefined
	});

	useEffect(() => {
		// only execute all the code below in client side
		if (typeof window !== "undefined") {
			// Handler to call on window resize
			function handleResize() {
				// Set window width/height to state
				setWindowSize({
					width: window.innerWidth,
					height: window.innerHeight
				});
			}

			// Add event listener
			window.addEventListener("resize", handleResize);

			// Call handler right away so state gets updated with initial window size
			handleResize();

			// Remove event listener on cleanup
			return () => window.removeEventListener("resize", handleResize);
		}
	}, []); // Empty array ensures that effect is only run on mount
	return windowSize;
}

export default function ImageGridList({ multimedia = [], slidesToShow = 2.1 }) {
	const [ items, setItems ] = useState([]);
	const [ open, setOpen ] = useState(false);
	const [ windowScreen, setWindowScreen ] = useState({});
	const [ currentDisplayItem, setCurrentDisplayItem ] = useState({});
	const size = useWindowSize();

	const handleOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	const openModal = (config) => {
		setCurrentDisplayItem(config);
		setOpen(true);
	};
	const getWidthVide = (_windowScreen) => {
		if (typeof _windowScreen !== "undefined") {
			if (_windowScreen.screen.width > 1200) {
				return `${_windowScreen.screen.width}px`;
			}
		}
		return "720px";
	};

	const getBody = () => {
		if (!currentDisplayItem.video) {
			return (
				<div className="image-container">
					{/* <img className="single-img" src="https://c7.staticflickr.com/9/8106/28941228886_86d1450016_b.jpg" /> */}
					<div className="image-cont">
						<div className="close-modal" onClick={() => setOpen(false)}>
							<LazyLoadImage src="/icono-contraer.svg" />
						</div>
						<LazyLoadImage className="single-img" src={currentDisplayItem.src} />
					</div>

					{/* <h2 id="simple-modal-title">Text in a modal</h2>
			<p id="simple-modal-description">Duis mollis, est non commodo luctus, nisi erat porttitor ligula.</p> */}
				</div>
			);
		} else {
			return (
				<div className="image-container" onClick={() => setOpen(false)}>
					<VideoComponent
						width={size.width > 1200 ? "720px" : `${size.width}px`}
						url={currentDisplayItem.src}
					/>
				</div>
			);
		}
	};

	useEffect(() => {
		if (typeof window !== "undefined") {
			setWindowScreen(getWidthVide(window));
		}
	}, []);

	useEffect(
		() => {
			let pivotArray = multimedia;
			if (multimedia.length > 8) {
				pivotArray.splice(8);
			}
			setItems(pivotArray);
		},
		[ multimedia ]
	);

	return (
		<React.Fragment>
			<DesktopGallery multimedia={items} openModal={(payload) => openModal(payload)} />
			<Carousel
				slidesToShow={slidesToShow}
				data={items}
				openModal={(payload) => openModal({ video: payload.video, src: payload.src })}
			/>
			<Modal
				open={open}
				onClose={handleClose}
				aria-labelledby="simple-modal-title"
				aria-describedby="simple-modal-description"
			>
				{getBody()}
			</Modal>
		</React.Fragment>
	);
}
