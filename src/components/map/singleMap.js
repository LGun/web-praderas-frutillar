import React, { Component, useContext, useEffect, useState } from "react";
import filter from "lodash/filter";
import find from "lodash/find";
import { LandContext } from "../../context/land.context";
import { formatLatLong } from "../../utils/helper";

var markers = [];

const SingleMap = ({ landNumber }) => {
	const lands = useContext(LandContext);
	const [ currentPolygons, setCurrentPolygons ] = useState([]);
	const [ map, setMap ] = useState({});

	const findLandByNumber = (number) => {
		return filter(lands, (_land) => Number(_land.landNumber) === Number(number));
	};

	const renderPolygon = (land, _map) => {
		if (_map && typeof _map !== "undefined") {
			const cordinates = formatLatLong(land.latLng);
			const polygon = new google.maps.Polygon({
				paths: cordinates,
				strokeColor: land.available === "Disponible" ? "#9FC230" : "#F0270E",
				strokeOpacity: 0.8,
				strokeWeight: 2,
				fillColor: land.available === "Disponible" ? "#9FC230" : "#F0270E",
				fillOpacity: 0.35
			});
			polygon.setMap && polygon.setMap(_map);
			const bounds = new google.maps.LatLngBounds();
			const polygonCoords =
				typeof cordinates !== "undefined" &&
				cordinates.map((cord) => new google.maps.LatLng(cord.lat, cord.lng));
			for (let i = 0; i < polygonCoords.length; i++) {
				bounds.extend(polygonCoords[i]);
			}

			const mark = new google.maps.Marker({
				icon: {
					url:
						"https://firebasestorage.googleapis.com/v0/b/original-mesh-305921.appspot.com/o/galery%2Fubicacion.svg?alt=media&token=d74860ec-b94a-4ec7-97b4-803de6872c18",
					scaledSize: new google.maps.Size(40, 40)
				},
				position: bounds.getCenter(),
				map: _map,
				label: `${land.landNumber}`,
				labelClass: "mapIconLabel"
			});

			markers.push(mark);
			return polygon;
		}
		return {};
	};

	const clearPolygon = () => {
		currentPolygons.map((polygon) => polygon.setMap && polygon.setMap(null));
		setCurrentPolygons([]);
	};

	const clearMarks = async () => {
		markers.map((mark) => mark.setMap && mark.setMap(null));
		markers = [];
	};

	const renderPolygons = async () => {
		if (map && typeof map !== "undefined") {
			clearPolygon();
			clearMarks();
			const filtered = findLandByNumber(landNumber);
			const arrayPolygon = filtered.map((land) => renderPolygon(land, map));
			setCurrentPolygons(arrayPolygon);
		}
	};

	useEffect(() => {
		const single_map = new google.maps.Map(document.getElementById("single_map"), {
			zoom: 15,
			center: { lat: -41.143619, lng: -73.134244 },
			mapTypeId: "hybrid"
		});
		setMap(single_map);
	}, []);

	useEffect(
		() => {
			renderPolygons();
		},
		[ landNumber, map ]
	);

	return (
		<div className="map-cont">
			<div id="single_map" />
		</div>
	);
};

export default SingleMap;
