import React, { Component, useContext, useEffect, useState } from "react";
import filter from "lodash/filter";
import find from "lodash/find";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import FavoriteIcon from "@material-ui/icons/Favorite";
import Dot from "@material-ui/icons/FiberManualRecord";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";

import { ModalDispatchContext } from "../../context/modal.context";
import { DispatchContext, LandContext } from "../../context/land.context";
import { UserDispatchContext, UserContext } from "../../context/user.context";
import { StepContext } from "../../context/step.context";
import firebase from "../../firebase";
import { formatLatLong, getLandIcons } from "../../utils/helper";
import { STEPPS } from "../../utils/constants";
import { createUserLikedLand, deleteUserLikedLand, getLandsByProjectId } from "../../services";
import InfoWindowEx from "./infoWindowEx";

var markers = [];

const SimpleMap = () => {
	const modalDispatch = useContext(ModalDispatchContext);
	const dispatch = useContext(DispatchContext);
	const lands = useContext(LandContext);
	const step = useContext(StepContext);
	const userDispatch = useContext(UserDispatchContext);
	const userData = useContext(UserContext);

	const [ currentPolygons, setCurrentPolygons ] = useState([]);
	const [ map, setMap ] = useState({});
	const [ markers2, setmarkers2 ] = useState();
	const [ currentMarker, setCurrentMarker ] = useState({});
	const [ liked, setLiked ] = useState(false);
	const findLandByStep = (step) => {
		switch (step) {
			case STEPPS.FIRST_STEPP:
			case STEPPS.SECOND_STEPP:
				return filter(lands, (_land) => _land.step === step);
			case STEPPS.VOLCANO:
				return filter(lands, (_land) => _land.volcanoView === "Si" || _land.volcanoView === "Parcial");
			case STEPPS.TREES:
				return filter(lands, (_land) => _land.trees);
			case STEPPS.CORNER:
				return filter(lands, (_land) => _land.cornerLand);
			case STEPPS.BEST_PRICES:
				return filter(lands, (_land) => _land.discount > 0);
			default:
				return lands;
		}
	};

	const saveLandLiked = (landNumber) => {
		let likedValue = true;
		if (typeof userData.likedLands !== "undefined") {
			const totalLandLiked = userData.likedLands.find((element) => element === landNumber);
			if (typeof totalLandLiked === "undefined") {
				userDispatch({
					type: "UPDATE",
					payload: {
						likedLands: [ ...userData.likedLands, landNumber ]
					}
				});
				likedValue = true;
			} else {
				dislikeLand(landNumber);
				likedValue = false;
			}
		} else {
			userDispatch({
				type: "UPDATE",
				payload: {
					likedLands: [ landNumber ]
				}
			});
			likedValue = true;
		}
		setLiked(likedValue);
		if (userData.profile) {
			const { customerId = "" } = userData.profile;
			if (customerId) {
				if (likedValue) {
					createUserLikedLand({ customerId: userData.profile.customerId, landNumber: landNumber });
				} else {
					deleteUserLikedLand({ customerId: userData.profile.customerId, landNumber: landNumber });
				}
			}
		}
	};

	const findLandByNumber = (number) => {
		return filter(lands, (_land) => Number(_land.landNumber) === Number(number));
	};

	const renderPolygon = (land, _map) => {
		const cordinates = formatLatLong(land.latLng);
		const polygon = new google.maps.Polygon({
			paths: cordinates,
			// strokeColor: land.available === "Disponible" ? "#9FC230" : "#F0270E",
			strokeColor: "#000",
			strokeOpacity: 0.8,
			strokeWeight: 2,
			fillColor: land.available === "Disponible" ? "#9FC230" : "#F0270E",
			fillOpacity: 0.35
		});
		polygon.setMap(_map);
		const bounds = new google.maps.LatLngBounds();
		const polygonCoords =
			typeof cordinates !== "undefined" && cordinates.map((cord) => new google.maps.LatLng(cord.lat, cord.lng));
		for (let i = 0; i < polygonCoords.length; i++) {
			bounds.extend(polygonCoords[i]);
		}
		if (land.available === "Disponible") {
			const mark = new google.maps.Marker({
				position: bounds.getCenter(),
				map: _map,
				icon: {
					url:
						"https://firebasestorage.googleapis.com/v0/b/original-mesh-305921.appspot.com/o/galery%2Fubicacion.svg?alt=media&token=d74860ec-b94a-4ec7-97b4-803de6872c18",
					scaledSize: new google.maps.Size(40, 40)
				},
				label: `${land.landNumber}`,
				labelClass: "mapIconLabel"
			});
			renderInfoWindow(_map, mark, land);
			markers.push(mark);
			setmarkers2(mark);
		}
		return polygon;
	};

	const renderInfoWindow = (map, _marker, land) => {
		_marker.addListener("click", () => {
			setCurrentMarker({ marker: _marker, visible: true, land });
			checkFavoriteLand(land.landNumber);
		});
	};

	const clearPolygon = () => {
		currentPolygons.map((polygon) => polygon.setMap(null));
	};

	const clearMarks = async () => {
		markers.map((mark) => mark.setMap(null));
		markers = [];
	};

	const renderPolygons = async () => {
		clearPolygon();
		clearMarks();
		if (step.filtered) {
			const filtered = typeof step.filtered !== "undefined" ? findLandByNumber(step.filtered) : [];
			console.log("filtered", filtered);
			const arrayPolygon = filtered.length > 0 ? filtered.map((land) => renderPolygon(land, map)) : [];
			setCurrentPolygons(arrayPolygon);
		} else {
			const filtered = findLandByStep(step.name) || [];
			console.log("filtered", filtered);
			const arrayPolygon = filtered.length > 0 ? filtered.map((land) => renderPolygon(land, map)) : [];
			setCurrentPolygons(arrayPolygon);
		}
	};

	const fechDataLands = async () => {
		const data = await getLandsByProjectId();
		dispatch({
			type: "UPDATE",
			payload: data
		});
	};

	useEffect(() => {
		fechDataLands();
		const _map = new google.maps.Map(document.getElementById("map"), {
			zoom: 15,
			center: { lat: -41.143619, lng: -73.134244 },
			mapTypeId: "hybrid"
		});
		setMap(_map);
	}, []);

	const checkFavoriteLand = (_landNumber) => {
		const _favoritesLands = localStorage.getItem("favoriteLands");
		if (_favoritesLands.includes(_landNumber)) {
			setLiked(true);
		} else {
			setLiked(false);
		}
	};

	const dislikeLand = (numberLand) => {
		const land = find(lands, { landNumber: numberLand });
		const newLikedLands = _.remove(userData.likedLands, (_land) => {
			return Number(_land) !== Number(land.landNumber);
		});
		userDispatch({
			type: "UPDATE",
			payload: {
				likedLands: typeof newLikedLands !== "undefined" ? newLikedLands : []
			}
		});
	};

	useEffect(
		() => {
			renderPolygons();
		},
		[ step, map, lands ]
	);

	useEffect(
		() => {
			if (typeof userData.likedLands !== "undefined") {
				localStorage.setItem("favoriteLands", userData.likedLands);
			}
		},
		[ userData.likedLands ]
	);

	return (
		<div className="position-relative">
			<div className="label-spec-map">
				<div>
					<Dot className="green-dot" />Disponible
				</div>
				<div>
					<Dot className="red-dot" />Vendida
				</div>
			</div>
			<div className="map-cont">
				<div id="map">
					<InfoWindowEx
						google={typeof google !== "undefined" ? google : null}
						marker={currentMarker.marker ? currentMarker.marker : null}
						visible={currentMarker.visible ? currentMarker.visible : false}
						map={map}
						className="full-width"
					>
						<div className="full-width">
							{typeof currentMarker.land !== "undefined" ? (
								<div id="content">
									<div id="siteNotice" />
									<div className="d-flex">
										<div class="d-flex ai-center semi-black simple-p mob-popup bold mr-3">
											{`Parcela Nº ${currentMarker.land.landNumber}`}
										</div>
										<div class="d-flex ai-center semi-black simple-p mob-popup bold mr-3">|</div>
										<div class="d-flex ai-center semi-black simple-p mob-popup bold mr-3">
											<span className=" red-color">{`$ ${Number(
												currentMarker.land.price
											).toLocaleString("es-ES")}`}</span>
										</div>
										<div class="d-flex ai-center semi-black simple-p mob-popup bold mr-3">|</div>
										<Button
											className="mob-popup green-btn"
											onClick={() =>
												modalDispatch({
													type: "UPDATE",
													payload: { open: true, landNumber: currentMarker.land.landNumber }
												})}
											variant="contained"
											color="primary"
										>
											Ver ficha
										</Button>
									</div>
									<div id="bodyContent">
										<div className="d-flex jc-space-b">
											<div class="d-flex ai-center semi-black simple-p mob-popup bold mr-3">
												{`${currentMarker.land.totalSize} m²`}
											</div>
											<div class="d-flex ai-center semi-black simple-p mob-popup bold mr-3">
												|
											</div>
											<div class="d-flex ai-center semi-black simple-p mob-popup bold mr-3">
												{getLandIcons(currentMarker.land)}
											</div>
											<div class="d-flex ai-center semi-black simple-p mob-popup bold mr-3">
												|
											</div>
											<div class="d-flex ai-center semi-black simple-p mob-popup bold mr-3">
												Me gusta
												<IconButton
													onClick={() => saveLandLiked(currentMarker.land.landNumber)}
												>
													{liked ? (
														<FavoriteIcon className="mob-popup green-color" />
													) : (
														<FavoriteBorderIcon className="mob-popup green-color" />
													)}
												</IconButton>
											</div>
										</div>
									</div>
								</div>
							) : null}
						</div>
					</InfoWindowEx>
				</div>
			</div>
		</div>
	);
};

export default SimpleMap;
