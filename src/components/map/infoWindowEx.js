import React from "react";
import { render } from "react-dom";
import { InfoWindow } from "google-maps-react";

export default class InfoWindowEx extends React.Component {
	constructor(props) {
		super(props);
		this.infoWindowRef = React.createRef();
		this.contentElement = typeof document !== "undefined" ? document.createElement(`div`) : null;
	}

	componentDidUpdate(prevProps) {
		if (this.props.children !== prevProps.children) {
			render(React.Children.only(this.props.children), this.contentElement);
			this.infoWindowRef.current.infowindow.setContent(this.contentElement);
		}
	}

	render() {
		return <InfoWindow ref={this.infoWindowRef} {...this.props} />;
	}
}
