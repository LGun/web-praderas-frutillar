import React from "react";
import { useRouter } from "next/router";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";
import LinkSmoothScroll from "../scroll";
{
	/* <Link activeClass="active"
      to="target"
      spy={true}
      smooth={true}
      hashSpy={true}
      offset={50}
      duration={500}
      delay={1000}
      isDynamic={true}
      onSetActive={this.handleSetActive}
      onSetInactive={this.handleSetInactive}
      ignoreCancelEvents={false}
>
  Your name
</Link> */
}
const useStyles = makeStyles((theme) => ({
	root: {
		width: "fit-content",
		height: "45px",
		borderRadius: "10px 0px 0px 0px",
		color: theme.palette.text.secondary,
		backgroundColor: "#292929",
		"& svg": {
			margin: theme.spacing(1.5)
		},
		"& hr": {
			height: "auto !important",
			alignSelf: "stretch",
			width: "1px",
			backgroundColor: "#fff",
			margin: "10px 0px"
		}
	},
	anchor: {
		color: "#FFF",
		padding: "10px 15px"
	}
}));

export default function VerticalDividers() {
	const classes = useStyles();
	const router = useRouter();
	return (
		<Grid container alignItems="center" className={`${classes.root} nav-menu-cont`}>
			<a className={classes.anchor} onClick={() => router.push({ pathname: "/checklist" })}>
				CHECK LIST DE COMPRA
			</a>
			<Divider className={classes.divider} orientation="vertical" flexItem />
			<a className={classes.anchor} onClick={() => router.push({ pathname: "/faq" })}>
				PREGUNTAS FRECUENTES
			</a>
			<Divider className={classes.divider} orientation="vertical" flexItem />
			<LinkSmoothScroll href="/#tu_mejor_inversion">
				<a className={classes.anchor}>CONOCE EL PROYECTO</a>
			</LinkSmoothScroll>
			{/* <Divider orientation="vertical" flexItem />
			<a className={classes.anchor} href="#">
				PRIMEROS PASOS A LA COMPRA
			</a>
			<Divider orientation="vertical" flexItem />
			<a className={classes.anchor} href="#">
				EN PROCESO
			</a> */}
			<Divider orientation="vertical" flexItem />
			<a className={classes.anchor} href="/#encuentra_tu_parcela">
				LAS PARCELAS
			</a>
			<Divider orientation="vertical" flexItem />
			<a className={classes.anchor} href="/#contacto">
				CONTACTO
			</a>
		</Grid>
	);
}
