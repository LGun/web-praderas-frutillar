import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";

const useStyles = makeStyles((theme) => ({
	root: {
		width: "100%",
		height: "55px",
		color: theme.palette.text.secondary,
		display: "flex",
		justifyContent: "center",
		background: "#B2B2B2 0% 0% no-repeat padding-box",
		boxShadow: "0px 3px 6px #00000029",
		"& svg": {
			margin: theme.spacing(1.5)
		},
		"& hr": {
			height: "auto !important",
			alignSelf: "stretch",
			width: "1px",
			backgroundColor: "#434343",
			margin: "10px 0px"
		}
	},
	anchor: {
		color: "#434343",
		padding: "10px 20px",
		"&:hover": {
			color: "#ff8900"
		}
	}
}));

export default function VerticalDividers() {
	const classes = useStyles();

	return (
		<Grid container alignItems="center" className={classes.root}>
			<a className={classes.anchor} href="#nuestra_ubicacion">
				NUESTRA UBICACIÓN
			</a>
			<Divider orientation="vertical" flexItem />
			<a className={classes.anchor} href="#caracteristicas">
				CARACTERÍSTICAS
			</a>
			<Divider orientation="vertical" flexItem />
			<a className={classes.anchor} href="#lugares_cercanos">
				LUGARES CERCANOS
			</a>
			<Divider orientation="vertical" flexItem />
			<a className={classes.anchor} href="#testimonios">
				TESTIMONIOS
			</a>
		</Grid>
	);
}
