import React from "react";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
	root: {
		width: "100%",
		maxWidth: "75rem",
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		margin: "0px auto",
		height: "1px"
	}
});

export default function SimpleCont() {
	const classes = useStyles();

	return (
		<div className={classes.root}>
			<div className="divier" />
		</div>
	);
}
