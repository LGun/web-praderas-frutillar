import React, { useContext, useState, useEffect } from "react";
import Slider from "react-slick";
import SalesCard from "../card/sales";
import NavigateBefore from "@material-ui/icons/NavigateBefore";
import NavigateNext from "@material-ui/icons/NavigateNext";

export default function MultipleItems(ref) {
	const [ multimediaItems, setMultimediaItems ] = useState([]);
	const getPackages = (items) => {
		const n = 3; //tweak this to add more items per line
		const result = new Array(Math.ceil(items.length / n)).fill().map((_) => items.splice(0, n));
		return result;
	};
	function SampleNextArrow(props) {
		const { className, style, onClick } = props;
		return (
			<div className={className} style={{ ...style, display: "block" }} onClick={onClick}>
				<NavigateNext />
			</div>
		);
	}

	function SamplePrevArrow(props) {
		const { className, style, onClick } = props;
		return (
			<div className={className} style={{ ...style, display: "block" }} onClick={onClick}>
				<NavigateBefore />
			</div>
		);
	}

	const settings = {
		arrow: true,
		dots: true,
		infinite: false,
		speed: 500,
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow: <SamplePrevArrow />,
		nextArrow: <SampleNextArrow />,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					initialSlide: 1
				}
			},
			{
				breakpoint: 780,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					initialSlide: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	};

	useEffect(
		() => {
			setMultimediaItems(getPackages(ref.multimedia));
		},
		[ ref.multimedia ]
	);

	return (
		<div id="slider-cont-sales">
			<Slider {...settings}>{multimediaItems.map((item) => <SalesCard multimedia={item} />)}</Slider>
		</div>
	);
}
