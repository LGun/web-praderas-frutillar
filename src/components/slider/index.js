import React, { useState, useContext } from "react";
import { Carousel, CarouselItem, CarouselControl, CarouselIndicators, CarouselCaption } from "reactstrap";
import { LazyLoadImage } from "react-lazy-load-image-component";
import "react-lazy-load-image-component/src/effects/blur.css";
import { MultimediaContext } from "../../context/multimedia.context";

const Example = (props) => {
	const [ activeIndex, setActiveIndex ] = useState(0);
	const [ animating, setAnimating ] = useState(false);
	const multimedia = useContext(MultimediaContext);
	const { mainSliders = [] } = multimedia;
	const next = () => {
		if (animating) return;
		const nextIndex = activeIndex === mainSliders.length - 1 ? 0 : activeIndex + 1;
		setActiveIndex(nextIndex);
	};

	const previous = () => {
		if (animating) return;
		const nextIndex = activeIndex === 0 ? mainSliders.length - 1 : activeIndex - 1;
		setActiveIndex(nextIndex);
	};

	const goToIndex = (newIndex) => {
		if (animating) return;
		setActiveIndex(newIndex);
	};

	const slides = mainSliders.map((item, index) => {
		return (
			<CarouselItem onExiting={() => setAnimating(true)} onExited={() => setAnimating(false)} key={index}>
				<div className="lazyload-wrapper">
					<picture>
						<source data-testid="source-image" srcSet={item.urlMultimedia} media="(min-width: 720px)" />
						<LazyLoadImage
							data-testid="one-clickable-container"
							alt="Parcelas"
							title="Parcelas Frutillar"
							className="BackgroundImageOnly-module_background-image__19Xw0 d-none d-md-flex"
							placeholder={
								<img
									className="BackgroundImageOnly-module_background-image__19Xw0 d-none d-md-flex"
									src="/slider-random-desktop-100-min.jpg"
								/>
							}
						/>
					</picture>
				</div>
				<div className="lazyload-wrapper">
					<picture>
						<source
							data-testid="source-image"
							srcSet={item.urlMobile || item.urlMultimedia}
							media="(max-width: 720px)"
						/>
						<LazyLoadImage
							data-testid="mobile-clickable-container"
							alt="Parcelas"
							title="Parcelas Frutillar"
							className="BackgroundImageOnly-module_background-image__19Xw0 d-md-none"
							placeholder={
								<img
									className="BackgroundImageOnly-module_background-image__19Xw0 d-md-none"
									src="/slider-random-mobile-100-min.jpg"
								/>
							}
						/>
					</picture>
				</div>
			</CarouselItem>
		);
	});

	return (
		<Carousel id="initial-slider" activeIndex={activeIndex} next={next} previous={previous}>
			<CarouselIndicators items={mainSliders} activeIndex={activeIndex} onClickHandler={goToIndex} />
			{slides}
			<CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />
			<CarouselControl direction="next" directionText="Next" onClickHandler={next} />
		</Carousel>
	);
};

export default Example;
