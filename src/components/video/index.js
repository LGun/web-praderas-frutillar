import React, { useEffect, useState } from "react";
import dynamic from "next/dynamic";
const ReactPlayer = dynamic(() => import("react-player"));

export default function VideoComponent(ref) {
	const { contClassName = "" } = ref;
	const [ startLoad, setStartLoad ] = useState(false);
	useEffect(() => {
		setStartLoad(true);
	}, []);
	return (
		<div className={`pd-top-5 ${contClassName}`}>
			{startLoad ? <ReactPlayer controls={true} width="100%" {...ref} /> : null}
		</div>
	);
}
