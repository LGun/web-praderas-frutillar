import React, { useEffect, useState, useContext } from "react";
import { useDropzone } from "react-dropzone";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import TouchAppIcon from "@material-ui/icons/TouchApp";
import CircularProgress from "@material-ui/core/CircularProgress";
import firebase from "../../firebase";
import { uploadFile, updateUser } from "../../services";
import { UserDispatchContext, UserContext } from "../../context/user.context";

const thumbsContainer = {
	display: "flex",
	flexDirection: "row",
	flexWrap: "wrap",
	marginTop: 16
};

const thumb = {
	display: "inline-flex",
	borderRadius: 2,
	border: "1px solid #eaeaea",
	marginBottom: 8,
	marginRight: 8,
	width: 100,
	height: 100,
	padding: 4,
	boxSizing: "border-box"
};

const thumbInner = {
	display: "flex",
	minWidth: 0,
	overflow: "hidden"
};

const img = {
	display: "block",
	width: "auto",
	height: "100%"
};

const useStyles = makeStyles((theme) => ({
	buttonProgress: {
		color: "#FFF"
	}
}));

export default function DropZone({ setType, type }) {
	const classes = useStyles();
	const userData = useContext(UserContext);
	const userDispatch = useContext(UserDispatchContext);
	const { profile = {} } = userData;
	const { customerId = "" } = profile;
	const [ files, setFiles ] = useState([]);
	const [ loading, setLoading ] = useState(false);
	const { getRootProps, getInputProps } = useDropzone({
		accept: "image/*",
		onDrop: (acceptedFiles) => {
			setFiles(
				acceptedFiles.map((file) =>
					Object.assign(file, {
						preview: URL.createObjectURL(file)
					})
				)
			);
		}
	});

	const thumbs = files.map((file) => (
		<div style={thumb} key={file.name}>
			<div style={thumbInner}>
				<img src={file.preview} style={img} />
			</div>
		</div>
	));

	useEffect(
		() => () => {
			files.forEach((file) => URL.revokeObjectURL(file.preview));
		},
		[ files ]
	);

	const handleContinue = async () => {
		setLoading(true);

		if (customerId && files[0]) {
			const payload = {
				filename: "urlFileWarranty",
				customerId,
				file: files[0]
			};
			if (type === "uploadFileStepThree") {
				payload.filename = "urlFileSeriousnessWarranty";
			}

			const urlFile = await uploadFile(payload);
			if (urlFile) {
				const updateUserPayload = {
					customerId
				};
				if (type === "uploadFileStepTwo") {
					updateUserPayload.step2 = {
						stateStep2: "En validación",
						stepNumber: 1,
						timeCreation: firebase.firestore.FieldValue.serverTimestamp()
					};
					updateUserPayload.urlFileWarranty = urlFile;
				} else if (type === "uploadFileStepThree") {
					updateUserPayload.step3 = {
						stateStep3: "En validación",
						stepNumber: 2,
						timeCreation: firebase.firestore.FieldValue.serverTimestamp()
					};
					updateUserPayload.urlFileSeriousnessWarranty = urlFile;
				}
				userDispatch({
					type: "UPDATE",
					payload: {
						profile: {
							...profile,
							...updateUserPayload
						}
					}
				});
				await updateUser(updateUserPayload);
				setType("successUpload");
			} else {
				// pantalla de error
			}
		}
		setLoading(false);
	};

	return (
		<React.Fragment>
			<div {...getRootProps({ className: "dropzone" })}>
				<input {...getInputProps()} />
				<TouchAppIcon className="dropzone-icon" />
				<p>Arrastre y suelte el archivo aquí, o haga clic para seleccionar archivos</p>
			</div>
			<aside style={thumbsContainer}>{thumbs}</aside>
			<Button
				id="contact-button"
				type="submit"
				value="Send"
				className="login-button mt-10"
				variant="contained"
				color="primary"
				disabled={files.length < 1}
				onClick={() => handleContinue()}
			>
				{loading ? <CircularProgress size={26} className={classes.buttonProgress} /> : "CONTINUAR"}
			</Button>
		</React.Fragment>
	);
}
