import ImageGallery from "react-image-gallery";
import VideoComponent from "../video";
class MyComponent extends React.Component {
	render() {
		const images = [
			{
				renderItem: () => <VideoComponent width="100%" height="450px" />,
				thumbnail: "http://lorempixel.com/250/150/nature/1/"
			},
			{
				original: "http://lorempixel.com/1000/600/nature/3/",
				thumbnail: "http://lorempixel.com/250/150/nature/2/"
			},
			{
				original: "http://lorempixel.com/1000/600/nature/3/",
				thumbnail: "http://lorempixel.com/250/150/nature/3/"
			}
		];

		return <ImageGallery items={images} showPlayButton={false} showFullscreenButton={false} />;
	}
}

export default MyComponent;
