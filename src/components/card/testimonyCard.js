import React, { memo } from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";
import Avatar from "@material-ui/core/Avatar";
import Grid from "@material-ui/core/Grid";

const TestimonyCard = memo(({ title, description, imageUrl }) => {
	return (
		<div className="d-flex row">
			<div className="d-flex row full-width full-height padding-20 testimony-cont bg-grey-card">
				<Grid item xs={4}>
					<div className="d-flex jc-center ai-center full-height full-width">
						<div className="avatar-image">
							<LazyLoadImage alt="Remy Sharp" src={imageUrl} />
						</div>
					</div>
				</Grid>
				<Grid id="testimony-text-cont-id" item xs={8}>
					<div className="d-flex column mh-100 mobile-ml-10">
						<h3 className="title-card-testimony">{title}</h3>
						<p className="p-card-testimony">{description}</p>
					</div>
				</Grid>
			</div>
		</div>
	);
});

export default TestimonyCard;
