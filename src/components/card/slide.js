import React, { useContext } from "react";
import Slider from "react-slick";
import TestimonyCard from "./TestimonyCard";
import { TestimonyContext } from "../../context/testimony.context";

export default function MultipleItems() {
	const testimonies = useContext(TestimonyContext);

	const settings = {
		arrow: false,
		dots: true,
		infinite: true,
		speed: 500,
		slidesToShow: 3,
		slidesToScroll: 3,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					initialSlide: 2
				}
			},
			{
				breakpoint: 780,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					initialSlide: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	};
	return (
		<div>
			<Slider {...settings}>
				<TestimonyCard
					title="Juan Carlos J. / Corredor de seguros"
					description="“Estamos muy contentos con la compra en Praderas de Frutillar, además de la plusvalía el lugar es hermoso. Éxito y gracias”"
					imageUrl="https://firebasestorage.googleapis.com/v0/b/original-mesh-305921.appspot.com/o/galery%2Ftestimonials%2Ftestimonio-03-hombre-100.jpg?alt=media&token=8ef723d5-04af-4caa-bad4-902a66bb6d56"
				/>

				<TestimonyCard
					title="Ellen I. / Empresaria inmobiliaria"
					description="“Compré mi parcela hace 4 años para tener un cambio de vida, estoy muy contenta ya que todo lo que soñé esta hoy materializado, es un condominio tranquilo, con excelente acceso y bellos caminos interiores. Todas las canalizaciones subterráneas que permiten tener las preciosas vistas despejadas.  Excelente inversión.   Gracias!!”"
					imageUrl="https://firebasestorage.googleapis.com/v0/b/original-mesh-305921.appspot.com/o/galery%2Ftestimonials%2Ftestimonio-03-mujer-100.jpg?alt=media&token=8bed8d3b-06f2-42c7-ae70-2429d6dd94ae"
				/>

				<TestimonyCard
					title="Ignacio C. / Ingeniero"
					description="“Estoy feliz de haber adquirido mi parcela en “Fundo Praderas de Frutillar” pues es un lugar de tranquilidad y belleza.  Los dueños han cumplido con todo lo que prometieron y cada día aumenta el precio del terreno.”"
					imageUrl="https://firebasestorage.googleapis.com/v0/b/original-mesh-305921.appspot.com/o/galery%2Ftestimonials%2Ftestimonio-hombre-01-8.png?alt=media&token=b5e9d75e-1070-4031-9771-57b97b0e1909"
				/>

				<TestimonyCard
					title="Isabel H. / Dentista"
					description="Compramos la parcela hace más de 3 años. Lo que más nos gustó es que refleja el entorno del Sur de Chile. Un lugar ideal para vivir. Ligia Quiroz siempre nos ha ayudado en todas nuestras dudas e inquietudes.  Todo ha evolucionado como lo describieron en un inicio, por lo que puedo dar fe de que son 100% confiables."
					imageUrl="https://firebasestorage.googleapis.com/v0/b/original-mesh-305921.appspot.com/o/galery%2Ftestimonials%2Ftestimonio-mujer-01-8.png?alt=media&token=95b9751e-4289-4545-a68f-29ead9695c66"
				/>

				<TestimonyCard
					title="Mario González"
					description="Nos enamoramos de la parcela en cuanto la vimos. Además la situación de “Fundo Praderas de Frutillar“ es excepcional.  Los dueños nos han ayudado en todo lo que hemos necesitado y nos han asesorado en todas las dudas."
					imageUrl="https://firebasestorage.googleapis.com/v0/b/original-mesh-305921.appspot.com/o/galery%2Ftestimonials%2Ftestimonio-hombre-02-8.png?alt=media&token=bcb5debb-7576-47ec-bb9f-100b28b193c6"
				/>

				<TestimonyCard
					title="Soledad M 7 C. Auditor"
					description="“Estamos felices de haber invertido como familia en la adquisición de una parcela en Praderas de Frutillar, es un bien para toda la vida.  Tranquilidad, bellezas naturales. Además que los dueños han cumplido con lo prometido, eso habla de gente de confianza y eso tranquiliza a cualquiera.”"
					imageUrl="https://firebasestorage.googleapis.com/v0/b/original-mesh-305921.appspot.com/o/galery%2Ftestimonials%2Ftestimonio-mujer-02-8.png?alt=media&token=346954c8-3dcf-4da5-8eab-869527c06dd5"
				/>
			</Slider>
		</div>
	);
}
