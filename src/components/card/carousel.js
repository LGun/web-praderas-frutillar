import React, { Component } from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";
import Slider from "react-slick";

export default function Carousel(props) {
	const settings = {
		arrow: true,
		dots: true,
		infinite: true,
		speed: 500,
		slidesToShow: 3,
		slidesToScroll: 3,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: props.slidesToShow || 2.1,
					slidesToScroll: 2,
					initialSlide: 1
				}
			},
			{
				breakpoint: 780,
				settings: {
					slidesToShow: props.slidesToShow || 2.1,
					slidesToScroll: 1,
					initialSlide: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: props.slidesToShow || 2.1,
					slidesToScroll: 1
				}
			}
		]
	};
	const isVideo = (tile) =>
		typeof tile.uploadType !== "undefined" && tile.uploadType === "Youtube Video" ? true : false;
	const getMultimediaUrl = (tile) =>
		typeof tile.urlMultimedia !== "undefined" && tile.urlMultimedia ? tile.urlMultimedia : "";
	return (
		<div className={typeof props.landInfo === "undefined" && "mobile-only"}>
			<Slider {...settings}>
				{props.data.map((tile, index) => {
					return (
						<div key={index}>
							<div
								className="d-flex mb-gall-cont bg-grey-card"
								key={index}
								cols={1}
								onClick={() =>
									props.openModal({
										video: isVideo(tile),
										src: getMultimediaUrl(tile),
										landNumber: tile.hover
									})}
								style={{
									background: `url(${tile.urlThumbnail ? tile.urlThumbnail : tile.urlMultimedia})`
								}}
							>
								{tile.dealText && (
									<React.Fragment>
										<div className="green-bar-right" />
										<div className="deal-text-cont">
											<div className="deal-text-one">{`Parcela Nº${tile.hover}`}</div>
											<div className="deal-text-two">
												<span>$</span>
												{tile.dealText}
											</div>
											<div className="deal-text-three">*Foto referencial</div>
										</div>
									</React.Fragment>
								)}

								{isVideo(tile) && (
									<div className="ytd-thumbnail-overlay-side-panel-renderer">
										<LazyLoadImage src="/video-play.svg" className="play-video-btn" />
									</div>
								)}
							</div>
						</div>
					);
				})}
			</Slider>
		</div>
	);
}
