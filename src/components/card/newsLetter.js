import React, { useState } from "react";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Alert from "@material-ui/lab/Alert";
import Button from "@material-ui/core/Button";
import Snackbar from "@material-ui/core/Snackbar";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import emailjs from "emailjs-com";
import { sendEvent } from "../../utils/googleTagManager";
import { ValidateEmail } from "../../utils/helper";

const useStyles = makeStyles({
	root: {
		width: "100%"
	},
	paper: {
		textAlign: "center"
	},
	textField: {
		backgroundColor: "#fff",
		backgroundColor: "#F5F5F5",
		border: "3px solid #D9D9D9",
		borderRadius: "8px",
		"& label.Mui-focused": {
			color: "#ee7f0e"
		},
		"& .MuiInput-underline:after": {
			borderBottomColor: "green"
		},
		"& .MuiOutlinedInput-root": {
			"&.Mui-focused fieldset": {
				borderColor: "#ee7f0e"
			}
		}
	},
	textFieldLarge: {
		height: "60%",
		backgroundColor: "#fff",
		backgroundColor: "#F5F5F5",
		border: "3px solid #D9D9D9",
		borderRadius: "8px",
		"&:active": {
			// boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
		},
		"& label.Mui-focused": {
			color: "#ee7f0e"
		},
		"& .MuiInput-underline:after": {
			borderBottomColor: "green"
		},
		"& .MuiOutlinedInput-root": {
			"& .MuiInputBase-input": {
				height: "100%",
				maxWidth: "100%"
			},
			height: "100%",
			"&.Mui-focused fieldset": {
				borderColor: "#ee7f0e"
			}
		}
	},
	buttonProgress: {
		color: "#FFF"
	}
});

export default function SimpleCont() {
	const classes = useStyles();
	const [ name, setName ] = useState();
	const [ email, setEmail ] = useState();
	const [ open, setOpen ] = useState(false);
	const [ message, setMessage ] = useState();
	const [ phoneNumber, setPhoneNumber ] = useState();
	const [ emailError, setEmailError ] = useState(false);
	const [ loading, setLoading ] = useState(false);
	const [ disabledSendButton, setDisabledSendButton ] = useState(true);
	const handleClick = () => {
		setOpen(true);
	};

	const handleClose = (event, reason) => {
		if (reason === "clickaway") {
			return;
		}

		setOpen(false);
	};

	const validateEmail = (stringValue) => {
		const valid = ValidateEmail(stringValue);
		setEmailError(!valid);
		setDisabledSendButton(!valid);
		return !valid;
	};

	const onEmailChange = (stringValue) => {
		setEmail(stringValue);
		validateEmail(stringValue);
	};

	const sendEmail = (e) => {
		e.preventDefault();
		setLoading(true);
		// emailjs.sendForm("YOUR_SERVICE_ID", "YOUR_TEMPLATE_ID", e.target, "YOUR_USER_ID").then(
		// const payload = {
		// 	message: "adjsfalsdjhajd",
		// 	from_name: "testing",
		// 	phone_number: "123123123",
		// 	email: "gamezbarriosluis@gmail.com",
		// 	user_email: "gamezbarriosluis@gmail.com",
		// 	user_name: "testing",
		// 	contact_number: "123123123"
		// };
		const _invalidEmail = validateEmail(email);
		setDisabledSendButton(true);
		if (!_invalidEmail) {
			emailjs.sendForm("service_12wl3mx", "template_d55ye6t", e.target, "user_El3qLtLzaswCzH6Dr9rdN").then(
				(result) => {
					sendEvent("contact-button-click");
					setName("");
					setEmail("");
					setMessage("");
					setPhoneNumber("");
					setLoading(false);
					handleClick(e);
				},
				(error) => {
					console.log("error", error.text);
					setLoading(false);
					handleClick(e);
				}
			);
		}
	};

	return (
		<div className={classes.root}>
			<Snackbar
				open={open}
				onClose={handleClose}
				autoHideDuration={3000}
				anchorOrigin={{ vertical: "top", horizontal: "center" }}
			>
				{/* <Alert onClose={handleClose} severity="success">
					This is a success message!
				</Alert> */}
				<Alert elevation={6} onClose={handleClose} severity="success" variant="filled">
					Correo enviado con éxito!
				</Alert>
			</Snackbar>
			<form id="contact_us_form" className="contact-form" onSubmit={sendEmail}>
				<Paper className="padding-20 contact-grey-card">
					<Grid className="full-height" container spacing={2}>
						<Grid className="full-height d-flex ai-center jc-center column" item xs={12} sm={4}>
							<div className="mobile-nl-title d-flex ai-start column">
								<h2 className="title">
									<strong>SOLICITA</strong>
								</h2>
								<h2 className="title">CONSULTA</h2>
							</div>
						</Grid>
						<Grid className="full-height d-flex jc-center ai-center column" item xs={12} sm={4}>
							<div className="full-width full-height d-flex ai-center jc-space-b column">
								<TextField
									id="name-outlined-basic"
									value={name}
									name="user_name"
									className={classes.textField}
									label="Nombre"
									variant="outlined"
									fullWidth
									onChange={(e) => setName(e.target.value)}
								/>
								<TextField
									id="email-outlined-basic"
									className={classes.textField}
									name="user_email"
									label="Email"
									variant="outlined"
									fullWidth
									error={emailError}
									value={email}
									onChange={(e) => onEmailChange(e.target.value)}
								/>
								<p className={`email-error-text ${emailError && "active"}`}>Email invalido</p>
								<TextField
									id="telefono-outlined-basic"
									className={classes.textField}
									name="contact_number"
									label="Teléfono"
									variant="outlined"
									fullWidth
									value={phoneNumber}
									onChange={(e) => setPhoneNumber(e.target.value)}
								/>
							</div>
						</Grid>
						<Grid className="full-height d-flex jc-space-b ai-center column" item xs={12} sm={4}>
							<div className="full-width full-height d-flex ai-end jc-space-b column">
								<TextField
									id="telefono-outlined-basic"
									className={classes.textFieldLarge}
									label="Mensaje"
									name="message"
									variant="outlined"
									rows={2}
									rowsMax={4}
									fullWidth
									multiline
									value={message}
									onChange={(e) => setMessage(e.target.value)}
								/>
								<Button
									id="contact-button"
									type="submit"
									value="Send"
									className="contact-button mobile-mt-5"
									variant="contained"
									color="primary"
									disabled={disabledSendButton}
								>
									{loading ? (
										<CircularProgress size={26} className={classes.buttonProgress} />
									) : (
										"ENVIAR"
									)}
								</Button>
							</div>
						</Grid>
					</Grid>
				</Paper>
			</form>
		</div>
	);
}
