import React from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
	root: {
		width: "100%"
	},
	paper: {
		textAlign: "center"
	}
});

export default function SimpleCont() {
	const classes = useStyles();

	return (
		<div className={classes.root}>
			<Paper className="contact-g-card">
				<div className="d-flex">
					<div className="contact-logos-cont">
						<LazyLoadImage className="phone-logo" src="/Rectangulo 1833@2x.png" alt="phone-image" />
					</div>
					<div className="mobile-flex-center">
						<h3 className="contact-card-title">LLÁMANOS</h3>
						<p className="contact-card-text desktop-only">
							Habla directamente con nuestros agentes de venta.<strong>+56 2 3210 3992</strong>
						</p>
						<a href="tel:232103992" className="mobile-full-width">
							<Button className="contact-button" variant="contained" color="primary">
								LLAMAR
							</Button>
						</a>
					</div>
				</div>
			</Paper>
		</div>
	);
}
