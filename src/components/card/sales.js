import React, { memo, useContext } from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";

import { ModalDispatchContext } from "../../context/modal.context";

const SalesCard = ({ multimedia, title, description, imageUrl }) => {
	const modalDispatch = useContext(ModalDispatchContext);

	const handleClickImage = (_landNumber) => {
		modalDispatch({
			type: "UPDATE",
			payload: { open: true, landNumber: _landNumber }
		});
	};

	if (multimedia.length > 0) {
		return (
			<div className="d-flex row jc-space-b">
				{typeof multimedia[0] !== "undefined" && (
					<div className="sales-first-item">
						<div class="lazyload-wrapper">
							<picture>
								<source
									data-testid="source-image"
									srcSet={multimedia[0].urlMultimedia}
									media="(min-width: 720px)"
								/>
								<LazyLoadImage data-testid="cs-image" alt="desktop-category-image" class="img-fluid" />
							</picture>
						</div>
					</div>
				)}

				{typeof multimedia[1] !== "undefined" && (
					<div className="sales-normal-item clickable" onClick={() => handleClickImage(multimedia[1].hover)}>
						<div class="lazyload-wrapper">
							<picture>
								<source
									data-testid="source-image"
									srcSet={multimedia[1].urlMultimedia}
									media="(min-width: 720px)"
								/>
								<LazyLoadImage data-testid="cs-image" alt="desktop-category-image" class="img-fluid" />
							</picture>
						</div>
					</div>
				)}

				{typeof multimedia[2] !== "undefined" && (
					<div className="sales-normal-item clickable" onClick={() => handleClickImage(multimedia[2].hover)}>
						<div class="lazyload-wrapper">
							<picture>
								<source
									data-testid="source-image"
									// srcset="https://images.contentstack.io/v3/assets/blt7c5c2f2f888a7cc3/bltbf793cf5e018afa4/5fbd866672a3526f28dbeda5/PC2-lonuevo-yale-241120-fl.jpg?auto=webp&amp;disable=upscale&amp;quality=70&amp;width=1280"
									srcSet={multimedia[2].urlMultimedia}
									media="(min-width: 720px)"
								/>
								<LazyLoadImage data-testid="cs-image" alt="desktop-category-image" class="img-fluid" />
							</picture>
						</div>
					</div>
				)}
			</div>
		);
	}
	return null;
};

export default SalesCard;
