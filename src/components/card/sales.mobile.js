import React, { memo } from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";
import Avatar from "@material-ui/core/Avatar";
import Grid from "@material-ui/core/Grid";

const SalesCardMobile = memo(({ title, description, imageUrl }) => {
	return (
		<div className="d-flex row jc-space-b">
			<div className="sales-first-item">
				<div class="lazyload-wrapper">
					<picture>
						<source
							data-testid="source-image"
							srcset="https://images.contentstack.io/v3/assets/blt7c5c2f2f888a7cc3/blt97b3993af986e467/5fc8e3e272a3526f28dc1413/PC1-lonuevo-festina-031220-fl.jpg?auto=webp&amp;disable=upscale&amp;quality=70&amp;width=1280"
							media="(min-width: 720px)"
						/>
						<img data-testid="cs-image" alt="desktop-category-image" class="img-fluid" />
					</picture>
				</div>
			</div>
			<div className="sales-normal-item">
				<div class="lazyload-wrapper">
					<picture>
						<source
							data-testid="source-image"
							srcset="https://images.contentstack.io/v3/assets/blt7c5c2f2f888a7cc3/bltbf793cf5e018afa4/5fbd866672a3526f28dbeda5/PC2-lonuevo-yale-241120-fl.jpg?auto=webp&amp;disable=upscale&amp;quality=70&amp;width=1280"
							media="(min-width: 720px)"
						/>
						<img data-testid="cs-image" alt="desktop-category-image" class="img-fluid" />
					</picture>
				</div>
			</div>
			<div className="sales-normal-item">
				<div class="lazyload-wrapper">
					<picture>
						<source
							data-testid="source-image"
							srcset="https://images.contentstack.io/v3/assets/blt7c5c2f2f888a7cc3/bltbf793cf5e018afa4/5fbd866672a3526f28dbeda5/PC2-lonuevo-yale-241120-fl.jpg?auto=webp&amp;disable=upscale&amp;quality=70&amp;width=1280"
							media="(min-width: 720px)"
						/>
						<img data-testid="cs-image" alt="desktop-category-image" class="img-fluid" />
					</picture>
				</div>
			</div>
		</div>
	);
});

export default SalesCardMobile;
