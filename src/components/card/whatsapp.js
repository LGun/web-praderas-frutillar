import React from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
	root: {
		width: "100%"
	},
	paper: {
		textAlign: "center"
	}
});

export default function Whatsapp() {
	const classes = useStyles();

	return (
		<div className={classes.root}>
			<Paper className="contact-g-card">
				<div className="d-flex">
					<div className="contact-logos-cont">
						<LazyLoadImage className="phone-logo" src="/whatsapp@2xx.png" alt="phone-image" />
					</div>
					<div className="mobile-flex-center">
						<h3 className="contact-card-title">WHATSAPP</h3>
						<p className="contact-card-text desktop-only">
							Escribe un mensaje directamente por la aplicación a nuestros ejecutivos.
						</p>
						<Button
							id="whatsapp-button"
							onClick={() => {
								window.open(
									"https://wa.me/56975790254?text=¡Hola!%20estoy%20en%20https://praderasdefrutillar.cl%20mi%20nombre%20es:",
									"_system"
								);
							}}
							className="contact-button"
							variant="contained"
							color="primary"
						>
							ESCRÍBENOS<span className="write-ws-mg desktop-only">EN WHATSAPP</span>
						</Button>
					</div>
				</div>
			</Paper>
		</div>
	);
}
