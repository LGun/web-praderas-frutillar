import React, { useState, useContext, useEffect } from "react";
import Modal from "@material-ui/core/Modal";
import Paper from "@material-ui/core/Paper";
import CloseIcon from "@material-ui/icons/Close";
import { makeStyles } from "@material-ui/core/styles";
import StepOneCont from "../content/stepOneCont";
import SuccessStepOne from "../content/SuccessStepOne";
import RegisterCont from "../content/registerModalCont";
import ChangePassCont from "../content/changePassCont";
import { ProfileModalContext, ProfileModalDispatch } from "../../context/profileModal.context";
import { StepModalDispatchContext, StepModalContext } from "../../context/stepModal.context";

const useStyles = makeStyles((theme) => ({
	paper: {
		width: "80%",
		height: "auto",
		maxWidth: "500px",
		maxHeight: "600px",
		minHeight: "580px",
		overflowY: "hidden",
		position: "absolute",
		backgroundColor: "#EAEAEA"
	},
	"@media (max-width: 1070px)": {
		paper: {
			width: "100%",
			height: "100%",
			padding: "0px",
			display: "flex",
			maxWidth: "inherit",
			maxHeight: "inherit",
			minHeight: "inherit",
			flexDirection: "column"
		}
	},
	textField: {
		marginBottom: "10px",
		backgroundColor: "#fff",
		backgroundColor: "#F5F5F5",
		border: "3px solid #D9D9D9",
		borderRadius: "8px",
		"& label.Mui-focused": {
			color: "#ee7f0e"
		},
		"& .MuiInput-underline:after": {
			borderBottomColor: "green"
		},
		"& .MuiOutlinedInput-root": {
			"&.Mui-focused fieldset": {
				borderColor: "#ee7f0e"
			}
		}
	}
}));

export default function SimpleModal(ref) {
	const modalDispatch = useContext(ProfileModalDispatch);
	const stepModalDispatch = useContext(StepModalDispatchContext);
	const modalState = useContext(StepModalContext);
	const [ modalCont, setModalCont ] = useState("step_one");
	const { open = false } = modalState;
	const classes = useStyles();
	const closeLoginModal = () => {
		setModalCont("login");
		modalDispatch({ type: "UPDATE", payload: { open: false } });
	};

	const handleClose = () => {
		stepModalDispatch({ type: "UPDATE", payload: { open: false } });
	};

	const changeModalCont = () => {
		if (modalCont === "login") {
			setModalCont("register");
		} else {
			setModalCont("login");
		}
	};

	const getModalCont = () => {
		switch (modalCont) {
			case "step_one":
				return (
					<StepOneCont setModalCont={(value) => setModalCont(value)} closeModal={() => closeLoginModal()} />
				);
			case "success_step_one":
				return (
					<SuccessStepOne
						closeModal={() => closeLoginModal()}
						setModalCont={(value) => setModalCont(value)}
					/>
				);
			default:
				return (
					<StepOneCont
						setModalCont={(value) => setModalCont(value)}
						closeModal={() => closeLoginModal()}
						setModalCont={(value) => setModalCont(value)}
					/>
				);
		}
	};

	const getModalHeaderText = () => {
		switch ("step_one") {
			case "step_one":
				return "Formulario";
			default:
				return "Formulario";
		}
	};

	return (
		<div>
			<Modal
				open={open}
				onClose={() => closeLoginModal()}
				aria-labelledby="simple-modal-title"
				aria-describedby="simple-modal-description"
				className="d-flex ai-center jc-center"
			>
				<Paper className={classes.paper}>
					<div className="modal-header-account">
						<div className="modal-header-title">{getModalHeaderText()}</div>
					</div>
					<div className="close-login-modal" onClick={() => handleClose()}>
						<CloseIcon style={{ color: "#FFFFFF", fontSize: "27px" }} className="profile-icon" />
					</div>
					<div className="login-modal-content">{getModalCont()}</div>
				</Paper>
			</Modal>
		</div>
	);
}
