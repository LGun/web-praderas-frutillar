import React, { useState, useContext, useEffect } from "react";
import Modal from "@material-ui/core/Modal";
import Paper from "@material-ui/core/Paper";
import CloseIcon from "@material-ui/icons/Close";
import InfoIcon from "@material-ui/icons/Info";
import { makeStyles } from "@material-ui/core/styles";
import DropZone from "../dropZone";
import { UserContext } from "../../context/user.context";
const useStyles = makeStyles((theme) => ({
	paper: {
		width: "80%",
		height: "auto",
		maxWidth: "500px",
		maxHeight: "600px",
		// minHeight: "580px",
		overflowY: "hidden",
		position: "absolute",
		backgroundColor: "#EAEAEA"
	},
	"@media (max-width: 1070px)": {
		paper: {
			width: "95%",
			height: "auto",
			padding: "0px",
			display: "flex",
			maxWidth: "inherit",
			maxHeight: "inherit",
			minHeight: "inherit",
			flexDirection: "column"
		}
	},
	textField: {
		marginBottom: "10px",
		backgroundColor: "#fff",
		backgroundColor: "#F5F5F5",
		border: "3px solid #D9D9D9",
		borderRadius: "8px",
		"& label.Mui-focused": {
			color: "#ee7f0e"
		},
		"& .MuiInput-underline:after": {
			borderBottomColor: "green"
		},
		"& .MuiOutlinedInput-root": {
			"&.Mui-focused fieldset": {
				borderColor: "#ee7f0e"
			}
		}
	}
}));

export default function SimpleModal({ modalOpen, setModalOpen, type = "info_account", setType }) {
	const classes = useStyles();
	const userData = useContext(UserContext);
	const { profile = {} } = userData;
	const { step6 = {}, stepNumber } = profile;
	const { finalPrice = "" } = step6;
	console.log("_profile", profile);
	const handleClose = () => {
		setModalOpen(false);
	};

	const getModalHeaderText = () => {
		switch (type) {
			case "info_account":
				return "Datos Transferencia";
			case "recipt":
				return "Datos para solicitar Vale Vista";
			case "notary":
				return "Datos Notarias";
			case "uploadFileStepTwo":
				return "Subir Comprobante";
			case "uploadFileStepThree":
				return "Subir Garantía";
			case "successUpload":
				return "¡Gracias!";
			default:
				return null;
		}
	};

	const getModalCont = () => {
		switch (type) {
			case "info_account":
				return (
					<React.Fragment>
						<div className="acount-data-text">
							<span>Nombre:</span> Inmobiliaria Los Cruceros Limitada
						</div>
						<div className="acount-data-text">
							<span>Banco:</span> Banco Chile
						</div>
						<div className="acount-data-text">
							<span>Cta. Cte.:</span> 2680422303
						</div>
						<div className="acount-data-text">
							<span>Rut:</span> 76.449.442-3
						</div>
						<div className="acount-data-text">
							<span>Email: </span> contacto@praderasdefrutillar.cl
						</div>
					</React.Fragment>
				);
			case "recipt":
				return (
					<React.Fragment>
						<div className="acount-data-text">
							<span>A nombre de:</span> “Inmobiliaria Los Cruceros Limitada”
						</div>
						<div className="acount-data-text">
							<span>Rut: </span> 76.449.442-3
						</div>
						<div className="acount-data-text">
							<span>Valor del Vale Vista:</span> ${finalPrice}.-
						</div>
					</React.Fragment>
				);
			case "notary":
				return (
					<React.Fragment>
						<div className="acount-data-text">
							<span>NOTARÍA EN SANTIAGO:</span>
						</div>
						<div className="acount-data-text">
							<span>Notario:</span> Raúl Undurraga Laso
						</div>
						<div className="acount-data-text">
							<span>Dirección:</span> Mac-Iver 225, Of. 302, Santiago
						</div>
						<div className="acount-data-text">
							<span>Teléfono:</span> (+562) 2633 5225
						</div>
						<div className="acount-data-text mb-10">
							<span>Contacto:</span> Marcelo Concha Lunes a Viernes 9:30 a 14:00 - 15:30 a 18:00
						</div>
						<div className="acount-data-text">
							<span>NOTARÍA EN PUERTO VARAS:</span>
						</div>
						<div className="acount-data-text">
							<span>Notario:</span> Bernardo Espinosa Bancalari
						</div>
						<div className="acount-data-text">
							<span>Dirección:</span> Avenida Gramado N°535, Puerto Varas, Chile
						</div>
						<div className="acount-data-text">
							<span>Teléfono:</span> 065-2235718 - 065-2230890
						</div>
						<div className="acount-data-text">
							<span>Contacto:</span> escrituras@notariapuertovaras.cl Lunes a Viernes 9:00 a 14:00 - 15:30
							a 18:30
						</div>
					</React.Fragment>
				);
			case "uploadFileStepTwo":
			case "uploadFileStepThree":
				return <DropZone setType={setType} type={type} />;
			case "successUpload":
				return (
					<React.Fragment>
						<div className="acount-data-text-dropzone">
							<strong>{`Hemos recibido con éxito tu ${stepNumber === 1
								? "comprobante."
								: "garantía."}`}</strong>
						</div>
						<div className="acount-data-text-dropzone">
							Validaremos a la brevedad la transacción y te enviaremos el comprobante correspondiente
						</div>
						{/* <CheckCircle className="check-circle-green" /> */}
					</React.Fragment>
				);

			default:
				return null;
		}
	};

	return (
		<div>
			<Modal
				open={modalOpen}
				onClose={() => handleClose()}
				aria-labelledby="simple-modal-title"
				aria-describedby="simple-modal-description"
				className="d-flex ai-center jc-center"
			>
				<Paper className={classes.paper}>
					<div className="modal-header-account">
						<div className="modal-header-title">
							<InfoIcon style={{ color: "#FFFFFF", fontSize: "27px", marginRight: "5px" }} />{" "}
							{getModalHeaderText()}
						</div>
					</div>
					<div className="close-login-modal" onClick={() => handleClose()}>
						<CloseIcon style={{ color: "#FFFFFF", fontSize: "27px" }} className="profile-icon" />
					</div>
					<div className="info-account-modal-content">{getModalCont()}</div>
				</Paper>
			</Modal>
		</div>
	);
}
