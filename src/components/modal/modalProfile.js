import React, { useState, useContext, useEffect } from "react";
import Modal from "@material-ui/core/Modal";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import CloseIcon from "@material-ui/icons/Close";
import { makeStyles } from "@material-ui/core/styles";
import { ProfileModalContext, ProfileModalDispatch } from "../../context/profileModal.context";
import LoginCont from "../content/loginModalCont";
import RegisterCont from "../content/registerModalCont";
import ChangePassCont from "../content/changePassCont";
import LoginWithGoogle from "../button/loginWithGoogle";

const useStyles = makeStyles((theme) => ({
	paper: {
		width: "80%",
		height: "auto",
		maxWidth: "500px",
		maxHeight: "600px",
		overflowY: "hidden",
		position: "absolute",
		backgroundColor: "#EAEAEA"
	},
	"@media (max-width: 1070px)": {
		paper: {
			width: "100%",
			height: "100%",
			padding: "0px",
			display: "flex",
			maxWidth: "inherit",
			maxHeight: "inherit",
			flexDirection: "column"
		}
	},
	textField: {
		marginBottom: "10px",
		backgroundColor: "#fff",
		backgroundColor: "#F5F5F5",
		border: "3px solid #D9D9D9",
		borderRadius: "8px",
		"& label.Mui-focused": {
			color: "#ee7f0e"
		},
		"& .MuiInput-underline:after": {
			borderBottomColor: "green"
		},
		"& .MuiOutlinedInput-root": {
			"&.Mui-focused fieldset": {
				borderColor: "#ee7f0e"
			}
		}
	}
}));

export default function ProfileModal(ref) {
	const modalDispatch = useContext(ProfileModalDispatch);
	const dispatch = useContext(ProfileModalDispatch);
	const modalState = useContext(ProfileModalContext);
	const { modalCont = "login" } = modalState;
	// const [ modalCont, setModalCont ] = useState(typeof modalState.type !== "undefined" ? modalState.type : "login");
	const classes = useStyles();
	const closeLoginModal = () => {
		setModalCont("login");
		modalDispatch({ type: "UPDATE", payload: { open: false } });
	};

	const setModalCont = (type) => {
		dispatch({
			type: "UPDATE",
			payload: {
				modalCont: type
			}
		});
	};

	const changeModalCont = () => {
		if (modalCont === "login") {
			setModalCont("register");
		} else {
			setModalCont("login");
		}
	};

	const getModalCont = () => {
		switch (modalCont) {
			case "login":
				return <LoginCont setModalCont={(value) => setModalCont(value)} closeModal={() => closeLoginModal()} />;
			case "register":
				return <RegisterCont closeModal={() => closeLoginModal()} />;
			case "changePass":
				return (
					<ChangePassCont
						setModalCont={(value) => setModalCont(value)}
						closeModal={() => closeLoginModal()}
					/>
				);
			default:
				return <LoginCont setModalCont={(value) => setModalCont(value)} closeModal={() => closeLoginModal()} />;
		}
	};

	const getModalHeaderText = () => {
		switch (modalCont) {
			case "login":
				return "Inicia sesión";
			case "register":
				return "Regístrate";
			case "changePass":
				return "¿Tienes problemas para entrar?";
			default:
				return "Inicia sesión";
		}
	};

	const getTextByStatus = () => (modalCont === "login" ? "Registrarse" : "Iniciar sesión");

	return (
		<div>
			<Modal
				open={modalState.open}
				onClose={() => closeLoginModal()}
				aria-labelledby="simple-modal-title"
				aria-describedby="simple-modal-description"
				className="d-flex ai-center jc-center"
			>
				<Paper className={classes.paper}>
					<div className="modal-header-account">
						<img className="login-icon" src="/login-24px.svg" />
						<div className="modal-header-title">{getModalHeaderText()}</div>
					</div>
					<div className="close-login-modal" onClick={() => closeLoginModal()}>
						<CloseIcon style={{ color: "#FFFFFF", fontSize: "27px" }} className="profile-icon" />
					</div>
					<div className="login-modal-content">
						{getModalCont()}
						<div className="or-cont">
							<Divider className="divider-or" />
							<span className="or-modal-text"> o </span>
							<Divider className="divider-or" />
						</div>
						<LoginWithGoogle closeModal={() => closeLoginModal()} />
						<div className="register-text-cont">
							<div className="text-reg">
								{modalCont === "register" ? "¿Ya tienes una cuenta?" : "¿No tienes una cuenta?"}
							</div>{" "}
							<Button className="register-sub-btn" onClick={() => changeModalCont()}>
								{getTextByStatus()}
							</Button>
						</div>
					</div>
				</Paper>
			</Modal>
		</div>
	);
}
