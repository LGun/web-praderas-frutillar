import React, { useState, useContext, useEffect } from "react";
import { useRouter } from "next/router";
import find from "lodash/find";
import filter from "lodash/filter";
import dynamic from "next/dynamic";
import Grid from "@material-ui/core/Grid";
import Modal from "@material-ui/core/Modal";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import { makeStyles } from "@material-ui/core/styles";
import FavoriteIcon from "@material-ui/icons/Favorite";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";

import SingleMap from "../map/singleMap";
import Gallery from "../gallery/landinfoGallery";
import { LandContext } from "../../context/land.context";
import { MultimediaContext } from "../../context/multimedia.context";
import { createUserLikedLand, deleteUserLikedLand } from "../../services";
import { UserDispatchContext, UserContext } from "../../context/user.context";
import { ModalDispatchContext, ModalContext } from "../../context/modal.context";
const ImageGallery = dynamic(() => import("../card/carousel"));
const VideoComponent = dynamic(() => import("../video"));

const useStyles = makeStyles((theme) => ({
	paper: {
		position: "absolute",
		width: "80%",
		height: "80%",
		backgroundColor: "#EAEAEA",
		padding: "20px",
		maxWidth: "800px",
		overflowY: "auto"
	},
	"@media (max-width: 1070px)": {
		paper: {
			width: "100%",
			height: "100%"
		}
	}
}));

export default function LandInfo(ref) {
	const router = useRouter();
	const modalDispatch = useContext(ModalDispatchContext);
	const modalState = useContext(ModalContext);
	const userDispatch = useContext(UserDispatchContext);
	const userData = useContext(UserContext);
	const lands = useContext(LandContext);
	const multimedia = useContext(MultimediaContext);
	const classes = useStyles();
	const [ liked, setLiked ] = useState(false);
	const [ videoUrl, setVideoUrl ] = useState("https://www.youtube.com/embed/JgL-ubt-cNQ");
	const [ open, setOpen ] = useState(false);
	const handleClose = () => {
		modalDispatch({ type: "UPDATE", payload: { open: false } });
	};

	const goToContact = () => {
		handleClose();
		window.location.href = "#encuentra_tu_parcela";
		window.localStorage.setItem("wantedLandNumber", landNumber);
		const { profile = {} } = userData;
		const { customerId = "" } = profile;
		console.log("customerId", customerId);
		if (customerId) {
			router.push(`checklist?landNumber=${landNumber}`);
			// window.location.href = "/#contacto";
		} else {
			//preventa
			router.push(`preVenta?landNumber=${landNumber}`);
		}
	};

	const dislikeLand = (numberLand) => {
		const land = find(lands, { landNumber: numberLand });
		const newLikedLands = _.remove(userData.likedLands, (_land) => {
			return Number(_land) !== Number(land.landNumber);
		});
		userDispatch({
			type: "UPDATE",
			payload: {
				likedLands: typeof newLikedLands !== "undefined" ? newLikedLands : []
			}
		});
	};

	const saveLandLiked = (_landNumber) => {
		let likedValue = true;
		if (typeof userData.likedLands !== "undefined") {
			const totalLandLiked = userData.likedLands.find((element) => element === _landNumber);
			if (typeof totalLandLiked === "undefined") {
				userDispatch({
					type: "UPDATE",
					payload: {
						likedLands: [ ...userData.likedLands, _landNumber ]
					}
				});
				likedValue = true;
			} else {
				dislikeLand(_landNumber);
				likedValue = false;
			}
		} else {
			userDispatch({
				type: "UPDATE",
				payload: {
					likedLands: [ _landNumber ]
				}
			});
			likedValue = true;
		}
		setLiked(likedValue);
		if (userData.profile) {
			const { customerId = "" } = userData.profile;
			if (customerId) {
				if (likedValue) {
					createUserLikedLand({ customerId: userData.profile.customerId, landNumber: _landNumber });
				} else {
					deleteUserLikedLand({ customerId: userData.profile.customerId, landNumber: _landNumber });
				}
			}
		}
	};

	useEffect(
		() => {
			const land = find(lands, { landNumber: modalState.landNumber }) || {};
			modalDispatch({ type: "UPDATE", payload: { land } });
			setVideoUrl("https://www.youtube.com/embed/JgL-ubt-cNQ");
			if (land) {
				const multimediaFiltered =
					filter(
						multimedia.oldData,
						(item) =>
							typeof item.sections !== "undefined" && item.sections.includes(String(land.landNumber))
					) || [];
				if (multimediaFiltered && typeof multimediaFiltered[0] !== "undefined") {
					if (typeof multimediaFiltered[0].urlVideo !== "undefined" && multimediaFiltered[0].urlVideo) {
						setVideoUrl(multimediaFiltered[0].urlVideo);
					}
				}
			}
		},
		[ modalState.landNumber ]
	);
	const { land = {} } = modalState;
	const {
		landNumber = "",
		servidumbreNumber = "",
		price = "",
		discount = "",
		totalSize = "",
		appraisalNumber = "",
		multimediaList = []
	} = land;
	console.log("LAND_", JSON.stringify(land));

	const getLandVideoUrl = () => {
		let _multimediaLand = {};
		if (multimediaList.length > 0) {
			_multimediaLand = multimediaList[0];
		}
		const { urlMultimedia = "" } = _multimediaLand;
		return urlMultimedia || "https://www.youtube.com/embed/JgL-ubt-cNQ";
	};

	const openModal = (config) => {
		setCurrentDisplayItem(config);
		setOpen(true);
	};

	return (
		<div>
			<Modal
				open={modalState.open}
				onClose={handleClose}
				aria-labelledby="simple-modal-title"
				aria-describedby="simple-modal-description"
				className="d-flex ai-center jc-center"
			>
				<Paper className={classes.paper}>
					<div className="pdp-header">
						<h2 className="pdp-modal-title">{`FICHA PARCELA ${landNumber || ""}`}</h2>
					</div>

					<div className="close-land-modal" onClick={() => handleClose()}>
						<img src="/icono-contraer.svg" />
					</div>

					<p className="pdp-subtitle">
						PARCELAS | <span>{`FICHA PARCELA Nº ${landNumber || ""}`}</span>
					</p>
					<VideoComponent url={getLandVideoUrl()} />
					<div className="d-flex jc-space-b padding-20">
						<div className="d-flex flex-1 jc-start ai-center row">
							<p className="pdp-subtitle padding-0">
								{`${totalSize} m²`}{" "}
								<strong>| {`$ ${Number(price || 0).toLocaleString("es-ES")}`} </strong>
							</p>
						</div>
						<div className="d-flex ai-center flex-1 jc-end row">
							<div className="d-flex ai-center semi-black simple-p-modal bold">
								Me gusta
								<IconButton onClick={() => saveLandLiked(landNumber)}>
									{liked ? (
										<FavoriteIcon className="green-color" />
									) : (
										<FavoriteBorderIcon className="green-color" />
									)}
								</IconButton>
							</div>

							<Button
								onClick={() => goToContact()}
								className="orange-btn"
								variant="contained"
								size="small"
								color="primary"
							>
								LA QUIERO
							</Button>
						</div>
					</div>
					<div className="divider-green-desktop" />
					<div className="pdp-header">
						<h2 className="pdp-modal-car">- Características -</h2>
					</div>
					<Grid container>
						<Grid item xs={6} md={4}>
							<span className="strong">Superficie</span>: {`${totalSize || ""} m²`}
						</Grid>
						<Grid item xs={6} md={4}>
							<span className="strong">Servidumbre</span>: {`${servidumbreNumber || ""} m²`}
						</Grid>
						<Grid item xs={6} md={4}>
							<span className="strong">Rol</span>: {appraisalNumber}
						</Grid>
						<Grid item xs={6} md={4}>
							<span className="strong">Gastos comunes</span>: 0,6 UF
						</Grid>
						<Grid item xs={6} md={4}>
							<span className="strong">Precio</span>: {`$ ${Number(price || 0).toLocaleString("es-ES")}`}
						</Grid>
						<Grid item xs={6} md={4}>
							<span className="strong">Descuento:</span> {`${discount || ""} %`}
						</Grid>
						{multimediaList.length > 1 && (
							<React.Fragment>
								<div className="pdp-header padding-20 pd-b-0">
									<h2 className="pdp-modal-cont">- Contenido Multimedia -</h2>
								</div>
								<div className="full-width mt-negative-10">
									<Gallery multimediaList={multimediaList} />
								</div>
							</React.Fragment>
						)}
						<div className="pdp-header padding-20">
							<h2 className="pdp-modal-map">- Mapa de la Parcela -</h2>
						</div>
						<div className="full-width">
							<SingleMap landNumber={landNumber} />
						</div>
					</Grid>
				</Paper>
			</Modal>
		</div>
	);
}
