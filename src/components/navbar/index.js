import React, { useState } from "react";
import { useRouter } from "next/router";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import CloseIcon from "@material-ui/icons/Close";
import DraftsIcon from "@material-ui/icons/Drafts";
import LinkSmoothScroll from "../scroll";
import MobileProfileBtn from "../button/mobileProfileBtn";
const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1
	},
	toolbar: {
		backgroundColor: "#EAEAEA",
		height: "68px"
	},
	menuIcon: {
		color: "#94B200",
		fontSize: "30px"
	},
	title: {
		flexGrow: 1
	},
	drawerPaper: {
		maxWidth: "70%"
	}
}));

const useStylesDrawer = makeStyles((theme) => ({
	root: {
		display: "flex",
		width: "auto"
	},
	appBar: {
		zIndex: theme.zIndex.drawer + 1
	},
	drawer: {
		width: "95%",
		flexShrink: 0,
		height: "auto"
	},
	drawerPaper: {
		width: "95%",
		height: "auto",
		backgroundColor: "#525252",
		top: "20px"
	},
	content: {
		flexGrow: 1,
		padding: theme.spacing(3)
	},
	toolbar: theme.mixins.toolbar
}));

export default function ButtonAppBar() {
	const [ open, setOpen ] = useState(false);
	const router = useRouter();
	const classes = useStyles();
	const classesDrawer = useStylesDrawer();
	return (
		<div className={`${classes.root} mobile-only`}>
			<AppBar position="fixed">
				<Toolbar className={classes.toolbar}>
					<div className="nav-flex-cont">
						<IconButton
							onClick={() => setOpen(true)}
							edge="start"
							className={classes.menuButton}
							color="inherit"
							aria-label="menu"
						>
							<MenuIcon className={classes.menuIcon} />
						</IconButton>
						<LinkSmoothScroll href="/#home">
							<Button className="absolute-logo" color="inherit">
								<LazyLoadImage className="logo-mobile" src="/Logo-png@2x.png" alt="logo" />
							</Button>
						</LinkSmoothScroll>

						<div className="d-flex ai-center">
							<LinkSmoothScroll href="/#home">
								<a href="#home">
									<LazyLoadImage className="home-icon" src="/home-water.svg" alt="logo" />
								</a>
							</LinkSmoothScroll>
							<MobileProfileBtn />
						</div>
					</div>
				</Toolbar>
			</AppBar>
			<Drawer
				className={classesDrawer.drawer}
				classes={{
					paper: classesDrawer.drawerPaper
				}}
				open={open}
				onClose={() => setOpen(false)}
			>
				{/* {list(anchor)} */}
				<div className="close-icon-drawer" onClick={() => setOpen(false)}>
					<CloseIcon />
				</div>
				<List className="drawer-list" component="nav" aria-label="main mailbox folders">
					<ListItem className="list-item-g">
						<ListItemIcon>
							<LazyLoadImage className="mobile-white-logo" src="/Imagen 42@2x.png" alt="logo" />
						</ListItemIcon>
						<ListItemText className="nav-bold" primary="Proyecto Praderas de Frutillar" />
					</ListItem>
					<ListItem button onClick={() => setOpen(false)}>
						<LazyLoadImage className="nav-home-icon" src="/nav-home-water.svg" alt="logo" />
						<LinkSmoothScroll href="/#home">
							<ListItemText primary="INICIO" />
						</LinkSmoothScroll>
					</ListItem>
					<Divider className="white-divider" />
					<ListItem
						button
						onClick={() => {
							setOpen(false);
							router.push({ pathname: "/checklist" });
						}}
					>
						<ListItemText primary="| Checklist de compra" />
					</ListItem>
					<Divider className="white-divider" />
					<ListItem
						button
						onClick={() => {
							setOpen(false);
							router.push({ pathname: "/faq" });
						}}
					>
						<ListItemText primary="| Preguntas frecuentes" />
					</ListItem>
					<Divider className="white-divider" />
					<ListItem button onClick={() => setOpen(false)}>
						<LinkSmoothScroll href="/#caracteristicas">
							<ListItemText primary="| Características del proyecto" />
						</LinkSmoothScroll>
					</ListItem>
					<Divider className="white-divider" />
					<ListItem button onClick={() => setOpen(false)}>
						<LinkSmoothScroll href="/#nuestra_ubicacion">
							<ListItemText primary="| Nuestra ubicación" />
						</LinkSmoothScroll>
					</ListItem>
					<Divider className="white-divider" />
					<ListItem button onClick={() => setOpen(false)}>
						<LinkSmoothScroll href="/#encuentra_tu_parcela">
							<ListItemText primary="| Características y disponibilidad de las parcelas" />
						</LinkSmoothScroll>
					</ListItem>
					<Divider className="white-divider" />
					<ListItem button onClick={() => setOpen(false)}>
						<LinkSmoothScroll href="/#lugares_cercanos">
							<ListItemText primary="| Lugares cercanos" />
						</LinkSmoothScroll>
					</ListItem>
					<Divider className="white-divider" />
					<ListItem button onClick={() => setOpen(false)}>
						<LinkSmoothScroll href="/#testimonios">
							<ListItemText primary="| Testimonios" />
						</LinkSmoothScroll>
					</ListItem>
					<Divider className="white-divider" />
					<ListItem button onClick={() => setOpen(false)}>
						<LinkSmoothScroll href="/#contacto">
							<ListItemText primary="> Contáctanos" />
						</LinkSmoothScroll>
					</ListItem>
					<Divider className="white-divider" />
					<ListItem button />
				</List>
			</Drawer>
		</div>
	);
}
