import React from "react";
import { useRouter } from "next/router";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import PinDropIcon from "@material-ui/icons/PinDrop";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import Slide from "@material-ui/core/Slide";
import Tooltip from "@material-ui/core/Tooltip";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";

import SubContentNav from "./subContentNav";
import LinkSmoothScroll from "../scroll";
import { sendEvent } from "../../utils/googleTagManager";
import ProfileBtn from "../button/profileBtn";

function HideOnScroll(props) {
	const { children, window } = props;
	const trigger = useScrollTrigger({ target: window ? window() : undefined });
	return (
		<Slide appear={false} direction="down" in={trigger}>
			{children}
		</Slide>
	);
}

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
		backgroundColor: "#94B200"
	},
	appBar: {
		backgroundColor: "#94B200"
	},
	toolbar: {
		backgroundColor: "#94B200",
		height: "40px",
		minHeight: "40px",
		display: "flex",
		alignItems: "center",
		justifyContent: "flex-end",
		margin: "0px auto",
		maxWidth: "75rem",
		width: "100%"
	},
	menuIcon: {
		color: "#94B200",
		fontSize: "30px"
	},
	menuButton: {
		marginRight: theme.spacing(2)
	},
	title: {
		flexGrow: 1
	},
	anchor: {
		cursor: "pointer",
		padding: "10px",
		fontWeight: 600
	},
	divider: {
		backgroundColor: "#FFF",
		margin: "5px 0px"
	},
	paper: {
		position: "absolute",
		width: "80%",
		height: "auto",
		maxHeight: "600px",
		backgroundColor: "#EAEAEA",
		// padding: "20px",
		maxWidth: "500px",
		overflowY: "hidden"
	},
	"@media (max-width: 1070px)": {
		paper: {
			padding: "0px",
			width: "100%",
			height: "100%",
			display: "flex",
			flexDirection: "column",
			maxWidth: "inherit",
			maxHeight: "inherit"
		}
	},
	textField: {
		marginBottom: "10px",
		backgroundColor: "#fff",
		backgroundColor: "#F5F5F5",
		border: "3px solid #D9D9D9",
		borderRadius: "8px",
		"& label.Mui-focused": {
			color: "#ee7f0e"
		},
		"& .MuiInput-underline:after": {
			borderBottomColor: "green"
		},
		"& .MuiOutlinedInput-root": {
			"&.Mui-focused fieldset": {
				borderColor: "#ee7f0e"
			}
		}
	}
}));

export default function DesktopAppBar() {
	const classes = useStyles();
	const router = useRouter();
	const _socials = (
		<div className="relative">
			<Tooltip title="Canal de YouTube" aria-label="YouTube">
				<a href="https://www.youtube.com/channel/UCE6lQ4At61BXe80l1pSUang" target="_blank">
					<LazyLoadImage className="social-icon" src="/you-tube.svg" alt="youtube" />
				</a>
			</Tooltip>
			<Tooltip title="Instagram" aria-label="instagram">
				<a href="https://instagram.com/praderas_de_frutillar?igshid=1nxirl6qv0ihk" target="_blank">
					<LazyLoadImage className="social-icon" src="/instagram-bosquejado@2x.png" alt="instagram" />
				</a>
			</Tooltip>
			<Tooltip title="Facebook" aria-label="facebook">
				<a
					href="https://es-la.facebook.com/pages/category/Real-Estate-Company/Fundo-Praderas-de-Frutillar-187686168544096/"
					target="_blank"
				>
					<LazyLoadImage className="social-icon" src="/facebook@2x.png" alt="facebook" />
				</a>
			</Tooltip>
			<Tooltip title="Whatsapp" aria-label="whatsapp">
				<a
					className="clickable"
					onClick={() => {
						window.open(
							"https://wa.me/56975790254?text=¡Hola!%20estoy%20en%20https://praderasdefrutillar.cl%20mi%20nombre%20es:",
							"_system"
						);
					}}
				>
					<LazyLoadImage className="social-icon" src="/whatsapp@2x.png" alt="whatsapp" />
				</a>
			</Tooltip>
			<div className="reason-border" />
		</div>
	);

	const handleClickProfile = () => {
		const userIsLoggedIn = false;
		if (!userIsLoggedIn) {
			setLogInModal(true);
		}
	};

	return (
		<div className="desktop-only">
			<div className={classes.root}>
				<AppBar className={classes.appBar} position="static">
					<Toolbar className={classes.toolbar}>
						<ProfileBtn handleClick={() => handleClickProfile()} />
						{_socials}
						<Tooltip title="Ubicación" aria-label="ubicación">
							<a
								href="https://www.google.com/maps/place/%22Fundo+Praderas+de+Frutillar%22/@-41.1271881,-73.1335007,11.57z/data=!4m5!3m4!1s0x9617f27fd9c8bd07:0xdd15cc2f22d767c0!8m2!3d-41.1443476!4d-73.1351875"
								target="_blank"
								className="flex-row relative"
							>
								<PinDropIcon style={{ color: "#292929", fontSize: "27px" }} className="profile-icon" />
								<div className="vertical-cont">
									<div>
										<strong>Frutillar</strong>
									</div>
									<div>Frutillar X Región de los Lagos</div>
								</div>
							</a>
						</Tooltip>
					</Toolbar>
				</AppBar>
				<HideOnScroll>
					<AppBar className={classes.root}>
						<Toolbar>
							{/* <LinkSmoothScroll href="#top" onClick={() => window.scrollTo(0, 0)}> */}
							<LazyLoadImage
								className="desktop-white-logo clickable"
								src="/Imagen 42@2x.png"
								alt="logo"
								onClick={() => window.scrollTo(0, 0)}
							/>
							{/* </LinkSmoothScroll> */}
							<Grid container alignItems="center" className={`${classes.root} nav-menu-cont jc-center`}>
								<a className={classes.anchor} onClick={() => router.push({ pathname: "/checklist" })}>
									CHECK LIST DE COMPRA
								</a>
								<Divider className={classes.divider} orientation="vertical" flexItem />
								<a className={classes.anchor} onClick={() => router.push({ pathname: "/faq" })}>
									PREGUNTAS FRECUENTES
								</a>
								<Divider className={classes.divider} orientation="vertical" flexItem />
								<LinkSmoothScroll href="/#tu_mejor_inversion">
									<a className={classes.anchor}>CONOCE EL PROYECTO</a>
								</LinkSmoothScroll>
								{/* <a className={classes.anchor} href="#">
									PRIMEROS PASOS A LA COMPRA
								</a>
								<Divider className={classes.divider} orientation="vertical" flexItem />
								<a className={classes.anchor} href="#">
									EN PROCESO
								</a> */}
								<Divider className={classes.divider} orientation="vertical" flexItem />
								<a className={classes.anchor} href="/#encuentra_tu_parcela">
									LAS PARCELAS
								</a>
								<Divider className={classes.divider} orientation="vertical" flexItem />
								<a className={classes.anchor} href="/#contacto">
									CONTACTO
								</a>
							</Grid>
							{_socials}
							<a
								href="https://www.google.com/maps/place/%22Fundo+Praderas+de+Frutillar%22/@-41.1271881,-73.1335007,11.57z/data=!4m5!3m4!1s0x9617f27fd9c8bd07:0xdd15cc2f22d767c0!8m2!3d-41.1443476!4d-73.1351875"
								target="_blank"
								className="flex-row relative"
							>
								<PinDropIcon style={{ color: "#292929", fontSize: "27px" }} className="profile-icon" />
							</a>
						</Toolbar>
					</AppBar>
				</HideOnScroll>
			</div>
			<SubContentNav />
		</div>
	);
}
