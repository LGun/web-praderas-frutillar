import React from "react";
import { useRouter } from "next/router";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { makeStyles } from "@material-ui/core/styles";
import MenuNav from "../menu/navMenu";
const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
		height: "100px",
		backgroundColor: "#E5E5E5",
		position: "relative"
	}
}));

export default function SubContentNav() {
	const classes = useStyles();
	const router = useRouter();
	return (
		<div className={classes.root}>
			<div className="sub-header-cont">
				<LazyLoadImage
					className="desktop-logo clickable"
					src="/Logo-png@2x.png"
					alt="logo"
					onClick={() => router.push(`/`)}
				/>
				<div className="desktop-logo" />
				<div className="absolute-cont">
					<MenuNav />
				</div>
			</div>
			<div className="complete-line" />
		</div>
	);
}
