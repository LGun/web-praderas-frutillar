import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

const useStyles = makeStyles((theme) => ({
	indicator: {
		backgroundColor: "#555555"
	},
	flexContainer: {
		height: "40px"
	}
}));

export default function DisabledTabs() {
	const [ value, setValue ] = React.useState(0);

	const handleChange = (event, newValue) => {
		setValue(newValue);
	};

	const classes = useStyles();
	return (
		<Paper className="mobile-only" square>
			<Tabs
				value={value}
				indicatorColor="primary"
				textColor="primary"
				onChange={handleChange}
				aria-label="disabled tabs example"
				id="tabs-cont"
				classes={{
					indicator: classes.indicator
					// flexContainer: classes.flexContainer
				}}
			>
				<Tab className="tab-size color-tab tab-one" label="CONOCE" />
				<Tab className="tab-size color-tab tab-two" label="COMPRA" />
				<Tab className="tab-size color-tab tab-three" label="DISFRUTA" />
			</Tabs>
		</Paper>
	);
}
