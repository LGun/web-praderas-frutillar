import React from "react";
import dynamic from "next/dynamic";
import { LazyLoadImage } from "react-lazy-load-image-component";
import Grid from "@material-ui/core/Grid";

import { makeStyles } from "@material-ui/core/styles";
const VideoComponent = dynamic(() => import("../video"));
const useStyles = makeStyles({
	root: {
		width: "100%",
		maxWidth: "75rem",
		display: "flex",
		flexDirection: "column",
		margin: "0px auto",
		backgroundColor: "#FAFAFA",
		paddingBottom: "3rem",
		paddingTop: "3rem",
		paddingLeft: "1rem",
		paddingRight: "1rem"
	}
});

export default function SimpleCont() {
	const classes = useStyles();

	return (
		<div id="tu_mejor_inversion" className={classes.root}>
			<div className="d-flex ai-center jc-center column">
				<h1 className="simple-title green padding-b20">
					<strong>-Tu Mejor Inversión-</strong>
				</h1>
				<Grid container spacing={3}>
					<Grid item xs={6}>
						<div className="main-video-cont">
							<VideoComponent
								width="100%"
								url="https://www.youtube.com/watch?v=JgL-ubt-cNQ&feature=youtu.be"
							/>
						</div>
					</Grid>
					<Grid item xs={6}>
						<Grid container spacing={3} className="full-height">
							<Grid xs={3}>
								<div className="d-flex ai-center jc-center column full-height">
									<LazyLoadImage
										id="fibra-icon"
										className="icon-image padding-b10"
										src="/fibra.svg"
									/>
									<p className="icon-box-text green">FIBRA ÓPTICA</p>
								</div>
							</Grid>
							<Grid xs={3}>
								<div className="d-flex ai-center jc-center column full-height">
									<LazyLoadImage className="icon-image padding-b10" src="/electricidad.svg" />
									<p className="icon-box-text green">ELECTRICIDAD SUBTERRÁNEA</p>
								</div>
							</Grid>
							<Grid xs={3}>
								<div className="d-flex ai-center jc-center column full-height">
									<LazyLoadImage className="icon-image padding-b10" src="/agua.svg" />
									<p className="icon-box-text green">AGUA TRATADA CON DISTRIBUCIÓN SUBTERRÁNEA</p>
								</div>
							</Grid>
							<Grid xs={3}>
								<div className="d-flex ai-center jc-center column full-height">
									<LazyLoadImage className="icon-image padding-b10" src="/caminos.svg" />
									<p className="icon-box-text green">AMPLIOS CAMINOS</p>
								</div>
							</Grid>
							<Grid xs={3}>
								<div className="d-flex ai-center jc-center column full-height">
									<LazyLoadImage className="icon-image padding-b10" src="/club.svg" />
									<p className="icon-box-text green">CLUB HOUSE</p>
								</div>
							</Grid>
							<Grid xs={3}>
								<div className="d-flex ai-center jc-center column full-height">
									<LazyLoadImage className="icon-image padding-b10" src="/tube.svg" />
									<p className="icon-box-text green">HOT TUBE</p>
								</div>
							</Grid>
							<Grid xs={3}>
								<div className="d-flex ai-center jc-center column full-height">
									<LazyLoadImage className="icon-image padding-b10" src="/plaza.svg" />
									<p className="icon-box-text green">PLAZA ENCANTADORA</p>
								</div>
							</Grid>
							<Grid xs={3}>
								<div className="d-flex ai-center jc-center column full-height">
									<LazyLoadImage className="icon-image padding-b10" src="/espejo.svg" />
									<p className="icon-box-text green">ESPEJO DE AGUA</p>
								</div>
							</Grid>
						</Grid>
					</Grid>
				</Grid>
			</div>
		</div>
	);
}
