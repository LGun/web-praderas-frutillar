import React, { useState, useEffect, useContext } from "react";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import { makeStyles, withStyles, useTheme } from "@material-ui/core/styles";
import CheckCircle from "@material-ui/icons/CheckCircle";
import { StepModalDispatchContext, StepModalContext } from "../../context/stepModal.context";
const useStyles = makeStyles((theme) => ({
	select: {
		"&:before": {
			borderColor: "#ff8f1c"
		},
		"&:after": {
			borderColor: "#ff8f1c"
		}
	},
	icon: {
		fill: "#ff8f1c"
	},
	formControl: {
		width: "100%"
	},
	indicator: {
		"& > span": {
			backgroundColor: "#555555"
		},
		backgroundColor: "#555555"
	},
	textField: {
		marginBottom: "10px",
		backgroundColor: "#fff",
		backgroundColor: "#F5F5F5",
		border: "3px solid #D9D9D9",
		borderRadius: "8px",
		"& label.Mui-focused": {
			color: "#ee7f0e"
		},
		"& .MuiInput-underline:after": {
			borderBottomColor: "green"
		},
		"& .MuiOutlinedInput-root": {
			"&.Mui-focused fieldset": {
				borderColor: "#ee7f0e"
			}
		}
	},
	backdrop: {
		zIndex: theme.zIndex.drawer + 1,
		color: "#fff"
	},
	buttonProgress: {
		color: "#FFF"
	}
}));

export default function SuccessStepOne({ closeModal, setModalCont }) {
	const stepModalDispatch = useContext(StepModalDispatchContext);
	const [ loading, setLoading ] = useState(false);
	const handleClose = () => {
		stepModalDispatch({ type: "UPDATE", payload: { open: false } });
	};
	return (
		<React.Fragment>
			<div className="success-step-one-cont">
				<CheckCircle className="success-check-circle" />
				<div className="text-so-s-title">¡Felicitaciones!</div>
				<div className="text-so-s2-title">
					Hemos recibido tu formulario con éxito, nuestro equipo te contactará a la brevedad
				</div>
			</div>
			<div className="buttons-form-one-cont">
				<Button
					id="contact-button"
					type="submit"
					value="Send"
					className="login-button mt-10"
					variant="contained"
					color="primary"
					onClick={() => handleClose()}
				>
					{loading ? <CircularProgress size={26} className={classes.buttonProgress} /> : "CONTINUAR"}
				</Button>
			</div>
		</React.Fragment>
	);
}
