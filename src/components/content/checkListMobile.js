import React, { useState, useContext, useEffect } from "react";
import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import { withStyles } from "@material-ui/core/styles";
import FormControlLabel from "@material-ui/core/FormControlLabel";

import firebase from "../../firebase";
import { updateUser } from "../../services";
import AccountInfoModal from "../modal/accountInfoModal";
import { StepModalDispatchContext } from "../../context/stepModal.context";
import { UserDispatchContext, UserContext } from "../../context/user.context";

function createData(step, responsable, time, description, download) {
	return { step, responsable, time, description, download };
}

const CustomCheckBox = withStyles({
	root: {
		color: "#FF8F1C",
		"&$checked": {
			color: "#FF8F1C"
		}
	},
	checked: {}
})((props) => <Checkbox color="default" {...props} />);

export default function CheckListMobile() {
	const userDispatch = useContext(UserDispatchContext);
	const userData = useContext(UserContext);
	const [ stepSevenCheck, setStepSevenCheck ] = useState(false);
	const [ stepEightCheck, setStepEightCheck ] = useState(false);
	const { profile = {} } = userData;
	const {
		stepNumber = 0,
		urlFileWarranty = "",
		urlFileSeriousnessWarranty = "",
		urlFileSignedSeriousnessWarranty = "",
		urlFileIntentionLetter = "",
		urlFileSignedDeedNotarized = "",
		urlFileDomainOwnershipCertificate = "",
		urlFileOthersCertificates = "",
		urlFilePublicDeed = "",
		customerId = "",
		step1 = {}
	} = profile;

	const [ stepTwoInfoModal, setStepTwoInfoModal ] = useState(false);
	const [ typeInfoModal, setTypeInfoModal ] = useState("info_account");

	const stepModalDispatch = useContext(StepModalDispatchContext);

	const getStatusByRow = (index) => {
		if (index === stepNumber) {
			return "active";
		} else if (index < stepNumber) {
			return "done";
		}
		return "";
	};
	const getNumber = (number) => {
		const finalNumber = number + 1;
		if (finalNumber.toString().length === 1) {
			return `0${finalNumber}`;
		}
		return finalNumber;
	};

	const handleOpenModal = () => {
		stepModalDispatch({ type: "UPDATE", payload: { open: true } });
	};

	const getBtnCss = (_step) => {
		return _step === stepNumber ? "form-button-step" : "form-button-step-two";
	};

	const handleCheckBoxChange = (_stepNumber) => {
		switch (_stepNumber) {
			case 7:
				setStepSevenCheck(!stepSevenCheck);
				updateUser({
					customerId,
					step7: {
						stateStep7: "En validación",
						stepNumber: 7,
						timeCreation: firebase.firestore.FieldValue.serverTimestamp(),
						isStepCheckedByCustomer: !stepSevenCheck
					}
				});
				userDispatch({
					type: "UPDATE",
					payload: {
						profile: {
							...profile,
							step7: {
								stateStep7: "En validación",
								stepNumber: 7,
								timeCreation: firebase.firestore.FieldValue.serverTimestamp(),
								isStepCheckedByCustomer: !stepSevenCheck
							}
						}
					}
				});
				break;
			case 8:
				setStepEightCheck(!stepEightCheck);
				updateUser({
					customerId,
					step8: {
						stateStep8: "En validación",
						stepNumber: 8,
						timeCreation: firebase.firestore.FieldValue.serverTimestamp(),
						isStepCheckedByCustomer: !stepEightCheck
					}
				});
				userDispatch({
					type: "UPDATE",
					payload: {
						profile: {
							...profile,
							step8: {
								stateStep8: "En validación",
								stepNumber: 8,
								timeCreation: firebase.firestore.FieldValue.serverTimestamp(),
								isStepCheckedByCustomer: !stepEightCheck
							}
						}
					}
				});
				break;
			default:
				break;
		}
	};

	useEffect(
		() => {
			const { profile = {} } = userData;
			const { step7 = {}, step8 = {} } = profile;
			const { isStepCheckedByCustomer: _stepSevenCheck = false } = step7;
			const { isStepCheckedByCustomer: _stepEightCheck = false } = step8;
			if (_stepSevenCheck) {
				setStepSevenCheck(_stepSevenCheck);
			}
			if (_stepEightCheck) {
				setStepEightCheck(_stepEightCheck);
			}
		},
		[ userData ]
	);

	const getStepStatus = (_stepNumber) => {
		if (typeof profile[`step${_stepNumber}`] !== "undefined" && profile[`step${_stepNumber}`]) {
			if (
				typeof profile[`step${_stepNumber}`][`stateStep${_stepNumber}`] !== "undefined" &&
				profile[`step${_stepNumber}`][`stateStep${_stepNumber}`]
			) {
				return profile[`step${_stepNumber}`][`stateStep${_stepNumber}`];
			}
		}
		return "Por Hacer";
	};

	const rows = [
		createData(
			"Completar Formulario",
			"Comprador",
			getStepStatus(1),
			"Acontinuación se desplegará el formulario que debes completar con tus datos, para comenzar con los trámites de compraventa",
			<React.Fragment>
				<Button
					id="step-one-btn"
					className={`mobile-mt-5 ${getBtnCss(0)} checklist-btn-mob ${getStepStatus(1) !== "Por Hacer" &&
						"btn-success-green"}`}
					variant="contained"
					color="primary"
					onClick={() => handleOpenModal()}
					disabled={0 !== stepNumber}
				>
					COMPLETAR FORMULARIO
				</Button>
			</React.Fragment>
		),
		createData(
			"Transferencia de Garantía",
			"Comprador",
			getStepStatus(2),
			"Debes realizar la Transferencia de Garantía y enviarnos el comprobante por los $3.000.000 por concepto de GARANTÍA DE SERIEDAD DE OFERTA, a la cuenta corriente de Inmobiliaria Los Cruceros Limitada",
			<React.Fragment>
				<Button
					id="step-one-btn"
					className={`mobile-mt-5 mb-10 ${getBtnCss(1)} checklist-btn-mob ${getStepStatus(2) !==
						"Por Hacer" && "btn-success-green"}`}
					variant="contained"
					color="primary"
					onClick={() => {
						setStepTwoInfoModal(true);
						setTypeInfoModal("info_account");
					}}
				>
					DATOS TRANSFERENCIA
				</Button>
				<Button
					id="step-one-btn"
					className={`mobile-mt-5 ${getBtnCss(1)} checklist-btn-mob ${getStepStatus(2) !== "Por Hacer" &&
						"btn-success-green"}`}
					variant="contained"
					color="primary"
					onClick={() => {
						setStepTwoInfoModal(true);
						setTypeInfoModal("uploadFileStepTwo");
					}}
					disabled={1 !== stepNumber}
				>
					SUBIR COMPROBANTE
				</Button>
			</React.Fragment>
		),
		createData(
			"Recibo Garantía por Seriedad de la Oferta para ser firmada por el comprador",
			"Comprador",
			getStepStatus(3),
			"A continuación descargar el documento “Garantía por Seriedad de la oferta” y luego subirlo ya firmada por ti.",
			<React.Fragment>
				<Button
					id="step-one-btn"
					className="form-button-step-two mobile-mt-5 mb-10"
					variant="contained"
					color="primary"
					onClick={() => window.open(urlFileSeriousnessWarranty, "_blank")}
					disabled={!urlFileSeriousnessWarranty}
					className={`mobile-mt-5 mb-10 ${getBtnCss(2)} checklist-btn-mob ${(getStepStatus(3) !==
						"Por Hacer" ||
						urlFileSeriousnessWarranty) &&
						"btn-success-green"}`}
				>
					DESCARGAR GARANTÍA
				</Button>
				<Button
					id="step-one-btn"
					className={`mobile-mt-5 ${getBtnCss(2)} checklist-btn-mob ${getStepStatus(3) !== "Por Hacer" &&
						"btn-success-green"}`}
					variant="contained"
					color="primary"
					onClick={() => {
						setStepTwoInfoModal(true);
						setTypeInfoModal("uploadFileStepThree");
					}}
					disabled={2 !== stepNumber}
				>
					SUBIR GARANTÍA
				</Button>
			</React.Fragment>
		),
		createData(
			"Descarga Garantía por seriedad de la oferta, firmada por representantes de la inmobiliaria",
			"Vendedor",
			getStepStatus(4),
			"Descargar “Garantía por seriedad de la oferta” como comprobante de la transferencia y aceptación de la oferta.",
			<React.Fragment>
				<Button
					id="step-one-btn"
					className={`mobile-mt-5 mb-10 ${getBtnCss(
						3
					)} checklist-btn-mob ${!urlFileSignedSeriousnessWarranty && "force-grey"} ${(getStepStatus(4) !==
						"Por Hacer" ||
						urlFileSignedSeriousnessWarranty) &&
						"btn-success-green"}`}
					variant="contained"
					color="primary"
					disabled={3 !== stepNumber}
					disabled={!urlFileSignedSeriousnessWarranty}
					onClick={() => window.open(urlFileSignedSeriousnessWarranty, "_blank")}
				>
					DESCARGAR GARANTÍA
				</Button>
			</React.Fragment>
		),
		createData(
			"Envío de escritura al comprador y notaría",
			"Vendedor",
			getStepStatus(5),
			"Enviamos a la Notaría y al Comprador, la Escritura de Compraventa y Carta de Instrucción documento que puedes descargar a continuación.",
			<React.Fragment>
				<Button
					id="step-one-btn"
					className={`mobile-mt-5 mb-10 ${getBtnCss(4)} checklist-btn-mob ${(getStepStatus(5) !==
						"Por Hacer" ||
						urlFilePublicDeed) &&
						"btn-success-green"}`}
					variant="contained"
					color="primary"
					disabled={!urlFilePublicDeed}
					onClick={() => window.open(urlFilePublicDeed, "_blank")}
				>
					DESCARGAR ESCRITURA DE COMPRAVENTA
				</Button>
				<Button
					id="step-one-btn"
					className={`mobile-mt-5 ${getBtnCss(4)} checklist-btn-mob ${(getStepStatus(5) !== "Por Hacer" ||
						urlFileIntentionLetter) &&
						"btn-success-green"}`}
					variant="contained"
					color="primary"
					disabled={!urlFileIntentionLetter}
					onClick={() => window.open(urlFileIntentionLetter, "_blank")}
				>
					DESCARGAR CARTA DE INSTRUCCIÓN
				</Button>
			</React.Fragment>
		),
		createData(
			"Datos de vale vista y notaria",
			"Comprador",
			getStepStatus(6),
			"¡IMPORTANTE! Debes sacar el Vale Vista en el banco con los siguientes datos para entregarlo en el momento de la firma de la escritura que será entregado al Notario para su custodia",
			<React.Fragment>
				<Button
					id="step-one-btn"
					className={`mobile-mt-5 mb-10 ${getBtnCss(5)} checklist-btn-mob ${getStepStatus(6) !==
						"Por Hacer" && "btn-success-green"}`}
					variant="contained"
					color="primary"
					onClick={() => {
						setStepTwoInfoModal(true);
						setTypeInfoModal("recipt");
					}}
				>
					VER DATOS PARA VALE VISTA
				</Button>
				<Button
					id="step-one-btn"
					className={`mobile-mt-5 ${getBtnCss(5)} checklist-btn-mob ${getStepStatus(6) !== "Por Hacer" &&
						"btn-success-green"}`}
					variant="contained"
					color="primary"
					onClick={() => {
						setStepTwoInfoModal(true);
						setTypeInfoModal("notary");
					}}
				>
					VER DATOS DE LA NOTARÍA
				</Button>
			</React.Fragment>
		),
		createData(
			"Toma del vale vista en el banco",
			"Comprador",
			getStepStatus(7),
			"Debes solicitar y tomar el Vale Vista en tu banco, para llevarlo posteriormente a la notaría",
			<React.Fragment>
				<FormControlLabel
					control={
						<CustomCheckBox
							checked={stepSevenCheck}
							onChange={() => handleCheckBoxChange(7)}
							disabled={6 !== stepNumber}
							name="checkedG"
						/>
					}
					label="Listo"
				/>
			</React.Fragment>
		),
		createData(
			"Firma escritura y entrega de vale vista en la notaria",
			"Comprador",
			getStepStatus(8),
			"En este paso, en la notaría físicamente debes firmar la Escritura de compraventa y la carta de instrucción y entregar el vale vista al Notario el cual se adjuntará a la carta de instrucción para ser liberado cuando se cumpla lo señalado por dicha carta",
			<React.Fragment>
				<FormControlLabel
					control={
						<CustomCheckBox
							checked={stepEightCheck}
							onChange={() => handleCheckBoxChange(8)}
							name="checkedG"
						/>
					}
					disabled={7 !== stepNumber}
					label="Listo"
				/>
			</React.Fragment>
		),
		createData(
			"Inscripción de la propiedad",
			"Vendedor",
			getStepStatus(9),
			"Una vez recibida la escritura con su repertorio respectivo desde la notaría, incribimos la propiedad en el Conservador de Bienes Raíces de Puerto Varas",
			<React.Fragment>
				<Button
					id="step-one-btn"
					className="form-button-step-two mobile-mt-5 mb-10"
					variant="contained"
					color="primary"
					onClick={() => window.open(urlFileSignedDeedNotarized, "_blank")}
					className={`mobile-mt-5 ${getBtnCss(8)} checklist-btn-mob ${(getStepStatus(9) !== "Por Hacer" ||
						urlFileSignedDeedNotarized) &&
						"btn-success-green"}`}
					disabled={!urlFileSignedDeedNotarized}
				>
					DESCARGAR ESCRITURA NOTARIADA Y COMPLEMENTOS
				</Button>
			</React.Fragment>
		),
		createData(
			"Procesando la Inscripción en el CBR",
			"Vendedor",
			getStepStatus(10),
			"El Conservador de bienes raíces se toma el tiempo para estudiar que los títulos de la propiedad estén correctos y se puedan inscribir a tu nombre. Una vez que este proceso haya concluido se emite el certificado de propiedad y de que se está libre de hipotecas, litigios, gravámenes y otros.",
			""
		),
		createData(
			"Entrega de certificados al propietario y al notario",
			"Vendedor",
			getStepStatus(11),
			"Te entregamos, al igual que al notario, los certificados emitidos por el Conservador de Bienes Raíces y se libera el Vale Vista. Terminando de esta forma el proceso de compraventa",
			<React.Fragment>
				<Button
					id="step-one-btn"
					className={`mobile-mt-5 mb-10 ${getBtnCss(10)} checklist-btn-mob ${(getStepStatus(11) !==
						"Por Hacer" ||
						urlFileDomainOwnershipCertificate) &&
						"btn-success-green"}`}
					variant="contained"
					color="primary"
					onClick={() => window.open(urlFileDomainOwnershipCertificate, "_blank")}
					disabled={!urlFileDomainOwnershipCertificate}
				>
					DESCARGAR CERTIFICADO DE DOMINIO A NOMBRE DEL COMPRADOR
				</Button>
				<Button
					id="step-one-btn"
					className={`mobile-mt-5 ${getBtnCss(10)} checklist-btn-mob ${(getStepStatus(11) !== "Por Hacer" ||
						urlFileOthersCertificates) &&
						"btn-success-green"}`}
					variant="contained"
					color="primary"
					onClick={() => window.open(urlFileOthersCertificates, "_blank")}
					disabled={!urlFileOthersCertificates}
				>
					DESCARGAR CERTIFICADOS LIBRE DE HIPOTECAS, LITIOGIOS Y GRAVÁMENES ENTRE OTROS
				</Button>
			</React.Fragment>
		)
	];

	return (
		<React.Fragment>
			<div className="checklist-mobile-cont">
				<div className="vertical-line-step" />
				{rows.map((row, index) => {
					if (index === 10) {
						return null;
					}
					return (
						<div className={`step-cont-mob ${index === 9 && "padding-b20"}`}>
							<div className="first-sec">
								<div className="number-step-mob">{getNumber(index)}</div>
								<div className="text-number-step-mob">{` ${row.step}`}</div>
							</div>
							<div className="second-sec">
								<div className={`circle-step-empty circle-icon-index ${getStatusByRow(index)}`} />
							</div>
							<div className="third-sec">
								<div className="step-mobile-card">
									<div className="step-mobile-card-header">
										<strong>Responsable:</strong> {row.responsable}
									</div>
									<div className="step-mobile-card-status">
										<strong>Estado:</strong> {row.time}
									</div>
									<div className="step-mobile-card-description">{row.description}</div>
									<div className="step-two-btn-cont">{row.download}</div>
								</div>
							</div>
						</div>
					);
				})}
				<AccountInfoModal
					modalOpen={stepTwoInfoModal}
					setModalOpen={(value) => setStepTwoInfoModal(value)}
					type={typeInfoModal}
					setType={setTypeInfoModal}
				/>
			</div>
			<div className="step-cont-mob">
				<div className="first-sec">
					<div className="number-step-mob">{getNumber(10)}</div>
					<div className="text-number-step-mob">{` ${rows[10].step}`}</div>
				</div>
				<div className="second-sec">
					<div className={`circle-step-empty circle-icon-index ${getStatusByRow(10)}`} />
				</div>
				<div className="third-sec">
					<div className="step-mobile-card">
						<div className="step-mobile-card-header">
							<strong>Responsable:</strong> {rows[10].responsable}
						</div>
						<div className="step-mobile-card-status">
							<strong>Estado:</strong> {rows[10].time}
						</div>
						<div className="step-mobile-card-description">{rows[10].description}</div>
						<div className="step-two-btn-cont">{rows[10].download}</div>
					</div>
				</div>
			</div>
			{stepNumber > 10 && (
				<div className="d-flex column now-enjoy-cont">
					<p className="now-enjoy">¡AHORA A DISFRUTAR!</p>
					<p className="now-enjoy-sub-p">
						de tu mejor inversión, recuerda que puedes contar con nosotros en el proceso de diseño y
						construcción de tu hogar.
					</p>
				</div>
			)}
		</React.Fragment>
	);
}
