import React from "react";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import ContactCard from "../card/contact";
import WhatsappCard from "../card/whatsapp";
import NewsLetter from "../card/newsLetter";
const useStyles = makeStyles({
	root: {
		width: "100%",
		maxWidth: "75rem",
		display: "flex",
		flexDirection: "column",
		margin: "0px auto",
		paddingBottom: "3rem",
		paddingTop: "3rem",
		paddingLeft: "1rem",
		paddingRight: "1rem"
	},
	paper: {
		textAlign: "center"
	},
	"@media (max-width: 1070px)": {
		root: {
			paddingTop: "1rem",
			paddingBottom: "1rem"
		}
	}
});

export default function SimpleCont() {
	const classes = useStyles();

	return (
		<div id="contacto" className={classes.root}>
			<div className="d-flex ai-center jc-center column">
				<div className="d-flex jc-center ai-center column padding-b20">
					<h1 className="simple-title green ">
						<strong>- Contacto -</strong>
					</h1>
					<h2 className="contact-sub-title">Hablemos por tu medio favorito.</h2>
				</div>
				<Grid container spacing={3}>
					<Grid item xs={6} sm={6}>
						<ContactCard />
					</Grid>
					<Grid item xs={6} sm={6}>
						<WhatsappCard />
					</Grid>
					<Grid item xs={12}>
						<NewsLetter />
					</Grid>
				</Grid>
			</div>
		</div>
	);
}
