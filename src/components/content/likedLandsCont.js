import React, { useContext } from "react";
import dynamic from "next/dynamic";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Favorite from "@material-ui/icons/Favorite";
import { makeStyles } from "@material-ui/core/styles";

import { UserContext } from "../../context/user.context";
import { ModalDispatchContext } from "../../context/modal.context";
import LandInfo from "../../components/modal/landInfo";

const useStyles = makeStyles({
	root: {
		width: "100%",
		maxWidth: "75rem",
		display: "flex",
		flexDirection: "column",
		margin: "0px auto",
		backgroundColor: "#FAFAFA",
		paddingBottom: "3rem",
		paddingTop: "3rem",
		paddingLeft: "1rem",
		paddingRight: "1rem"
	},
	favorite: {
		marginRight: "5px"
	},
	greenFavorite: {
		marginRight: "5px",
		color: "#94b200"
	},
	paper: {
		textAlign: "center",
		backgroundColor: "#E3E3E3",
		width: "100%",
		position: "relative",
		textAlign: "start",
		maxWidth: "500px",
		fontSize: "14px",
		color: "#484848",
		cursor: "pointer",
		padding: "10px",
		boxShadow: "none"
	},
	landContainer: {
		maxWidth: "800px",
		width: "100%"
	},
	paddingSides: {
		padding: "10px"
	},
	button: {
		background: "#707070",
		borderRadius: "16px",
		color: "#FFFFFF"
	},
	"@media (max-width: 1070px)": {
		paddingSides: {
			padding: "10px 0px"
		},
		button: {
			fontSize: "12px"
		},
		greenFavorite: {
			width: "1.3em",
			height: "1.3em"
		}
	}
});

export default function LikedLandsCont({ handleBack }) {
	const classes = useStyles();
	const _user = useContext(UserContext);
	const modalDispatch = useContext(ModalDispatchContext);
	const { likedLands = [] } = _user;
	return (
		<Grid item xs={12} className={classes.landContainer}>
			<Grid container xs={12} md={12}>
				{likedLands.map((landNumber) => {
					return (
						<Grid container xs={12} md={4} className={classes.paddingSides}>
							<Paper className={classes.paper}>
								<div className="d-flex jc-space-b ai-center">
									<Favorite className={classes.greenFavorite} />
									<span className="land-number-profile">{landNumber}</span>
									<Button
										variant="contained"
										className={classes.button}
										onClick={() => {
											modalDispatch({
												type: "UPDATE",
												payload: { open: true, landNumber: landNumber }
											});
										}}
									>
										VER FICHA
									</Button>
								</div>
							</Paper>
						</Grid>
					);
				})}
			</Grid>
			<LandInfo />
		</Grid>
	);
}
