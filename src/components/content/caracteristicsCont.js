import React, { useEffect, useContext, useState } from "react";
import _ from "lodash";
import { makeStyles } from "@material-ui/core/styles";
import { LazyLoadImage } from "react-lazy-load-image-component";
import Grid from "@material-ui/core/grid";

import Gallery from "../gallery";
import { MultimediaContext } from "../../context/multimedia.context";
import { sanitizeMultimediaData } from "../../utils/helper";

const useStyles = makeStyles({
	root: {
		width: "100%",
		maxWidth: "75rem",
		display: "flex",
		flexDirection: "column",
		margin: "0px auto",
		paddingBottom: "3rem",
		paddingTop: "3rem",
		paddingLeft: "1rem",
		paddingRight: "1rem"
	},
	"@media (max-width: 1070px)": {
		root: {
			paddingTop: "1rem",
			paddingBottom: "1.5rem"
		}
	}
});

export default function CaracteristicsCont() {
	const classes = useStyles();
	const multimedia = useContext(MultimediaContext);
	const { others = [] } = multimedia;

	const getBanner = (type) => {
		switch (type) {
			case "DESKTOP":
				const bigBanner =
					_.find(others, function(o) {
						return o.title === "Big Banner";
					}) || {};
				const { urlMultimedia = "" } = bigBanner;
				return urlMultimedia;
			case "MOBILE":
				const mobileBanner =
					_.find(others, function(o) {
						return o.title === "Big Banner Mob";
					}) || {};
				const { urlMultimedia: mobUrlMultimedia = "" } = mobileBanner;
				return mobUrlMultimedia;
			default:
				return "";
		}
	};

	return (
		<div id="caracteristicas" className={classes.root}>
			<div>
				<h1 className="simple-title green padding-b20">
					<strong>- Características -</strong>
				</h1>
				<p className="simple-p semi-black padding-b20 desktop-only">
					Praderas de Frutillar cuenta con un ambicioso plan de áreas comunes tales como casa club,
					encantadora plaza, tinas calientes entre árboles y flores y una lagunita. Las parcelas en venta
					tienen 5.000 metros cuadrados (media hectárea), con un entorno maravilloso y algunas con vistas a
					los volcanes Osorno, Puntiagudo, Tronador y Calbuco. Cercano a diversos lugares de interés
					turístico, naturales y ecológicos.
				</p>
				<p className="b-simple-p sub-black padding-b20 desktop-only">
					<strong>
						Para quienes quieren disfrutar y vivir en la naturaleza sin perder la cercana conexión con Chile
						y el mundo
					</strong>
				</p>
				<Gallery multimedia={multimedia.caracteristics} />
				<div className="big-banner">
					<LazyLoadImage src={getBanner("DESKTOP")} alt="conectividad" className="desktop-only" />
					<LazyLoadImage src={getBanner("MOBILE")} alt="conectividad" className="mobile-only" />
				</div>
			</div>
		</div>
	);
}
