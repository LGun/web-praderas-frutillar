import React, { useState, useContext } from "react";
import dynamic from "next/dynamic";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import LandInfo from "../modal/landInfo";
import LandTable from "../table/landTable";
import SearchAreaBtn from "../button/searchAreaBtn";
import { STEPPS } from "../../utils/constants";
import { StepContext, DispatchContext } from "../../context/step.context";
const Map = dynamic(() => import("../map"));

const useStyles = makeStyles({
	root: {
		width: "100%",
		maxWidth: "75rem",
		display: "flex",
		flexDirection: "column",
		margin: "0px auto",
		paddingBottom: "3rem",
		paddingTop: "3rem",
		paddingLeft: "1rem",
		paddingRight: "1rem"
	},
	"@media (max-width: 1070px)": {
		root: {
			paddingTop: "1rem",
			paddingBottom: "1rem"
		}
	}
});

export default function SelectAreaCont() {
	const step = useContext(StepContext) || {};
	const dispatchStep = useContext(DispatchContext);
	const classes = useStyles();
	const [ selectedButton, setSelectedButton ] = useState("");
	const getSelected = (buttonName) => {
		const { name = "" } = step;
		if (name === buttonName) {
			return "active";
		}
		return "";
	};
	const eventClickBtn = (buttonName) => {
		let newFilerName = buttonName;
		const { name = "" } = step;
		if (name === buttonName) {
			newFilerName = "";
		}
		setSelectedButton(newFilerName);
		dispatchStep({
			type: "UPDATE",
			payload: {
				name: newFilerName,
				filtered: ""
			}
		});
	};
	return (
		<div id="encuentra_tu_parcela" className={classes.root}>
			<div>
				<h1 id="encuentra_tu_parcela_title" className="simple-title green padding-b20">
					<strong>- Encuentra Tu Parcela -</strong>
				</h1>
				<div className="d-flex jc-center ai-center padding-b20">
					<SearchAreaBtn />
				</div>
				<p className="area-selector-sub-title">
					<img className="filter-icon mr-1" src="/filtro.svg" /> En el mapa haz click al número de la parcela
					y verás su precio y características. Marca el <FavoriteBorderIcon /> y podrás compararla en la tabla
					de abajo.
				</p>
				<div className="mobile-padding d-flex jc-space-b ai-center padding-b20 row full-width m-0">
					<Button
						onClick={() => eventClickBtn(STEPPS.FIRST_STEPP)}
						className={`select-area-btn ${getSelected(STEPPS.FIRST_STEPP)}`}
						variant="contained"
						color="primary"
						disableElevation
					>
						<img className="mr-1" src="/Rectangulo 1825@2x.png" />
						<div className="mr-1" />
						<div className="d-flex jc-start ai-start column">
							<div>PRIMERA</div>
							<div className="mt-n-10">ETAPA</div>
						</div>
					</Button>
					<Button
						onClick={() => eventClickBtn(STEPPS.SECOND_STEPP)}
						className={`select-area-btn ${getSelected(STEPPS.SECOND_STEPP)}`}
						variant="contained"
						color="primary"
						disableElevation
					>
						<img className="mr-1" src="/Rectangulo 1826@2x.png" />
						<div className="d-flex jc-start ai-start column">
							<div>SEGUNDA</div>
							<div className="mt-n-10">ETAPA</div>
						</div>
					</Button>
					<Button
						onClick={() => eventClickBtn(STEPPS.CORNER)}
						className={`select-area-btn ${getSelected(STEPPS.CORNER)}`}
						variant="contained"
						color="primary"
						disableElevation
					>
						<img className="mr-1" src="/Rectangulo 1827@2x.png" />
						<div className="d-flex jc-start ai-start column">
							<div>PARCELA</div>
							<div className="mt-n-10">ESQUINA</div>
						</div>
					</Button>
					<Button
						onClick={() => eventClickBtn(STEPPS.VOLCANO)}
						className={`select-area-btn ${getSelected(STEPPS.VOLCANO)}`}
						variant="contained"
						color="primary"
						disableElevation
					>
						<img className="mr-1" src="/volcan.svg" />
						<div className="d-flex jc-start ai-start column">
							<div>VISTA AL</div>
							<div className="mt-n-10">VOLCÁN</div>
						</div>
					</Button>
					<Button
						onClick={() => eventClickBtn(STEPPS.TREES)}
						className={`select-area-btn ${getSelected(STEPPS.TREES)}`}
						variant="contained"
						color="primary"
						disableElevation
					>
						<img className="mr-1" src="/Rectangulo 1779@2x.png" />
						ÁRBOLES
					</Button>
					<Button
						onClick={() => eventClickBtn(STEPPS.BEST_PRICES)}
						className={`select-area-btn ${getSelected(STEPPS.BEST_PRICES)}`}
						variant="contained"
						color="primary"
						disableElevation
					>
						<img className="mr-1" src="/Rectangulo 1828@2x.png" />
						<div className="d-flex jc-start ai-start column">
							<div>MEJORES</div>
							<div className="mt-n-10">PRECIOS</div>
						</div>
					</Button>
				</div>
				<div className="padding-b20">
					<Map />
				</div>
				<div className="mob-cp-p d-flex jc-center ai-center row full-width m-0 bg-land-row padding-20">
					<strong>
						COMPARA TUS PARCELAS <FavoriteBorderIcon className="green-color" />
					</strong>
				</div>
				<div className="full-width padding-b30">
					<LandTable />
				</div>
			</div>
			<LandInfo />
		</div>
	);
}
