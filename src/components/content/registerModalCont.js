import React, { useState, useContext } from "react";
import { useRouter } from "next/router";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";

import firebase from "../../firebase";
import { auth } from "../../firebase";
import { createUser } from "../../services";
import { UserDispatchContext } from "../../context/user.context";
import { ProfileModalContext } from "../../context/profileModal.context";
import { getErrorMessageByCodeRegister, ValidateEmail } from "../../utils/helper";

const useStyles = makeStyles((theme) => ({
	textField: {
		marginBottom: "10px",
		backgroundColor: "#fff",
		backgroundColor: "#F5F5F5",
		border: "3px solid #D9D9D9",
		borderRadius: "8px",
		"& label.Mui-focused": {
			color: "#ee7f0e"
		},
		"& .MuiInput-underline:after": {
			borderBottomColor: "green"
		},
		"& .MuiOutlinedInput-root": {
			"&.Mui-focused fieldset": {
				borderColor: "#ee7f0e"
			}
		}
	},
	buttonProgress: {
		color: "#FFF"
	}
}));

export default function RegisterModalCont({ closeModal }) {
	const classes = useStyles();
	const router = useRouter();
	const [ email, setEmail ] = useState("");
	const [ emailError, setEmailError ] = useState(false);
	const [ password, setPassword ] = useState("");
	const [ passwordError, setPasswordError ] = useState(false);
	const [ name, setName ] = useState("");
	const [ nameError, setNameError ] = useState(false);
	const [ phoneNumber, setPhoneNumber ] = useState("");
	const [ phoneNumberError, setPhoneNumberError ] = useState(false);
	const [ errorMessage, setErrorMessage ] = useState();
	const [ validFields, setValidFields ] = useState(false);
	const [ loading, setLoading ] = useState(false);
	const userDispatch = useContext(UserDispatchContext);
	const modalOptions = useContext(ProfileModalContext);

	const validateEmail = (stringValue) => {
		setEmail(stringValue);
		const valid = ValidateEmail(stringValue);
		setEmailError(!valid);
		return !valid;
	};

	const validatePassword = (stringValue) => {
		setPassword(stringValue);
		const valid = stringValue.length >= 6;
		setPasswordError(!valid);
		return !valid;
	};

	const validateName = (stringValue) => {
		setName(stringValue);
		const valid = stringValue.length > 0;
		setNameError(!valid);
		return !valid;
	};

	const validatePhone = (stringValue) => {
		setPhoneNumber(stringValue);
		const valid = stringValue.length > 0;
		setPhoneNumberError(!valid);
		return !valid;
	};

	const createUserWithEmailAndPasswordHandler = async () => {
		setLoading(true);

		const _emailValid = validateEmail(email);
		const _passwordValid = validatePassword(password);
		const _phoneValid = validatePhone(phoneNumber);
		const _nameValid = validateName(name);
		if (!_emailValid && !_passwordValid && !_phoneValid && !_nameValid) {
			try {
				const { user } = await auth.createUserWithEmailAndPassword(email, password);
				console.log("auth user", user);
				const payload = {
					firstName: name,
					phone: phoneNumber,
					email: user.email,
					customerId: user.uid,
					timeCreation: firebase.firestore.FieldValue.serverTimestamp()
				};
				const response = await createUser(payload);
				userDispatch({
					type: "UPDATE",
					payload: {
						profile: payload
					}
				});
				localStorage.setItem("customerId", payload.customerId);
				if (typeof modalOptions.redirectTo !== "undefined" && modalOptions.redirectTo === "checklist") {
					router.push("/checklist");
				}
				setErrorMessage("");
				closeModal();
			} catch (error) {
				let errorMessage = "";
				if (error && typeof error.code !== "undefined") {
					errorMessage = getErrorMessageByCodeRegister(error.code);
				}
				setErrorMessage(errorMessage);
				console.log("Error Signing up with email and password", error);
			}
		}

		setLoading(false);
	};

	return (
		<React.Fragment>
			<AccountCircleIcon className="login-profile-icon padding-b10" />
			<TextField
				id="name-outlined-basic"
				name="name"
				className={classes.textField}
				label="Nombre"
				variant="outlined"
				fullWidth
				value={name}
				error={nameError}
				onChange={(e) => validateName(e.target.value)}
			/>
			<TextField
				id="email-outlined-basic"
				name="email"
				className={classes.textField}
				label="Email"
				variant="outlined"
				fullWidth
				value={email}
				onChange={(e) => validateEmail(e.target.value)}
				error={emailError}
			/>
			<p className={`email-error-text sign-process ${emailError && "active"}`}>Email invalido</p>
			<TextField
				id="phone-outlined-basic"
				name="phoneNumber"
				className={classes.textField}
				label="Teléfono"
				variant="outlined"
				fullWidth
				value={phoneNumber}
				onChange={(e) => validatePhone(e.target.value)}
				error={phoneNumberError}
				inputProps={{ maxLength: 12 }}
			/>
			<TextField
				id="password-outlined-basic"
				name="password"
				type="password"
				className={classes.textField}
				label="Password"
				variant="outlined"
				fullWidth
				value={password}
				error={passwordError}
				onChange={(e) => validatePassword(e.target.value)}
			/>
			<p className={`email-error-text sign-process ${passwordError && "active"}`}>
				Contraseña debe tener al menos 6 caracteres
			</p>
			<Button
				id="register-button"
				type="submit"
				value="Send"
				className="login-button"
				variant="contained"
				color="primary"
				onClick={createUserWithEmailAndPasswordHandler}
				disabled={validFields}
			>
				{loading ? <CircularProgress size={26} className={classes.buttonProgress} /> : "REGISTRARME"}
			</Button>
			<p className={`form-error-text ${errorMessage && "active"}`}>{errorMessage}</p>
		</React.Fragment>
	);
}
