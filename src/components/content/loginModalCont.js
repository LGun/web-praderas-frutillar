import React, { useState, useEffect, useContext } from "react";
import { useRouter } from "next/router";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import CircularProgress from "@material-ui/core/CircularProgress";

import { auth } from "../../firebase";
import { getUserByUid } from "../../services";
import { getErrorMessageByCodeLogin } from "../../utils/helper";
import { UserDispatchContext } from "../../context/user.context";
import { ProfileModalContext } from "../../context/profileModal.context";

const useStyles = makeStyles((theme) => ({
	textField: {
		marginBottom: "10px",
		backgroundColor: "#fff",
		backgroundColor: "#F5F5F5",
		border: "3px solid #D9D9D9",
		borderRadius: "8px",
		"& label.Mui-focused": {
			color: "#ee7f0e"
		},
		"& .MuiInput-underline:after": {
			borderBottomColor: "green"
		},
		"& .MuiOutlinedInput-root": {
			"&.Mui-focused fieldset": {
				borderColor: "#ee7f0e"
			}
		}
	},
	backdrop: {
		zIndex: theme.zIndex.drawer + 1,
		color: "#fff"
	},
	buttonProgress: {
		color: "#FFF"
	}
}));

export default function LoginModalCont({ closeModal, setModalCont }) {
	const classes = useStyles();
	const router = useRouter();
	const [ email, setEmail ] = useState();
	const [ password, setPassword ] = useState();
	const [ errorMessage, setErrorMessage ] = useState();
	const [ validFields, setValidFields ] = useState(false);
	const [ loading, setLoading ] = useState(false);
	const userDispatch = useContext(UserDispatchContext);
	const modalOptions = useContext(ProfileModalContext);

	const signInHandler = async () => {
		setLoading(true);
		const result = await auth
			.signInWithEmailAndPassword(email, password)
			.then((response) => {
				setErrorMessage("");
				return response;
			})
			.catch((error) => {
				let errorMessage = "";
				if (error && typeof error.code !== "undefined") {
					errorMessage = getErrorMessageByCodeLogin(error.code);
				}
				setErrorMessage(errorMessage);
				console.error("Error signing in with password and email", error);
			});

		if (typeof result !== "undefined" && result) {
			const { user = "" } = result;
			const { uid = "" } = user;
			const data = await getUserByUid(uid);
			userDispatch({
				type: "UPDATE",
				payload: {
					profile: data
				}
			});
			localStorage.setItem("customerId", uid);
			if (typeof modalOptions.redirectTo !== "undefined" && modalOptions.redirectTo === "checklist") {
				router.push("/checklist");
			}
			closeModal();
		}
		setLoading(false);
	};

	const validateFields = () => {
		if (email && email.length > 0 && password && password.length > 4) {
			setValidFields(true);
		} else {
			setValidFields(false);
		}
	};

	useEffect(
		() => {
			validateFields();
		},
		[ email, password ]
	);

	return (
		<React.Fragment>
			<AccountCircleIcon className="login-profile-icon padding-b10" />
			<TextField
				id="name-outlined-basic"
				name="email"
				className={classes.textField}
				label="Email"
				variant="outlined"
				fullWidth
				value={email}
				onChange={(e) => setEmail(e.target.value)}
			/>
			<TextField
				id="name-outlined-basic"
				name="password"
				className={classes.textField}
				label="Password"
				variant="outlined"
				fullWidth
				value={password}
				type="password"
				onChange={(e) => setPassword(e.target.value)}
			/>
			<Button
				id="contact-button"
				type="submit"
				value="Send"
				className="login-button"
				variant="contained"
				color="primary"
				disabled={!validFields}
				onClick={() => signInHandler()}
			>
				{loading ? <CircularProgress size={26} className={classes.buttonProgress} /> : "LOGIN"}
			</Button>
			<p className={`form-error-text ${errorMessage && "active"}`}>{errorMessage}</p>
			<div className="lost-pass-text-cont" onClick={() => setModalCont("changePass")}>
				<div className="text-reg">¿Has olvidado la contraseña?</div>
			</div>
		</React.Fragment>
	);
}
