import React, { useContext, useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { LazyLoadImage } from "react-lazy-load-image-component";
import _ from "lodash";
import dynamic from "next/dynamic";
import Typography from "@material-ui/core/Typography";
import SalesSlider from "../slider/SalesSlider";
import { MultimediaContext } from "../../context/multimedia.context";
import { orderMultimediaByTitle } from "../../utils/helper";
import { ModalDispatchContext } from "../../context/modal.context";

const Carousel = dynamic(() => import("../card/carousel"));
const useStyles = makeStyles({
	root: {
		width: "100%",
		maxWidth: "75rem",
		display: "flex",
		flexDirection: "column",
		margin: "0px auto",
		// backgroundColor: "#FAFAFA",
		paddingBottom: "3rem",
		paddingTop: "3rem",
		paddingLeft: "1rem",
		paddingRight: "1rem"
	},
	"@media (max-width: 1070px)": {
		root: {
			paddingTop: "1rem",
			paddingBottom: "1rem"
		}
	}
});

export default function SimpleCont() {
	const modalDispatch = useContext(ModalDispatchContext);
	const handleClickImage = (_landNumber) => {
		if (!isNaN(Number(_landNumber))) {
			modalDispatch({
				type: "UPDATE",
				payload: { open: true, landNumber: _landNumber }
			});
		}
	};
	const classes = useStyles();
	const [ multimediaSales, setMultimediaSales ] = useState([]);
	const multimedia = useContext(MultimediaContext);
	useEffect(
		() => {
			// setMultimediaSales(orderMultimediaByTitle(multimedia.sales));
			setMultimediaSales(multimedia.sales);
			console.log("multimedia.sales", multimedia.sales);
		},
		[ multimedia ]
	);
	return (
		<div id="sales" className={classes.root}>
			<div>
				<h1 className="simple-title green padding-b20">
					<strong>- Pago Preferente -</strong>
				</h1>
				<p className="simple-p semi-black padding-b20 desktop-only">
					Conoce nuestras parcelas con excelente descuento y aprovéchalos mientras están vigentes. Invierte en
					una de las mejores parcelas del sur de Chile. Escríbenos por whatsapp, email o llámanos y con gusto
					conversamos.
				</p>
				{/* <div className="padding-b30 desktop-only">
					<SalesSlider
						multimedia={_.filter(multimediaSales, function(item) {
							return !item.isMobile;
						})}
					/>
				</div> */}
				<div className="img-cont-sales">
					<div className="lazyload-wrapper">
						<picture>
							<source
								data-testid="source-image"
								srcSet="https://firebasestorage.googleapis.com/v0/b/original-mesh-305921.appspot.com/o/galery%2Fothers%2Fbanner-beneficio-desktop-100.jpg?alt=media&token=b333017b-66be-4df5-af48-5aa0097abb6d"
								media="(min-width: 720px)"
							/>
							<LazyLoadImage
								data-testid="one-clickable-container"
								alt="Parcelas"
								title="Parcelas Frutillar"
								className="BackgroundImageOnly-module_background-image__19Xw0 d-none d-md-flex"
								placeholder={
									<img
										className="BackgroundImageOnly-module_background-image__19Xw0 d-none d-md-flex"
										src="/slider-random-desktop-100-min.jpg"
									/>
								}
							/>
						</picture>
					</div>
					<div className="lazyload-wrapper">
						<picture>
							<source
								data-testid="source-image"
								srcSet="https://firebasestorage.googleapis.com/v0/b/original-mesh-305921.appspot.com/o/galery%2Fothers%2Fbanner-beneficio-mobile-100.jpg?alt=media&token=564fbd83-79aa-4cca-ab82-0157343129f6"
								media="(max-width: 720px)"
							/>
							<LazyLoadImage
								data-testid="mobile-clickable-container"
								alt="Parcelas"
								title="Parcelas Frutillar"
								className="BackgroundImageOnly-module_background-image__19Xw0 d-md-none"
								placeholder={
									<img
										className="BackgroundImageOnly-module_background-image__19Xw0 d-md-none"
										src="/slider-random-mobile-100-min.jpg"
									/>
								}
							/>
						</picture>
					</div>
					{/* <Carousel
						slidesToShow={1.1}
						data={_.filter(multimediaSales, function(item) {
							return !item.isMobile;
						})}
						openModal={(payload) => handleClickImage(payload.landNumber)}
					/> */}
				</div>
			</div>
		</div>
	);
}
