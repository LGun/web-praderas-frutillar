import React, { useState, useEffect, useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import LockIcon from "@material-ui/icons/Lock";
import CircularProgress from "@material-ui/core/CircularProgress";

import { auth } from "../../firebase";
import { ValidateEmail } from "../../utils/helper";

const useStyles = makeStyles((theme) => ({
	textField: {
		marginBottom: "10px",
		backgroundColor: "#fff",
		backgroundColor: "#F5F5F5",
		border: "3px solid #D9D9D9",
		borderRadius: "8px",
		"& label.Mui-focused": {
			color: "#ee7f0e"
		},
		"& .MuiInput-underline:after": {
			borderBottomColor: "green"
		},
		"& .MuiOutlinedInput-root": {
			"&.Mui-focused fieldset": {
				borderColor: "#ee7f0e"
			}
		}
	},
	backdrop: {
		zIndex: theme.zIndex.drawer + 1,
		color: "#fff"
	},
	buttonProgress: {
		color: "#FFF"
	}
}));

export default function ChangePassModalCont({ closeModal, setModalCont }) {
	const classes = useStyles();
	const [ email, setEmail ] = useState();
	const [ emailError, setEmailError ] = useState(false);
	const [ errorMessage, setErrorMessage ] = useState();
	const [ loading, setLoading ] = useState(false);
	const [ emailHasBeenSent, setEmailHasBeenSent ] = useState(false);

	const changePassHandler = async () => {
		setLoading(true);
		const verifyEmail = validateEmail(email);
		if (!verifyEmail) {
			const result = await auth
				.sendPasswordResetEmail(email)
				.then((response) => {
					setEmailHasBeenSent(true);
					// setTimeout(() => {
					// 	setEmailHasBeenSent(false);
					// }, 3000);
					console.log("response", response);
				})
				.catch((error) => {
					console.log("error: ", error);
					setErrorMessage("Error al restablecer la contraseña");
				});
		}
		setLoading(false);
	};

	const validateEmail = (stringValue) => {
		setEmail(stringValue);
		const valid = ValidateEmail(stringValue);
		setEmailError(!valid);
		return !valid;
	};

	const getBtnText = () => {
		return emailHasBeenSent ? "Reenviar enlace de acceso" : "Enviar enlace de acceso";
	};

	return (
		<React.Fragment>
			<LockIcon className="login-profile-icon padding-b10" />
			<p className="change-pass-sub-text">
				Introduce tu correo electrónico y te enviaremos un enlace para que vuelvas a entrar en tu cuenta.
			</p>
			<TextField
				id="name-outlined-basic"
				name="email"
				className={classes.textField}
				label="Email"
				variant="outlined"
				fullWidth
				value={email}
				error={emailError}
				onChange={(e) => validateEmail(e.target.value)}
			/>
			<p className={`email-error-text sign-process ${emailError && "active"}`}>Email invalido</p>
			<Button
				id="contact-button"
				type="submit"
				value="Send"
				className="login-button"
				variant="contained"
				color="primary"
				disabled={emailError}
				onClick={() => changePassHandler()}
			>
				{loading ? <CircularProgress size={26} className={classes.buttonProgress} /> : getBtnText()}
			</Button>
			<p className={`form-error-text ${errorMessage && "active"}`}>{errorMessage}</p>
		</React.Fragment>
	);
}
