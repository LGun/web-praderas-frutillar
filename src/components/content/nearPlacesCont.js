import React, { useEffect, useState, useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import ImageGallert from "../imageGallery";
import Gallery from "../gallery";
import { MultimediaContext } from "../../context/multimedia.context";
import { sanitizeMultimediaData } from "../../utils/helper";

const useStyles = makeStyles({
	root: {
		width: "100%",
		maxWidth: "75rem",
		display: "flex",
		flexDirection: "column",
		margin: "0px auto",
		paddingBottom: "3rem",
		paddingTop: "3rem",
		paddingLeft: "1rem",
		paddingRight: "1rem"
	},
	"@media (max-width: 1070px)": {
		root: {
			paddingTop: "1rem",
			paddingBottom: "1.5rem"
		}
	}
});

export default function NearPlacesCont() {
	const classes = useStyles();
	const multimedia = useContext(MultimediaContext);

	return (
		<div id="lugares_cercanos" className={classes.root}>
			<div>
				<h1 className="simple-title green padding-b20">
					<strong>- Lugares Cercanos -</strong>
				</h1>
				<p className="simple-p semi-black padding-b20 desktop-only">
					Ciudades y Puntos de interés, Embarcaderos, Deportes, Servicios
				</p>

				<Gallery multimedia={multimedia.nearPlaces} />
			</div>
		</div>
	);
}
