import React, { useEffect, useContext, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import ImageGallert from "../imageGallery";
import Gallery from "../gallery";
import { MultimediaContext } from "../../context/multimedia.context";

const useStyles = makeStyles({
	root: {
		width: "100%",
		maxWidth: "75rem",
		display: "flex",
		flexDirection: "column",
		margin: "0px auto",
		paddingBottom: "3rem",
		paddingTop: "3rem",
		paddingLeft: "1rem",
		paddingRight: "1rem"
	},
	"@media (max-width: 1070px)": {
		root: {
			paddingTop: "1rem",
			paddingBottom: "1.5rem"
		}
	}
});

export default function GalleryCont() {
	const classes = useStyles();
	const multimedia = useContext(MultimediaContext);

	return (
		<div id="nuestra_ubicacion" className={classes.root}>
			<div>
				<h1 className="simple-title green padding-b20">
					<strong>- Nuestra Ubicación -</strong>
				</h1>
				<p className="simple-p semi-black padding-b20 desktop-only">
					Fundo Praderas de Frutillar, es una parcelación a 5 minutos del centro de Frutillar, X Región de los
					Lagos. Estos terrenos en venta están en los más hermosos parajes de Chile, a 12 minutos del Lago
					Llanquihue.
				</p>
				<p className="b-simple-p sub-black padding-b20 desktop-only">
					<strong>
						Disfruta de la tranquila vida, en entornos campestres, bella naturaleza, con la mejor
						conectividad digital y física de la zona.
					</strong>
				</p>
				{/* <div className="mb-big-baner" /> */}
				{/* <ImageGallert /> */}
				<Gallery slidesToShow={1.1} multimedia={multimedia.ourUbication} />
			</div>
		</div>
	);
}
