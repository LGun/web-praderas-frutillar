import React from "react";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
	root: {
		width: "100%",
		maxWidth: "75rem",
		display: "flex",
		flexDirection: "column",
		margin: "0px auto",
		backgroundColor: "#FAFAFA",
		paddingBottom: "3rem",
		paddingTop: "3rem",
		paddingLeft: "1rem",
		paddingRight: "1rem"
	}
});

export default function SimpleCont() {
	const classes = useStyles();

	return (
		<div className={classes.root}>
			<div>
				{/* <h1 className="simple-title green padding-b20">
					<strong>- Fundo Praderas De Frutillar -</strong>
				</h1>
				<p className="simple-p semi-black padding-b20">
					Parcelación a 5 min. del centro de Frutillar, X Región de los Lagos, ubicado en los más hermosos
					parajes de Chile, a 12 minutos del Lago Llanquihue. Contamos con electrificación subterránea y agua
					potable funcionando, amplios caminos .Ven a disfrutar y vivir en la tranquilidad de la naturaleza,
					sin perder la cercana conexión con Chile y el Mundo.
				</p>
				<p className="b-simple-p sub-black">
					<strong>¡VEN Y CONSTRUYE TU HOGAR AHORA!</strong>
				</p> */}
				<h1 className="simple-title green padding-b20">
					<strong>- Fundo Praderas De Frutillar -</strong>
				</h1>
				<Grid container spacing={3}>
					<Grid item xs={6}>
						<p className="simple-p semi-black padding-b20 justified">
							Parcelación a 5 min. del centro de Frutillar, X Región de los Lagos, ubicado en los más
							hermosos parajes de Chile, a 12 minutos del Lago Llanquihue. Contamos con electrificación
							subterránea, fibra óptica y APR en proceso, con distribución subterránea, todo funcionando,
							amplios caminos. Puedes construir tu hogar en Fundo Praderas de Frutillar ahora, rodeado de
							lindos campos donde se respira el aroma de la paz. Es una excelente inversión por su gran
							plusvalía, estas hermosas tierras aumentan su precio cada día. Roles individuales y estudio
							de títulos. Ven a disfrutar y vivir en la tranquilidad de la naturaleza, sin perder la
							cercana conexión con Chile y el Mundo.
						</p>
					</Grid>
					<Grid item xs={6}>
						<p className="simple-p semi-black padding-b20 justified">
							Breathtaking parcels only 5 minutes from downtown Frutillar, Chile. It is located in one of
							the most unique and beautiful places in the south of Chile, 12 minutes from Lake Llanquihue.
							It includes newly installed underground power lines, access treated water, and easy access
							via ample roads. This vacant land is fit and ready to build your dream forever home or that
							vacation spot you and your family have been wanting. Here you will feel close to nature
							while having access to modern commodities, including high-speed internet. Come enjoy life at
							its fullest.
						</p>
					</Grid>
					{/* <Grid item xs={6}>
						<p className="b-simple-p sub-black">
							<strong>¡VEN Y CONSTRUYE TU HOGAR AHORA!</strong>
						</p>
					</Grid>
					<Grid item xs={6}>
						<p className="b-simple-p sub-black">
							<strong>¡COME AND BUILD YOUR HOME NOW!!</strong>
						</p>
					</Grid> */}
				</Grid>
			</div>
		</div>
	);
}
