import React from "react";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import LandTable from "../table/landTable";
import Checkbox from "@material-ui/core/Checkbox";
import SearchAreaBtn from "../button/searchAreaBtn";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
const useStyles = makeStyles({
	root: {
		width: "100%",
		maxWidth: "75rem",
		display: "flex",
		flexDirection: "column",
		margin: "0px auto",
		backgroundColor: "#efefef",
		paddingBottom: "3rem",
		paddingTop: "3rem",
		paddingLeft: "1rem",
		paddingRight: "1rem"
	}
});

export default function SelectAreaCont() {
	const [ checked, setChecked ] = React.useState(true);
	const handleChange = (event) => {
		setChecked(!checked);
	};
	const classes = useStyles();

	return (
		<div className={classes.root}>
			<div>
				<h1 className="simple-title green padding-b20">
					<strong>- Cálculo de cuotas bancarias -</strong>
				</h1>
				<div className="d-flex jc-center ai-center padding-b20">
					<span className="checkbox-sub-title">Seleccionar</span>
					<div className="d-flex jc-center ai-center checkbox-title">
						<Checkbox
							checked={checked}
							onChange={handleChange}
							inputProps={{ "aria-label": "primary checkbox" }}
							className="selected"
						/>
						UF
					</div>

					<div className="d-flex jc-center ai-center checkbox-title">
						<Checkbox
							checked={!checked}
							onChange={handleChange}
							inputProps={{ "aria-label": "primary checkbox" }}
							className="selected"
						/>
						PESO CHILENO
					</div>
				</div>
				{/* <p className="area-selector-sub-title">
					(falta icono) Filtra según nuestras características por parcela
				</p> */}
				<div className="d-flex jc-space-b ai-center padding-b20 row full-width m-0" />
				<div className="full-width padding-b30" />
			</div>
		</div>
	);
}
