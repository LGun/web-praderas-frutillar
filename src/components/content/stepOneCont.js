import React, { useState, useEffect, useContext } from "react";
import filter from "lodash/filter";
import { validarRUT } from "validar-rut";
import SwipeableViews from "react-swipeable-views";
import firebase from "../../firebase";
import Fab from "@material-ui/core/Fab";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import Button from "@material-ui/core/Button";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import InputBase from "@material-ui/core/InputBase";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import CircularProgress from "@material-ui/core/CircularProgress";
import { makeStyles, withStyles, useTheme } from "@material-ui/core/styles";

import { updateUser, getLandsByProjectId } from "../../services";
import { ValidateEmail } from "../../utils/helper";
import { LandContext, DispatchContext } from "../../context/land.context";
import { UserDispatchContext, UserContext } from "../../context/user.context";
import { PERSONAL_STATUS, CHILE_STATE_DISTRICTS } from "../../utils/constants";
import { StepModalDispatchContext, StepModalContext } from "../../context/stepModal.context";

function TabPanel(props) {
	const { children, value, index, getContent, ...other } = props;

	return (
		<div
			role="tabpanel"
			hidden={value !== index}
			id={`full-width-tabpanel-${index}`}
			aria-labelledby={`full-width-tab-${index}`}
			className="step-one-panel"
			{...other}
		>
			{getContent(index)}
		</div>
	);
}

const useStyles = makeStyles((theme) => ({
	root: {
		".MuiFormLabel-root.Mui-focused": {
			borderColor: "#ee7f0e",
			color: "#ee7f0e"
		},
		"& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline": {
			borderColor: "#ee7f0e !important"
		},
		"& .MuiOutlinedInput-input": {
			color: "green"
		},
		"& .MuiInputLabel-root": {
			color: "green"
		},
		"& .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline": {
			borderColor: "green"
		},
		"&:hover .MuiOutlinedInput-input": {
			color: "red"
		},
		"&:hover .MuiInputLabel-root": {
			color: "red"
		},
		"&:hover .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline": {
			borderColor: "red"
		},
		"& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-input": {
			color: "purple"
		},
		"& .MuiInputLabel-root.Mui-focused": {
			color: "purple"
		},
		"& .MuiOutlinedInput-root": {
			"&.Mui-focused fieldset": {
				borderColor: "#ee7f0e"
			}
		},
		"& label.Mui-focused": {
			color: "#ee7f0e"
		},
		"& .MuiInput-underline:after": {
			borderBottomColor: "green"
		},
		"& .MuiOutlinedInput-root": {
			"&.Mui-focused fieldset": {
				borderColor: "#ee7f0e"
			}
		}
	},
	icon: {
		fill: "#ff8f1c"
	},
	formControl: {
		width: "100%"
	},
	indicator: {
		"& > span": {
			backgroundColor: "#555555"
		},
		backgroundColor: "#555555"
	},
	textField: {
		backgroundColor: "#fff",
		backgroundColor: "#F5F5F5",
		// border: "3px solid #D9D9D9",
		borderRadius: "8px",
		"& label.Mui-focused": {
			color: "#ee7f0e"
		},
		"& .MuiInput-underline:after": {
			borderBottomColor: "green"
		},
		"& .MuiOutlinedInput-root": {
			"&.Mui-focused fieldset": {
				borderColor: "#ee7f0e"
			}
		}
	},
	backdrop: {
		zIndex: theme.zIndex.drawer + 1,
		color: "#fff"
	},
	buttonProgress: {
		color: "#FFF"
	}
}));

const BootstrapInput = withStyles((theme) => ({
	root: {
		"label + &": {
			marginTop: theme.spacing(3)
		}
	},
	input: {
		borderRadius: 4,
		position: "relative",
		border: "1px solid rgba(0, 0, 0, 0.23)",
		fontSize: 18,
		padding: "16px 26px 16px 12px",
		transition: theme.transitions.create([ "border-color" ]),
		fontFamily: [
			"-apple-system",
			"BlinkMacSystemFont",
			'"Segoe UI"',
			"Roboto",
			'"Helvetica Neue"',
			"Arial",
			"sans-serif",
			'"Apple Color Emoji"',
			'"Segoe UI Emoji"',
			'"Segoe UI Symbol"'
		].join(","),
		"&:focus": {
			borderRadius: 4,
			borderColor: "#ff8f1c"
		}
	}
}))(InputBase);

function a11yProps(index) {
	return {
		id: `full-width-tab-${index}`,
		"aria-controls": `full-width-tabpanel-${index}`
	};
}

export default function StepOneCont({ closeModal, setModalCont }) {
	const theme = useTheme();
	const classes = useStyles();

	const [ buttonDisabled, setButtonDisabled ] = useState(true);

	const stepModalDispatch = useContext(StepModalDispatchContext);
	const modalState = useContext(StepModalContext);
	const userDispatch = useContext(UserDispatchContext);
	const dispatch = useContext(DispatchContext);
	const lands = useContext(LandContext);
	const userProfile = useContext(UserContext);
	const { profile = "" } = userProfile;
	const { customerId = "" } = profile;

	const [ value, setValue ] = useState(0);

	const [ disabled, setDisabled ] = useState(true);
	const [ paymentMethod, setPaymentMethod ] = useState("Contado");
	const [ landNumber, setLandNumber ] = useState("");

	const [ firstName, setFirstName ] = useState("");
	const [ firstLastName, setFirstLastName ] = useState("");

	const [ availableLands, setAvailableLands ] = useState([]);
	const [ nationality, setNacionality ] = useState("");
	const [ dni, setDni ] = useState("");
	const [ personalStatus, setPersonalStatus ] = useState("");
	const [ job, setJob ] = useState("");

	const [ firstNameError, setFirstNameError ] = useState(false);
	const [ firstLastNameError, setFirstLastNameError ] = useState(false);
	const [ availableLandsError, setAvailableLandsError ] = useState(false);
	const [ nationalityError, setNacionalityError ] = useState(false);
	const [ dniError, setDniError ] = useState(false);
	const [ personalStatusError, setPersonalStatusError ] = useState(false);
	const [ jobError, setJobError ] = useState(false);

	const [ district, setDistrict ] = useState();
	const [ state, setState ] = useState("Región Metropolitana de Santiago");
	const [ phone, setPhone ] = useState();
	const [ email, setEmail ] = useState();
	const [ address, setAddress ] = useState();
	const [ signCity, setSignCity ] = useState();

	const [ validFields, setValidFields ] = useState(false);
	const [ loading, setLoading ] = useState(false);

	const [ emailError, setEmailError ] = useState(false);
	const [ addressError, setAddressError ] = useState(false);
	const [ districtError, setDistrictError ] = useState(false);
	const [ stateError, setStateError ] = useState(false);
	const [ signCityError, setSignCityError ] = useState(false);
	const [ phoneError, setPhoneError ] = useState(false);

	const handleChangeIndex = (index) => {
		setValue(index);
	};

	const validateEmail = (stringValue) => {
		setEmail(stringValue);
		const valid = ValidateEmail(stringValue);
		setEmailError(!valid);
		return !valid;
	};

	const validateDni = (stringValue = "") => {
		setDni(stringValue);
		let valid = false;
		if (stringValue) {
			valid = validarRUT(stringValue);
		}
		setDniError(!valid);
		return !valid;
	};

	const fechDataLands = async () => {
		const data = await getLandsByProjectId();
		dispatch({
			type: "UPDATE",
			payload: data
		});
	};

	const handleChangeLandNumber = (event) => {
		setLandNumber(event.target.value);
	};

	const handlePaymentMethodChange = (event) => {
		setPaymentMethod(event.target.value);
	};

	const handleBack = () => {
		setLoading(true);
		switch (value) {
			case 0:
				break;
			case 1:
				setValue(0);
				break;
			case 2:
				setValue(1);
				break;
			default:
				break;
		}
		setLoading(false);
	};

	const updateSecondSection = () => {
		const _firstNameValid = onChangeFieldValue(firstName, setFirstName, setFirstNameError);
		const _firstLastNameValid = onChangeFieldValue(firstLastName, setFirstLastName, setFirstLastNameError);
		const _nationalityValid = onChangeFieldValue(nationality, setNacionality, setNacionalityError);
		const _dniValid = validateDni(dni);
		const _personalStatusValid = onChangeFieldValue(personalStatus, setPersonalStatus, setPersonalStatusError);
		const _jobValid = onChangeFieldValue(job, setJob, setJobError);

		if (
			!_firstNameValid &&
			!_firstLastNameValid &&
			!_nationalityValid &&
			!_dniValid &&
			!_personalStatusValid &&
			!_jobValid
		) {
			setValue(2);
			updateUser({ customerId, firstName, firstLastName, nationality: nationality, dni, personalStatus, job });
			setDisabled(true);
		}
	};

	const updateThirdSection = () => {
		const _phoneValid = onChangeFieldValue(phone, setPhone, setPhoneError);
		const _emailValid = validateEmail(email);
		const _addressValid = onChangeFieldValue(address, setAddress, setAddressError);
		const _signCityValid = onChangeFieldValue(signCity, setSignCity, setSignCityError);

		if (!_phoneValid && !_emailValid && !_addressValid && !_signCityValid) {
			updateUser({
				customerId,
				district,
				state,
				phone,
				email,
				address,
				signCity,
				step1: {
					stateStep1: "En validación",
					stepNumber: 1,
					timeCreation: firebase.firestore.FieldValue.serverTimestamp()
				},
				stepNumber: 0
			});
			userDispatch({
				type: "UPDATE",
				payload: {
					profile: {
						...userProfile.profile,
						district,
						state,
						phone,
						email,
						address,
						signCity,
						step1: {
							stateStep1: "En validación",
							stepNumber: 1,
							timeCreation: firebase.firestore.FieldValue.serverTimestamp()
						},
						stepNumber: 0
					}
				}
			});
			setModalCont("success_step_one");
		}
	};

	const handleContinue = async () => {
		setLoading(true);

		switch (value) {
			case 0:
				setValue(1);
				updateUser({ customerId, paymentMethod, landNumber });
				break;
			case 1:
				updateSecondSection();
				break;
			case 2:
				updateThirdSection();
				break;
			default:
				break;
		}
		setLoading(false);
	};

	const validateFields = () => {
		if (email && email.length > 0) {
			setValidFields(true);
		} else {
			setValidFields(false);
		}
	};

	const onChangeFieldValue = (value, _setValue, _setError) => {
		if (!value) {
			_setError(true);
			_setValue(value);
			return true;
		}
		_setValue(value);
		const valid = value.length > 0;
		_setError(!valid);
		return !valid;
	};

	const getDistrict = () => {
		if (typeof state !== "undefined" && state) {
			const _district = _.find(CHILE_STATE_DISTRICTS, function(item) {
				return item.state === state;
			});
			return _district.district.map((_item) => {
				return <MenuItem value={_item}>{_item}</MenuItem>;
			});
		}
	};

	const onChangeState = (string) => {
		setState(string);
		if (typeof string !== "undefined" && string) {
			const _district = _.find(CHILE_STATE_DISTRICTS, function(item) {
				return item.state === string;
			});
			console.log("_district", _district.district[0]);
			setDistrict(_district.district[0]);
		}
	};

	useEffect(
		() => {
			if (typeof state !== "undefined" && state) {
				const _district = _.find(CHILE_STATE_DISTRICTS, function(item) {
					return item.state === state;
				});
				console.log("_district", _district.district[0]);
				setDistrict(_district.district[0]);
			}
		},
		[ state ]
	);

	const getContent = (key) => {
		switch (key) {
			case 0:
				return (
					<React.Fragment>
						<div className="form-control-step-padding">
							<div className="label-step">N° de Parcelas a Reservar</div>
							<FormControl variant="outlined" className={classes.formControl}>
								<Select
									labelId="demo-simple-select-outlined-label"
									id="demo-simple-select-outlined"
									onChange={handleChangeLandNumber}
									input={<BootstrapInput />}
									value={landNumber}
								>
									{availableLands.map((land) => (
										<MenuItem value={land.landNumber}>{`Nº ${land.landNumber}`}</MenuItem>
									))}
								</Select>
							</FormControl>
						</div>
						<div className="form-control-step-padding">
							<div className="label-step">Forma de compra</div>
							<FormControl variant="outlined" className={classes.formControl}>
								<Select
									labelId="demo-simple-select-outlined-label"
									id="demo-simple-select-outlined"
									onChange={handlePaymentMethodChange}
									value={paymentMethod}
									input={<BootstrapInput />}
								>
									<MenuItem value="Contado">Contado</MenuItem>
									<MenuItem value="Crédito entidad financiera">Crédito entidad financiera</MenuItem>
								</Select>
							</FormControl>
						</div>
					</React.Fragment>
				);
			case 1:
				return (
					<React.Fragment>
						<div className="field-cont-step-one">
							<TextField
								name="nombre"
								className={classes.textField}
								label="Nombre"
								variant="outlined"
								fullWidth
								value={firstName}
								error={firstNameError}
								onChange={(e) => onChangeFieldValue(e.target.value, setFirstName, setFirstNameError)}
							/>
							<p className={`email-error-text sign-process ${firstNameError && "active"}`}>
								Campo requerido
							</p>
						</div>
						<div className="field-cont-step-one">
							<TextField
								name="apellido"
								className={classes.textField}
								label="Apellido"
								variant="outlined"
								value={firstLastName}
								fullWidth
								error={firstLastNameError}
								onChange={(e) =>
									onChangeFieldValue(e.target.value, setFirstLastName, setFirstLastNameError)}
							/>
							<p className={`email-error-text sign-process ${firstLastNameError && "active"}`}>
								Campo requerido
							</p>
						</div>
						<div className="field-cont-step-one">
							<TextField
								name="nacionalidad"
								className={classes.textField}
								label="Nacionalidad"
								value={nationality}
								variant="outlined"
								fullWidth
								error={nationalityError}
								onChange={(e) =>
									onChangeFieldValue(e.target.value, setNacionality, setNacionalityError)}
							/>
							<p className={`email-error-text sign-process ${nationalityError && "active"}`}>
								Campo requerido
							</p>
						</div>
						<div className="field-cont-step-one">
							<TextField
								name="rut"
								className={classes.textField}
								label="Rut"
								variant="outlined"
								fullWidth
								value={dni}
								error={dniError}
								onChange={(e) => validateDni(e.target.value)}
							/>
							<p className={`email-error-text sign-process ${dniError && "active"}`}>Rut invalido</p>
						</div>
						<div className="field-cont-step-one">
							<FormControl variant="outlined" className={classes.formControl}>
								<InputLabel id="demo-simple-select-outlined-label">Estado Civil</InputLabel>
								<Select
									label="Estado Civil"
									labelId="demo-simple-select-outlined-label"
									id="demo-simple-select-outlined"
									className="select-step-one"
									value={personalStatus}
									error={personalStatusError}
									onChange={(e) =>
										onChangeFieldValue(e.target.value, setPersonalStatus, setPersonalStatusError)}
								>
									{PERSONAL_STATUS.map((_status) => (
										<MenuItem value={_status.value}>{_status.value}</MenuItem>
									))}
								</Select>
								<p
									className={`select-sp email-error-text sign-process ${personalStatusError &&
										"active"}`}
								>
									Campo requerido
								</p>
							</FormControl>
						</div>
						<div className="field-cont-step-one">
							<TextField
								name="profesion"
								className={classes.textField}
								label="Profesión / Actividad"
								variant="outlined"
								fullWidth
								value={job}
								error={jobError}
								onChange={(e) => onChangeFieldValue(e.target.value, setJob, setJobError)}
							/>
							<p className={`email-error-text sign-process ${jobError && "active"}`}>Campo requerido</p>
						</div>
					</React.Fragment>
				);
			case 2:
				return (
					<React.Fragment>
						<div className="field-cont-step-one">
							<TextField
								name="direccion"
								className={classes.textField}
								label="Direccion"
								variant="outlined"
								fullWidth
								value={address}
								error={addressError}
								onChange={(e) => onChangeFieldValue(e.target.value, setAddress, setAddressError)}
							/>
							<p className={`email-error-text sign-process ${addressError && "active"}`}>
								Campo requerido
							</p>
						</div>
						<div className="field-cont-step-one">
							<FormControl variant="outlined" className={classes.formControl}>
								<InputLabel id="demo-simple-select-outlined-label-region">Región</InputLabel>
								<Select
									label="Región"
									onChange={(e) => onChangeState(e.target.value)}
									className="select-step-one"
									value={state}
									error={stateError}
									inputProps={{ className: classes.root }}
								>
									{CHILE_STATE_DISTRICTS.map((item) => (
										<MenuItem value={item.state}>{item.state}</MenuItem>
									))}
								</Select>
							</FormControl>
							<p className={`email-error-text sign-process ${stateError && "active"}`}>Campo requerido</p>
						</div>
						<div className="field-cont-step-one">
							<FormControl variant="outlined" className={classes.formControl}>
								<InputLabel id="demo-simple-select-outlined-label">Comuna</InputLabel>
								<Select
									label="Comuna"
									onChange={(e) => setDistrict(e.target.value)}
									className="select-step-one"
									value={district}
									error={districtError}
									defaultValue="Comuna"
								>
									{getDistrict()}
								</Select>
							</FormControl>
							<p className={`email-error-text sign-process ${districtError && "active"}`}>
								Campo requerido
							</p>
						</div>
						<div className="field-cont-step-one">
							<TextField
								name="teléfono"
								className={classes.textField}
								label="Teléfono"
								variant="outlined"
								fullWidth
								value={phone}
								error={phoneError}
								onChange={(e) => onChangeFieldValue(e.target.value, setPhone, setPhoneError)}
							/>
							<p className={`email-error-text sign-process ${phoneError && "active"}`}>Campo requerido</p>
						</div>
						<div className="field-cont-step-one">
							<TextField
								name="email"
								error={emailError}
								className={classes.textField}
								label="Email"
								variant="outlined"
								fullWidth
								value={email}
								onChange={(e) => validateEmail(e.target.value)}
							/>
							<p className={`email-error-text sign-process ${emailError && "active"}`}>Email invalido</p>
						</div>
						<div className="field-cont-step-one">
							<FormControl variant="outlined" className={classes.formControl}>
								<InputLabel id="demo-simple-select-outlined-label">
									*Ciudad donde firmará la escritura
								</InputLabel>
								<Select
									label="*Ciudad donde firmará la escritura"
									onChange={(e) => setSignCity(e.target.value)}
									className="select-step-one"
									value={signCity}
									error={signCityError}
									defaultValue="Ciudad donde firmará"
								>
									<MenuItem value="Santiago">Santiago</MenuItem>
									<MenuItem value="Puerto Varas">Puerto Varas</MenuItem>
								</Select>
							</FormControl>
							<p className={`email-error-text sign-process ${signCityError && "active"}`}>
								Campo requerido
							</p>
						</div>
					</React.Fragment>
				);
			default:
				return <div>{key}</div>;
		}
	};

	useEffect(
		() => {
			validateFields();
		},
		[ email ]
	);
	useEffect(() => {
		fechDataLands();
		if (typeof window !== "undefined" && window) {
			const _landNumber = window.localStorage.getItem("wantedLandNumber") || landNumber;
			setLandNumber(_landNumber);
		} else if (typeof userProfile.landNumber !== "undefined" && userProfile.landNumber) {
			setLandNumber(userProfile.landNumber);
		}
	}, []);

	useEffect(
		() => {
			console.log("lands", lands);
			setAvailableLands(filter(lands, (_land) => _land.available === "Disponible"));
		},
		[ lands ]
	);

	useEffect(
		() => {
			const { profile = {} } = userProfile;
			const {
				email = "",
				landNumber = "",
				paymentMethod = "",
				firstLastName = "",
				firstName = "",
				nationality = "",
				job = "",
				personalStatus = "",
				dni = "",
				district = "",
				state = "",
				phone = "",
				address = "",
				signCity = ""
			} = profile;

			if (district) {
				setDistrict(district);
			}
			if (state) {
				setState(state);
			}
			if (phone) {
				setPhone(phone);
			}
			if (email) {
				setEmail(email);
			}
			if (address) {
				setAddress(address);
			}
			if (signCity) {
				setSignCity(signCity);
			}

			if (email) {
				setEmail(email);
			}
			if (landNumber) {
				setLandNumber(landNumber);
			}
			if (paymentMethod) {
				console.log("paymentMethod", paymentMethod);
				setPaymentMethod(paymentMethod);
			}
			if (firstName) {
				setFirstName(firstName);
			}
			if (firstLastName) {
				setFirstLastName(firstLastName);
			}
			if (nationality) {
				setNacionality(nationality);
			}
			if (dni) {
				setDni(dni);
			}
			if (personalStatus) {
				let formatedPersonalStatus = personalStatus.toLowerCase();
				formatedPersonalStatus =
					formatedPersonalStatus.charAt(0).toUpperCase() + formatedPersonalStatus.slice(1);
				setPersonalStatus(formatedPersonalStatus);
			}
			if (job) {
				setJob(job);
			}
		},
		[ userProfile ]
	);

	useEffect(
		() => {
			if (
				firstNameError ||
				firstLastNameError ||
				availableLandsError ||
				nationalityError ||
				dniError ||
				personalStatusError ||
				jobError ||
				emailError ||
				addressError ||
				districtError ||
				stateError ||
				signCityError ||
				phoneError
			) {
				console.log("firstNameError", firstNameError);
				console.log("firstLastNameError", firstLastNameError);
				console.log("availableLandsError", availableLandsError);
				console.log("nationalityError", nationalityError);
				console.log("dniError", dniError);
				console.log("personalStatusError", personalStatusError);
				console.log("jobError", jobError);
				console.log("emailError", emailError);
				console.log("addressError", addressError);
				console.log("districtError", districtError);
				console.log("stateError", stateError);
				console.log("signCityError", signCityError);
				console.log("phoneError", phoneError);
				setButtonDisabled(true);
			} else {
				setButtonDisabled(false);
			}
		},
		[
			firstNameError,
			firstLastNameError,
			availableLandsError,
			nationalityError,
			dniError,
			personalStatusError,
			jobError,
			emailError,
			addressError,
			districtError,
			stateError,
			signCityError,
			phoneError
		]
	);

	return (
		<React.Fragment>
			<div className="step-one-title">
				<div className="text-so-title">INGRESA TUS DATOS</div>
				<div className="text-so-title">PARA COMENZAR LA COMPRA</div>
			</div>
			<Tabs
				classes={{
					indicator: classes.indicator
				}}
				value={value}
				// onChange={handleChange}
				aria-label="simple tabs example"
				className="mb-10"
			>
				<Tab
					label={
						<div className="tab-label-cont">
							<div className="tab-label-top">01</div>
							<div className="tab-label-bottom">Nº DE PARCELA</div>
						</div>
					}
					{...a11yProps(0)}
				/>

				<Tab
					label={
						<div className="tab-label-cont">
							<div className="tab-label-top">02</div>
							<div className="tab-label-bottom">DATOS PERSONALES</div>
						</div>
					}
					{...a11yProps(1)}
				/>
				<Tab
					label={
						<div className="tab-label-cont">
							<div className="tab-label-top">03</div>
							<div className="tab-label-bottom">DIRECCION</div>
						</div>
					}
					{...a11yProps(2)}
				/>
			</Tabs>
			<SwipeableViews
				axis={theme.direction === "rtl" ? "x-reverse" : "x"}
				index={value}
				onChangeIndex={handleChangeIndex}
				style={{ width: "100%" }}
			>
				<TabPanel value={value} index={0} getContent={(i) => getContent(i)} />
				<TabPanel value={value} index={1} getContent={(i) => getContent(i)} />
				<TabPanel value={value} index={2} getContent={(i) => getContent(i)} />
			</SwipeableViews>
			<div className="buttons-form-one-cont">
				{value !== 0 && (
					<Fab className="arrow-back-circular-btn" aria-label="back">
						<ArrowBackIcon className="white-arrow" onClick={() => handleBack()} />
					</Fab>
				)}
				<Button
					id="contact-button"
					type="submit"
					value="Send"
					className="login-button mt-10"
					variant="contained"
					color="primary"
					// disabled={true}
					disabled={buttonDisabled}
					onClick={() => handleContinue()}
				>
					{loading ? <CircularProgress size={26} className={classes.buttonProgress} /> : "CONTINUAR"}
				</Button>
			</div>
		</React.Fragment>
	);
}
