import React from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import VideoComponent from "../video";

const useStyles = makeStyles({
	root: {
		width: "100%",
		maxWidth: "75rem",
		display: "flex",
		flexDirection: "column",
		margin: "0px auto",
		backgroundColor: "#FAFAFA",
		paddingBottom: "1rem",
		paddingTop: "1rem",
		paddingLeft: "1rem",
		paddingRight: "1rem"
	}
});

export default function KnowUsText() {
	const classes = useStyles();

	return (
		<div className={classes.root}>
			<div>
				<p className="mobile-simple-title semi-black">
					<strong>Fundo Praderas De Frutillar</strong>
				</p>
				<p className="mobile-simple-p semi-black padding-b10">
					Fundo Praderas de Frutillar, parcelación a 5 min. del centro de Frutillar, X Región de los Lagos,
					ubicado en los más hermosos parajes de Chile, a 12 minutos del Lago Llanquihue. Contamos
					recientemente con electrificación subterránea y agua tratada con distribución subterránea, amplios
					caminos .Ven a disfrutar y vivir en la tranquilidad de la naturaleza, sin perder la cercana conexión
					con Chile y el Mundo.
				</p>
				<p className="mobile-b-simple-p sub-black">
					<strong>¡Ven y construye tu hogar ahora!</strong>
				</p>
				<div className="divider-green-mobile" />
				<div className="main-video-cont">
					<VideoComponent
						width="100%"
						height="200px"
						url="https://www.youtube.com/watch?v=JgL-ubt-cNQ&feature=youtu.be"
					/>
				</div>
			</div>
		</div>
	);
}
