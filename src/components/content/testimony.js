import React, { useContext } from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import SlideCards from "../card/slide";
const useStyles = makeStyles({
	root: {
		width: "100%",
		maxWidth: "75rem",
		display: "flex",
		flexDirection: "column",
		margin: "0px auto",
		// backgroundColor: "#FAFAFA",
		paddingBottom: "3rem",
		paddingTop: "3rem",
		paddingLeft: "1rem",
		paddingRight: "1rem"
	},
	"@media (max-width: 1070px)": {
		root: {
			paddingTop: "1rem",
			paddingBottom: "1rem"
		}
	}
});

export default function SimpleCont() {
	const classes = useStyles();

	return (
		<div id="testimonios" className={classes.root}>
			<div>
				<h1 className="simple-title green padding-b20">
					<strong>- Testimonios -</strong>
				</h1>
				<p className="simple-p semi-black padding-b20 desktop-only">
					Los nuevos dueños de parcelas en Fundo Praderas de Frutillar, hablan del proceso de compra y de las
					parcelas mucho mejor de lo que nosotros podrías hacerlo, te invitamos a leer cuales son sus
					testimonios.
				</p>
				<div className="padding-b30">
					<SlideCards />
				</div>
			</div>
		</div>
	);
}
