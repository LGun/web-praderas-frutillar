import React, { useState, useContext, useEffect } from "react";
import moment from "moment";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import Checkbox from "@material-ui/core/Checkbox";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import FormControlLabel from "@material-ui/core/FormControlLabel";

import firebase from "../../firebase";
import { uploadFile, updateUser } from "../../services";
import AccountInfoModal from "../modal/accountInfoModal";
import { UserDispatchContext, UserContext } from "../../context/user.context";
import { StepModalDispatchContext, StepModalContext } from "../../context/stepModal.context";

const useStyles = makeStyles({
	table: {
		minWidth: 650
	}
});

function createData(step, responsable, time, description, download) {
	return { step, responsable, time, description, download };
}

const StyledTableCell = withStyles((theme) => ({
	head: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white
	},
	body: {
		fontSize: 14
	}
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
	root: {
		"&:nth-of-type(odd)": {
			backgroundColor: "#F2F2F2"
		}
	}
}))(TableRow);
const CustomCheckBox = withStyles({
	root: {
		color: "#FF8F1C",
		"&$checked": {
			color: "#FF8F1C"
		}
	},
	checked: {}
})((props) => <Checkbox color="default" {...props} />);

export default function BasicTable() {
	const classes = useStyles();
	const userData = useContext(UserContext);
	const userDispatch = useContext(UserDispatchContext);
	const [ stepSevenCheck, setStepSevenCheck ] = useState(false);
	const [ stepEightCheck, setStepEightCheck ] = useState(false);
	const { profile = {} } = userData;
	const {
		stepNumber = 0,
		urlFileWarranty = "",
		urlFileSeriousnessWarranty = "",
		urlFileSignedSeriousnessWarranty = "",
		urlFileIntentionLetter = "",
		urlFileSignedDeedNotarized = "",
		urlFileDomainOwnershipCertificate = "",
		urlFileOthersCertificates = "",
		urlFilePublicDeed = "",
		customerId = "",
		step1 = {}
	} = profile;

	const [ stepTwoInfoModal, setStepTwoInfoModal ] = useState(false);
	const [ typeInfoModal, setTypeInfoModal ] = useState("info_account");

	const stepModalDispatch = useContext(StepModalDispatchContext);

	const getStatusByRow = (index) => {
		if (index === stepNumber) {
			return "active";
		} else if (index < stepNumber) {
			return "done";
		}
		return "";
	};
	const getNumber = (number) => {
		const finalNumber = number + 1;
		if (finalNumber.toString().length === 1) {
			return `0${finalNumber}`;
		}
		return finalNumber;
	};

	const handleOpenModal = () => {
		stepModalDispatch({ type: "UPDATE", payload: { open: true } });
	};

	const getBtnCss = (_step) => {
		return _step === stepNumber ? "form-button-step" : "form-button-step-two";
	};

	const handleCheckBoxChange = (_stepNumber) => {
		switch (_stepNumber) {
			case 7:
				setStepSevenCheck(!stepSevenCheck);
				updateUser({
					customerId,
					step7: {
						stateStep7: "En validación",
						stepNumber: 7,
						timeCreation: firebase.firestore.FieldValue.serverTimestamp(),
						isStepCheckedByCustomer: !stepSevenCheck
					}
				});
				userDispatch({
					type: "UPDATE",
					payload: {
						profile: {
							...profile,
							step7: {
								stateStep7: "En validación",
								stepNumber: 7,
								timeCreation: firebase.firestore.FieldValue.serverTimestamp(),
								isStepCheckedByCustomer: !stepSevenCheck
							}
						}
					}
				});
				break;
			case 8:
				setStepEightCheck(!stepEightCheck);
				updateUser({
					customerId,
					step8: {
						stateStep1: "En validación",
						stepNumber: 8,
						timeCreation: firebase.firestore.FieldValue.serverTimestamp(),
						isStepCheckedByCustomer: !stepEightCheck
					}
				});
				userDispatch({
					type: "UPDATE",
					payload: {
						profile: {
							...profile,
							step8: {
								stateStep1: "En validación",
								stepNumber: 8,
								timeCreation: firebase.firestore.FieldValue.serverTimestamp(),
								isStepCheckedByCustomer: !stepEightCheck
							}
						}
					}
				});
				break;
			default:
				break;
		}
	};

	useEffect(
		() => {
			const { profile = {} } = userData;
			const { step7 = {}, step8 = {} } = profile;
			const { isStepCheckedByCustomer: _stepSevenCheck = false } = step7;
			const { isStepCheckedByCustomer: _stepEightCheck = false } = step8;
			if (_stepSevenCheck) {
				setStepSevenCheck(_stepSevenCheck);
			}
			if (_stepEightCheck) {
				setStepEightCheck(_stepEightCheck);
			}
		},
		[ userData ]
	);

	const getStepStatus = (_stepNumber) => {
		if (typeof profile[`step${_stepNumber}`] !== "undefined" && profile[`step${_stepNumber}`]) {
			if (
				typeof profile[`step${_stepNumber}`][`stateStep${_stepNumber}`] !== "undefined" &&
				profile[`step${_stepNumber}`][`stateStep${_stepNumber}`]
			) {
				return profile[`step${_stepNumber}`][`stateStep${_stepNumber}`];
			}
		}
		return "Por Hacer";
	};

	const rows = [
		createData(
			"Completar Formulario",
			"Comprador",
			getStepStatus(1),
			"Acontinuación se desplegará el formulario que debes completar con tus datos, para comenzar con los trámites de compraventa",
			<Button
				id="step-one-btn"
				className={`mobile-mt-5 ${getBtnCss(0)} ${getStepStatus(1) !== "Por Hacer" && "btn-success-green"}`}
				variant="contained"
				color="primary"
				onClick={() => handleOpenModal()}
				disabled={0 !== stepNumber}
			>
				COMPLETAR FORMULARIO
			</Button>
		),
		createData(
			"Transferencia de Garantía",
			"Comprador",
			getStepStatus(2),
			"Debes realizar la Transferencia de Garantía y enviarnos el comprobante por los $3.000.000 por concepto de GARANTÍA DE SERIEDAD DE OFERTA, a la cuenta corriente de Inmobiliaria Los Cruceros Limitada",
			<div className="step-two-btn-cont">
				<Button
					id="step-one-btn"
					className={`mobile-mt-5 mb-10 ${getBtnCss(1)} ${getStepStatus(2) !== "Por Hacer" &&
						"btn-success-green"}`}
					variant="contained"
					color="primary"
					onClick={() => {
						setStepTwoInfoModal(true);
						setTypeInfoModal("info_account");
					}}
				>
					DATOS TRANSFERENCIA
				</Button>
				<Button
					id="step-one-btn"
					className={`mobile-mt-5 ${getBtnCss(1)} ${getStepStatus(2) !== "Por Hacer" && "btn-success-green"}`}
					variant="contained"
					color="primary"
					onClick={() => {
						setStepTwoInfoModal(true);
						setTypeInfoModal("uploadFileStepTwo");
					}}
					disabled={1 !== stepNumber}
				>
					SUBIR COMPROBANTE
				</Button>
			</div>
		),
		createData(
			"Recibo Garantía por Seriedad de la Oferta para ser firmada por el comprador",
			"Comprador",
			getStepStatus(3),
			"A continuación descargar el documento “Garantía por Seriedad de la oferta” y luego subirlo ya firmada por ti.",
			<div className="step-two-btn-cont">
				<Button
					id="step-one-btn"
					className="form-button-step-two mobile-mt-5 mb-10"
					variant="contained"
					color="primary"
					onClick={() => window.open(urlFileSeriousnessWarranty, "_blank")}
					disabled={!urlFileSeriousnessWarranty}
					className={`mobile-mt-5 mb-10 ${getBtnCss(2)} ${(getStepStatus(3) !== "Por Hacer" ||
						urlFileSeriousnessWarranty) &&
						"btn-success-green"}`}
				>
					DESCARGAR GARANTÍA
				</Button>
				<Button
					id="step-one-btn"
					className={`mobile-mt-5 ${getBtnCss(2)} ${getStepStatus(3) !== "Por Hacer" && "btn-success-green"}`}
					variant="contained"
					color="primary"
					onClick={() => {
						setStepTwoInfoModal(true);
						setTypeInfoModal("uploadFileStepThree");
					}}
					disabled={2 !== stepNumber}
				>
					SUBIR GARANTÍA
				</Button>
			</div>
		),
		createData(
			"Descarga Garantía por seriedad de la oferta, firmada por representantes de la inmobiliaria",
			"Vendedor",
			getStepStatus(4),
			"Descargar “Garantía por seriedad de la oferta” como comprobante de la transferencia y aceptación de la oferta.",
			<div className="step-two-btn-cont">
				<Button
					id="step-one-btn"
					className={`mobile-mt-5 mb-10 ${getBtnCss(3)} ${(getStepStatus(4) !== "Por Hacer" ||
						urlFileSignedSeriousnessWarranty) &&
						"btn-success-green"}`}
					variant="contained"
					color="primary"
					disabled={3 !== stepNumber}
					disabled={!urlFileSignedSeriousnessWarranty}
					onClick={() => window.open(urlFileSignedSeriousnessWarranty, "_blank")}
				>
					DESCARGAR GARANTÍA
				</Button>
			</div>
		),
		createData(
			"Envío de escritura al comprador y notaría",
			"Vendedor",
			getStepStatus(5),
			"Enviamos a la Notaría y al Comprador, la Escritura de Compraventa y Carta de Instrucción documento que puedes descargar a continuación.",
			<div className="step-two-btn-cont">
				<Button
					id="step-one-btn"
					className={`mobile-mt-5 mb-10 ${getBtnCss(4)} ${(getStepStatus(5) !== "Por Hacer" ||
						urlFilePublicDeed) &&
						"btn-success-green"}`}
					variant="contained"
					color="primary"
					disabled={!urlFilePublicDeed}
					onClick={() => window.open(urlFilePublicDeed, "_blank")}
				>
					DESCARGAR ESCRITURA DE COMPRAVENTA
				</Button>
				<Button
					id="step-one-btn"
					className={`mobile-mt-5 ${getBtnCss(4)} ${(getStepStatus(5) !== "Por Hacer" ||
						urlFileIntentionLetter) &&
						"btn-success-green"}`}
					variant="contained"
					color="primary"
					disabled={!urlFileIntentionLetter}
					onClick={() => window.open(urlFileIntentionLetter, "_blank")}
				>
					DESCARGAR CARTA DE INSTRUCCIÓN
				</Button>
			</div>
		),
		createData(
			"Datos de vale vista y notaria",
			"Comprador",
			getStepStatus(6),
			"¡IMPORTANTE! Debes sacar el Vale Vista en el banco con los siguientes datos para entregarlo en el momento de la firma de la escritura que será entregado al Notario para su custodia",
			<div className="step-two-btn-cont">
				<Button
					id="step-one-btn"
					className={`mobile-mt-5 mb-10 ${getBtnCss(5)} ${getStepStatus(6) !== "Por Hacer" &&
						"btn-success-green"}`}
					variant="contained"
					color="primary"
					onClick={() => {
						setStepTwoInfoModal(true);
						setTypeInfoModal("recipt");
					}}
				>
					VER DATOS PARA VALE VISTA
				</Button>
				<Button
					id="step-one-btn"
					className={`mobile-mt-5 ${getBtnCss(5)} ${getStepStatus(6) !== "Por Hacer" && "btn-success-green"}`}
					variant="contained"
					color="primary"
					onClick={() => {
						setStepTwoInfoModal(true);
						setTypeInfoModal("notary");
					}}
				>
					VER DATOS DE LA NOTARÍA
				</Button>
			</div>
		),
		createData(
			"Toma del vale vista en el banco",
			"Comprador",
			getStepStatus(7),
			"Debes solicitar y tomar el Vale Vista en tu banco, para llevarlo posteriormente a la notaría",
			<div>
				<FormControlLabel
					control={
						<CustomCheckBox
							checked={stepSevenCheck}
							onChange={() => handleCheckBoxChange(7)}
							disabled={6 !== stepNumber}
							name="checkedG"
						/>
					}
					label="Listo"
				/>
			</div>
		),
		createData(
			"Firma escritura y entrega de vale vista en la notaria",
			"Comprador",
			getStepStatus(8),
			"En este paso, en la notaría físicamente debes firmar la Escritura de compraventa y la carta de instrucción y entregar el vale vista al Notario el cual se adjuntará a la carta de instrucción para ser liberado cuando se cumpla lo señalado por dicha carta",
			<div>
				<FormControlLabel
					control={
						<CustomCheckBox
							checked={stepEightCheck}
							onChange={() => handleCheckBoxChange(7)}
							name="checkedG"
						/>
					}
					disabled={7 !== stepNumber}
					label="Listo"
				/>
			</div>
		),
		createData(
			"Inscripción de la propiedad",
			"Vendedor",
			getStepStatus(9),
			"Una vez recibida la escritura con su repertorio respectivo desde la notaría, incribimos la propiedad en el Conservador de Bienes Raíces de Puerto Varas",
			<div className="step-two-btn-cont">
				<Button
					id="step-one-btn"
					className="form-button-step-two mobile-mt-5 mb-10"
					variant="contained"
					color="primary"
					onClick={() => window.open(urlFileSignedDeedNotarized, "_blank")}
					className={`mobile-mt-5 ${getBtnCss(8)} ${getBtnCss(4)} ${(getStepStatus(9) !== "Por Hacer" ||
						urlFileIntentionLetter) &&
						"btn-success-green"}`}
					disabled={!urlFileSignedDeedNotarized}
				>
					DESCARGAR ESCRITURA NOTARIADA Y COMPLEMENTOS
				</Button>
			</div>
		),
		createData(
			"Procesando la Inscripción en el CBR",
			"Vendedor",
			getStepStatus(10),
			"El Conservador de bienes raíces se toma el tiempo para estudiar que los títulos de la propiedad estén correctos y se puedan inscribir a tu nombre. Una vez que este proceso haya concluido se emite el certificado de propiedad y de que se está libre de hipotecas, litigios, gravámenes y otros.",
			""
		),
		createData(
			"Entrega de certificados al propietario y al notario",
			"Vendedor",
			getStepStatus(11),
			"Te entregamos, al igual que al notario, los certificados emitidos por el Conservador de Bienes Raíces y se libera el Vale Vista. Terminando de esta forma el proceso de compraventa",
			<div className="step-two-btn-cont">
				<Button
					id="step-one-btn"
					className={`mobile-mt-5 mb-10 ${getBtnCss(10)} ${(getStepStatus(11) !== "Por Hacer" ||
						urlFileDomainOwnershipCertificate) &&
						"btn-success-green"}`}
					variant="contained"
					color="primary"
					onClick={() => window.open(urlFileDomainOwnershipCertificate, "_blank")}
					disabled={!urlFileDomainOwnershipCertificate}
				>
					DESCARGAR CERTIFICADO DE DOMINIO A NOMBRE DEL COMPRADOR
				</Button>
				<Button
					id="step-one-btn"
					className={`mobile-mt-5 ${getBtnCss(10)} ${(getStepStatus(11) !== "Por Hacer" ||
						urlFileOthersCertificates) &&
						"btn-success-green"}`}
					variant="contained"
					color="primary"
					onClick={() => window.open(urlFileOthersCertificates, "_blank")}
					disabled={!urlFileOthersCertificates}
				>
					DESCARGAR CERTIFICADOS LIBRE DE HIPOTECAS, LITIOGIOS Y GRAVÁMENES ENTRE OTROS
				</Button>
			</div>
		)
	];

	return (
		<TableContainer className="desktop-only" component={Paper}>
			<Table className={classes.table} aria-label="simple table">
				<TableHead>
					<TableRow>
						<TableCell />
						<TableCell align="left">
							<span className="table-header-steps">Paso</span>
						</TableCell>
						<TableCell align="left">
							<span className="table-header-steps">Responsable</span>
						</TableCell>
						<TableCell align="left">
							<span className="table-header-steps">Estado</span>
						</TableCell>
						<TableCell align="left">
							<span className="table-header-steps">Descripción</span>
						</TableCell>
						<TableCell align="left">
							<span className="table-header-steps">Descargar</span>
						</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{rows.map((row, index) => {
						return (
							<StyledTableRow key={row.step}>
								<TableCell align="center" className="step-icon-cont" component="th" scope="row">
									<div className={`circle-step-empty circle-icon-index ${getStatusByRow(index)}`} />
									<div className="vertical-line-step" />
								</TableCell>
								<TableCell align="left" component="th" scope="row">
									<div className="flex-cont-step-title">
										<span className="table-step-number">{getNumber(index)}</span>
										<span className="table-step-text">{` ${row.step}`}</span>
									</div>
								</TableCell>
								<TableCell align="center">
									<span className="table-step-text">{row.responsable}</span>
								</TableCell>
								<TableCell align="center">
									<span className="table-step-text">{row.time}</span>
								</TableCell>
								<TableCell align="left">
									<span className="table-step-text">{row.description}</span>
								</TableCell>
								<TableCell align="center">{row.download}</TableCell>
							</StyledTableRow>
						);
					})}
				</TableBody>
			</Table>
			<AccountInfoModal
				modalOpen={stepTwoInfoModal}
				setModalOpen={(value) => setStepTwoInfoModal(value)}
				type={typeInfoModal}
				setType={setTypeInfoModal}
			/>
		</TableContainer>
	);
}
