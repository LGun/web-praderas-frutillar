import React, { useContext, useEffect, useState } from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import _ from "lodash";
import find from "lodash/find";
import Table from "@material-ui/core/Table";
import Button from "@material-ui/core/Button";
import Tooltip from "@material-ui/core/Tooltip";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import IconButton from "@material-ui/core/IconButton";
import FavoriteIcon from "@material-ui/icons/Favorite";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import { deleteUserLikedLand } from "../../services";
import { LandContext } from "../../context/land.context";
import { ModalDispatchContext } from "../../context/modal.context";
import { UserDispatchContext, UserContext } from "../../context/user.context";

const StyledTableCell = withStyles((theme) => ({
	head: {
		backgroundColor: "#FFFFFF",
		color: "#000000",
		fontSize: "18px"
	},
	body: {
		fontSize: "18px"
	},
	"@media (max-width: 1070px)": {
		head: {
			fontSize: "10px"
		},
		body: {
			fontSize: "10px"
		},
		root: {
			padding: "0px 3px !important",
			textAlign: "center"
		}
	}
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
	root: {
		backgroundColor: "#C6D2D5",
		"&:nth-of-type(odd)": {
			backgroundColor: "#EDEDED"
		}
	}
}))(TableRow);

const useStyles = makeStyles({
	table: {
		width: "100%"
	},
	button: {
		background: "#707070",
		borderRadius: "16px",
		color: "#FFFFFF"
	},
	"@media (max-width: 1070px)": {
		table: {
			maxWidth: "100%"
		},
		button: {
			borderRadius: "16px",
			color: "#FFFFFF",
			fontSize: "10px"
		}
	}
});

export default function LandTable() {
	const classes = useStyles();
	const userData = useContext(UserContext);
	const userDispatch = useContext(UserDispatchContext);
	const modalDispatch = useContext(ModalDispatchContext);
	const lands = useContext(LandContext);
	const [ currentLikedLands, setCurrentLikedLands ] = useState([]);

	const getLandIcons = (land) => {
		const trees = land.trees ? (
			<Tooltip title="Arboleda" aria-label="arboleda">
				<img className="small-icon-image" src="/Rectangulo 1779.png" />
			</Tooltip>
		) : null;
		const volcano =
			land.volcanoView === "Si" || land.volcanoView === "Parcial" ? (
				<Tooltip title="Vista al volcán" aria-label="volcán">
					<img className="small-icon-image" src="/volcan.svg" />
				</Tooltip>
			) : null;
		const corner = land.cornerLand ? (
			<Tooltip title="Parcela esquina" aria-label="esquina">
				<img className="small-icon-image" src="/Rectangulo 1827@2x.png" />
			</Tooltip>
		) : null;
		const discount =
			land.discount > 0 ? (
				<Tooltip title="Parcela con descuento" aria-label="descuento">
					<img className="small-icon-image" src="/Rectangulo 1828@2x.png" />
				</Tooltip>
			) : null;
		return [ volcano, trees, corner, discount ];
	};

	const dislikeLand = (numberLand) => {
		const land = find(lands, { landNumber: numberLand });
		const newLikedLands = _.remove(userData.likedLands, (_land) => {
			return Number(_land) !== Number(land.landNumber);
		});
		userDispatch({
			type: "UPDATE",
			payload: {
				likedLands: typeof newLikedLands !== "undefined" ? newLikedLands : []
			}
		});
		if (userData.profile) {
			const { customerId = "" } = userData.profile;
			if (customerId) {
				deleteUserLikedLand({ customerId: customerId, landNumber: numberLand });
			}
		}
	};

	useEffect(
		() => {
			if (typeof userData.likedLands !== "undefined") {
				const likedlands = userData.likedLands.map((numberLand) => {
					if (typeof lands !== "undefined") {
						return find(lands, { landNumber: numberLand });
					}
				});
				setCurrentLikedLands(likedlands);
			} else {
				if (typeof localStorage.getItem("favoriteLands") !== "undefined") {
					let storageLikedLands = localStorage.getItem("favoriteLands");
					if (storageLikedLands && typeof storageLikedLands !== "undefined") {
						storageLikedLands = storageLikedLands.split(",");
						storageLikedLands = storageLikedLands.map((_likedLands) => {
							return _likedLands;
						});
					} else {
						storageLikedLands = [];
					}

					userDispatch({
						type: "UPDATE",
						payload: {
							likedLands: typeof storageLikedLands !== "undefined" ? storageLikedLands : []
						}
					});
					const _likedlands = storageLikedLands.map((numberLand) => {
						if (typeof lands !== "undefined") {
							return find(lands, { landNumber: numberLand });
						}
					});
					setCurrentLikedLands(_likedlands);
				}
			}
		},
		[ lands ]
	);

	useEffect(
		() => {
			if (typeof userData.likedLands !== "undefined") {
				const likedlands = userData.likedLands.map((numberLand) => {
					if (typeof lands !== "undefined") {
						return find(lands, { landNumber: numberLand });
					}
				});
				setCurrentLikedLands(likedlands);
			}
		},
		[ userData ]
	);

	return (
		<Table size="small" className={classes.table} aria-label="customized table">
			<TableHead>
				<TableRow>
					<StyledTableCell>Nº</StyledTableCell>
					<StyledTableCell align="right">VALOR</StyledTableCell>
					<StyledTableCell align="right">M2</StyledTableCell>
					<StyledTableCell className="mobile-car-hr" align="right">
						CARACTERÍSTICAS
					</StyledTableCell>
					<StyledTableCell align="right" />
					<StyledTableCell align="right">
						<FavoriteBorderIcon className="green-color table-favorite-header" />
					</StyledTableCell>
				</TableRow>
			</TableHead>
			<TableBody>
				{typeof currentLikedLands !== "undefined" &&
					currentLikedLands.map(
						(row, index) =>
							typeof row !== "undefined" && (
								<StyledTableRow key={index}>
									<StyledTableCell component="th" scope="row">
										<strong>{row.landNumber}</strong>
									</StyledTableCell>
									<StyledTableCell align="right">
										<span className="red-color">
											<strong>{`$ ${Number(row.price).toLocaleString("es-ES")}`}</strong>
										</span>
									</StyledTableCell>
									<StyledTableCell align="right">{row.totalSize} m²</StyledTableCell>
									<StyledTableCell align="right">{getLandIcons(row)}</StyledTableCell>
									<StyledTableCell align="right">
										<Button
											variant="contained"
											className={classes.button}
											onClick={() =>
												modalDispatch({
													type: "UPDATE",
													payload: { open: true, landNumber: row.landNumber }
												})}
										>
											VER FICHA
										</Button>
									</StyledTableCell>
									<StyledTableCell align="right">
										<IconButton onClick={() => dislikeLand(row.landNumber)}>
											<Tooltip title="Favorito" aria-label="favorito">
												<FavoriteIcon className="green-color" />
											</Tooltip>
										</IconButton>
									</StyledTableCell>
								</StyledTableRow>
							)
					)}
			</TableBody>
		</Table>
	);
}
