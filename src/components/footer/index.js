import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
	root: {
		width: "100%",
		height: "250px",
		color: theme.palette.text.secondary,
		display: "flex",
		justifyContent: "center",
		background: "#363636"
	},
	anchor: {
		color: "#434343",
		padding: "10px 20px"
	},
	"@media (max-width: 1070px)": {
		root: {
			height: "204px"
		}
	}
}));

export default function Footer() {
	const classes = useStyles();

	return (
		<div>
			<Grid container alignItems="center" className={classes.root}>
				<div className="d-flex jc-space-b full-height ai-center column padding-20">
					<img className="footer-logo desktop-only" src="/Imagen 42@2x.png" alt="logo" />
					<div>
						<div className="footer-small-text footer-text">
							Las imágenes exhibidas en el sitio son de carácter ilustrativo, referencial, y no
							constituyen necesariamente una representación exacta de la realidad. Éstas podrían presentar
							diferencias entre lo exhibido en la página web y la realidad.
						</div>
						<div className="footer-text">
							Desarrollo y mantención realizado por <img className="logo-72d" src="/logo-72d.svg" /> SPA -{" "}
							<a href="/privacy-policy" className="footer-text">
								Términos y condiciones
							</a>
							{" "}
							-
							{" "}
							<a href="/condiciones-de-precios" className="footer-text">
								Términos y condiciones de precio preferente
							</a>
						</div>
						<div className="footer-text">Todos los derechos reservados.</div>
						<div className="footer-text" />
					</div>
				</div>
			</Grid>
		</div>
	);
}
