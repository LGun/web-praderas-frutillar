// const withOptimizedImages = require("next-optimized-images");
// const path = require("path");
// module.exports = withOptimizedImages({
// 	webpack(config) {
// 		config.resolve.alias.images = path.join(__dirname, "images");
// 		return config;
// 	}
// });

module.exports = {
	trailingSlash: true,
	webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
		// Note: we provide webpack above so you should not `require` it
		// Perform customizations to webpack config
		config.plugins.push(new webpack.IgnorePlugin(/\/__tests__\//));

		// Important: return the modified config
		return config;
	}
};
